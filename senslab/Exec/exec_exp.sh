#!/bin/bash

# for i = iteration
# for k = scenario
# for j = network
# for h = solution

for (( i = 1; i <= 5; i++ ))
do
	for (( k = 1; k <= 1; k++ ))
	do
		for (( j = 1; j <= 1; j++ ))
		do
			echo "Lauching Scenario $k WSN-$j"
			
			for (( h = 1; h <= 2; h++ ))
			do
			
				if [[ $h -eq 1 ]] 
				then
					solution="kangurou"
					#continue
				else
					solution="geo-multicast"
					#continue
				fi
				
				echo "$solution: Launching experiment iteration #$i"

				#if [[ $i -ge 2 ]]; then
				#	if [[ $i -le 12 ]]; then
				#		if [[ $h -eq 3 ]]; then
				#			continue
				#		fi 
				#	fi
				#fi

				#if [[ $i -le 1 ]]; then
				#	if [[ $h -eq 1 ]]; then
				#		continue
				#	fi 
				#fi
							
				./autoLaunch.sh "$solution" $k $j $i > ktmp
				exp_id=$(grep '"id": ' ktmp)
				exp_id="$(cut -d':' -f2 <<<"$exp_id")"
				echo "Experiment ID: $exp_id launched."
				while true; do
					iotlab-experiment get -i "$exp_id" -s > ktmp
					exp_status=$(grep '"state": ' ktmp)
					exp_status="$(cut -d':' -f2 <<<"$exp_status")"
					exp_status="$(cut -d'"' -f2 <<<"$exp_status")"
					if [[ "$exp_status" = "Running" ]]; then
						break
					else
						echo "Current status: \"$exp_status\" -- waiting for status \"Running\""
						sleep 10
					fi
				done
				echo "Data injection started."
				./inserGeneric.sh "$solution" "$k" "$j" "$i" > ktmp
				echo "Starting packet exchange."
				while true; do
					iotlab-experiment get -i "$exp_id" -s > ktmp
					exp_status=$(grep '"state": ' ktmp)
					exp_status="$(cut -d':' -f2 <<<"$exp_status")"
					exp_status="$(cut -d'"' -f2 <<<"$exp_status")"
					if [[ "$exp_status" = "Running" ]]; then
						sleep 60
						#if [[ ! -e "ConnectivityStatus-$solution-${k}-${j}-${i}" ]]; then
						#	./CheckConnectivity.sh $solution ${k} ${j} ${i}					
						#fi
					else
						break
					fi
				done
				echo "Experiment finished with status: \"$exp_status\"."
			done	
		done
	done	
done
