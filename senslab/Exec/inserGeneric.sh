#!/bin/bash

sol=$1
sc=$2
wsn=$3
itera=$4

[[ -e "log_$sol-sc$sc-net$wsn-exec$itera" ]] && rm -r "log_$sol-sc$sc-net$wsn-exec$itera"
[[ -e "log_$sol-sc$sc-net$wsn-exec$itera" ]] || mkdir "log_$sol-sc$sc-net$wsn-exec$itera"

cd "sc$sc"

tmp=0
node=1
num=$(sed -n $node"p" numNode$wsn)
cat infotopo$wsn | while read line
do
	let tmp++
	if [ $tmp -eq 1 ]
	then
		now=`echo "$(date +%s%N | cut -b1-13) % 1000000000" | bc`
		now=`echo "$now * 0.128" | bc`
		now="$(cut -d'.' -f1 <<<"$now")"
		str="t:${now}|i:${num}|${line}"
		echo "$str" >> "../log_$sol-sc$sc-net$wsn-exec$itera/node$num.in"
	else
		str="${line}"
		echo "$str" >> "../log_$sol-sc$sc-net$wsn-exec$itera/node$num.in"
	fi
        if [ $tmp -eq 4 ]
        then
		echo "#" >> ../log_$sol-sc$sc-net$wsn-exec$itera/node$num.in
		sleep 1
                nc wsn430-$num 20000 < ../log_$sol-sc$sc-net$wsn-exec$itera/node$num.in >> ../log_$sol-sc$sc-net$wsn-exec$itera/node$num.out &
                echo "connected node$num"
                let tmp=0
                let node++
		num=$(sed -n $node"p" numNode$wsn)
        fi
done
