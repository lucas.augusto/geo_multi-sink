/* contiki includes */
#include "contiki.h"
#include "net/rime.h"
#include "sys/rtimer.h"

/* standard library includes */
#include <stdio.h>  /* For printf() */
#include <string.h> /* For memcpy */

/* Project Includes */
#include "kangurou_packets.h"
#include "kangurou.h"

/*---------------------------------------------------------------------------*/

void prepare_reading_pkt(const reading_hdr_t *reading_hdr, reading_pkt_t *reading_pkt){

	memcpy(&reading_pkt->reading_hdr,reading_hdr,sizeof(reading_hdr_t));

	reading_pkt->value = 0;
	reading_pkt->timestamp = (clock_time_t)(SYSNOW + get_node_offset());
	reading_pkt->timestamp_original = reading_pkt->timestamp;

	return;
}
