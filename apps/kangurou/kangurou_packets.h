#ifndef KANGUROU_PACKETS_H
#define KANGUROU_PACKETS_H

#include "contiki.h"
#include "net/rime.h"

#include "support.h"

typedef struct {
  uint8_t ver;		/**< Protocol version. */
  uint8_t type;		/**< Packet type. */
  uint8_t hops;
  pos_t   pos;
  rimeaddr_t originator;
} kanycast_hdr_t;

typedef struct {
	kanycast_hdr_t hdr;
	int8_t place_holder1[MAX_SINKS]; //just to make the size of the KanGuRou and GeoK packets the same
	//int8_t place_holder2[MAX_SINKS]; //just to make the size of the KanGuRou and GeoK packets the same
	double place_holder3; //just to make the size of the KanGuRou and GeoK packets the same
} broadcast_pkt_t;

typedef struct {
  kanycast_hdr_t hdr;
  rimeaddr_t src;
  rimeaddr_t prev;
  rimeaddr_t dest;
  rimeaddr_t ref;
  rimeaddr_t ref_dest;
  double dist;
  int8_t sinks[ALL_LOCAL_NODES];
  int8_t k;
  int8_t recovery_flag;
  uint8_t prep;
  pos_t ref_pos;
  //rimeaddr_t place_holder[MAX_SINKS-4]; //just to make the size of the KanGuRou and GeoK packets the same
} reading_hdr_t;

typedef struct {
	reading_hdr_t reading_hdr;
	clock_time_t timestamp;
	clock_time_t timestamp_original;
	double energy_cost;
	float value;
} reading_pkt_t;


void prepare_reading_pkt(const reading_hdr_t *reading_hdr, reading_pkt_t *reading_pkt);


#endif
