#include "net/rime.h"

#include <stdio.h> /* For printf() */

#include "kangurou_functions.h"
#include "support.h"
#include "kangurou.h"

//-----------------------------------------------------------------------------
/* Returns the index of an array based on the rime address */
int get_index_by_addr(const rimeaddr_t *item_addr, const rimeaddr_t *list_addr, int list_size){
	int i;
	for(i=0;i<list_size;i++){
		if(rimeaddr_cmp(item_addr, &list_addr[i])){
			return i;
		}
	}
	return -1;
}

//-----------------------------------------------------------------------------
/* Returns the pointer of a list_t based on the rime address */
struct kneighbor * get_neighbor_pointer_by_addr(const rimeaddr_t *item_addr){

	struct kneighbor *ind;

	list_t list_n = get_neighbor_list();

	for(ind = list_head(list_n); ind != NULL; ind = list_item_next(ind)) {
		if(rimeaddr_cmp(&ind->addr, item_addr)){
			return ind;
		}
	}
	return NULL;
}

//-----------------------------------------------------------------------------
/* Returns the pointer of a list_t based on the rime address */
struct ksink * get_sink_pointer_by_addr(const rimeaddr_t *item_addr){

	struct ksink *ind;

	list_t list_s = get_sink_list();

	for(ind = list_head(list_s); ind != NULL; ind = list_item_next(ind)) {
		if(rimeaddr_cmp(&ind->addr, item_addr)){
			return ind;
		}
	}

	return NULL;
}

//-----------------------------------------------------------------------------
/* Returns the pointer of a list_t based on the rime address */
pos_t * get_node_position_by_addr(const rimeaddr_t *item_addr){

	if(rimeaddr_cmp(&rimeaddr_node_addr, item_addr)){
		return (pos_t *)&own_pos;
	}

	struct ksink *s = get_sink_pointer_by_addr(item_addr);

	if(s!=NULL){
		return (pos_t *)&s->pos;
	}

	struct kneighbor *n = get_neighbor_pointer_by_addr(item_addr);

	if(n!=NULL){
		return (pos_t *)&n->pos;
	}

	return NULL;
}

/* Returns the pointer of a list_t based on the index*/
pos_t * get_node_position(int index){

	allnodes *all;

	all = get_all_local_nodes();

	return (pos_t *)&all->pos[index];
}

//-----------------------------------------------------------------------------
/* Initialize an address list with the rimeaddr_null */
void rimeaddr_null_init(rimeaddr_t *list_addr, int size){

	int i;

	for(i=0;i<size;i++){
		rimeaddr_copy(&list_addr[i], &rimeaddr_null);
	}

	return;
}
