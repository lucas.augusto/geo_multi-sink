#ifndef KANGUROU_H
#define KANGUROU_H

#include "lib/list.h"
#include "lib/memb.h"
#include "net/rime.h"

#include <stdint.h>

#include "kangurou_packets.h"
#include "support.h"

#define KANGUROU_VERSION 		1
#define KANGUROU_BROADCAST_POS  1
#define KANGUROU_MULTIHOP 		2

/* This structure holds information about sinks. */
struct ksink {
  /* The ->next pointer is needed since we are placing these on a Contiki list. */
  struct ksink *next;

  /* The ->addr field holds the Rime address of the sink. */
  rimeaddr_t addr;

  /* The ->pos field holds the location of the sink */
  pos_t pos;
};

/* This structure holds information about neighbors. */
struct kneighbor {
  /* The ->next pointer is needed since we are placing these on a Contiki list. */
  struct kneighbor *next;

  /* The ->addr field holds the Rime address of the neighbor. */
  rimeaddr_t addr;

  /* The ->pos field holds the location of the neighbor */
  pos_t pos;
};

typedef struct {
	rimeaddr_t addr[ALL_LOCAL_NODES];
	int8_t issink[ALL_LOCAL_NODES];
	int8_t isneighbor[ALL_LOCAL_NODES];
	int8_t self;
	pos_t pos[ALL_LOCAL_NODES];
} allnodes;

/*
struct entitydata {
	//int8_t NbTargets;
	int8_t target[ALL_LOCAL_NODES];
  //float range;
  //int ALPHA_NRJ;
  //float C_NRJ;
 // float A_NRJ;
  int8_t *CDS;
  int8_t k;
  //int8_t splits;
  //float tree_weight;
  //float consumed_nrj;
  //float path_length;
};
*/
extern pos_t own_pos;

extern process_event_t broadcast_event;

PROCESS_NAME(broadcast_process);
PROCESS_NAME(multihop_process);

uint8_t get_received_external_id();

uint8_t get_received_max_k();

struct entitydata * get_entitydata();

list_t get_neighbor_list();

list_t get_sink_list();

long int get_node_offset();

int8_t is_sink();

void kangurou_init();

void print_neighbors();

void print_sinks();

void new_packet_generation(void *data);

void send_packet_multihop(void *data);

void packet_received(reading_pkt_t *pkt);

allnodes * all_local_nodes();

allnodes * get_all_local_nodes();

#endif
