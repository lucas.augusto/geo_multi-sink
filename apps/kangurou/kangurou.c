/*
 * Copyright (c) 2006, Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

/**
 * \file
 *         kangurou.c
 * \author
 *         Lucas Leao <lucas.leao@femto-st.fr>
 *
 *  Based on the KanGuRou routing protocol from:
 *  Nathalie Mitton
 *  http://researchers.lille.inria.fr/~mitton/home.html
 *
 */

/* contiki includes */
#include "contiki.h"
#include "net/rime.h"
#include "dev/serial-line.h"

#ifndef SENSLAB
	#include "dev/button-sensor.h"
#endif

#include "lib/random.h"
#include "sys/rtimer.h"

/* standard library includes */
#include <stdio.h>  /* For printf() */
#include <string.h> /* For memcpy */
#include <stdlib.h>

/* project includes */
#include "support.h"
#include "kangurou_packets.h"
#include "kangurou_functions.h"
#include "kangurou_decisions.h"
#include "my_metrics.h"
#include "kangurou.h"

/* Set DEBUG to 1 to include debug output */
#define DEBUG 0
#if DEBUG
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

/* Set LOG to 1 to include debug output */
#define LOG 1
#if LOG
#define PRINTF_LOG(...) printf(__VA_ARGS__)
#else
#define PRINTF_LOG(...)
#endif

/*---------------------------------------------------------------------------*/

static broadcast_pkt_t broadcast_pkt;

/*---------------------------------------------------------------------------*/

/* node's position */
pos_t own_pos;

/* time offset for network sync */
long int my_offset;

/* node type */
int8_t i_am_sink = 0;

/* External node ID */
uint8_t myid = 0;

/* node entitydata */
//struct entitydata entitydata;

allnodes *my_all_local_nodes;

/* variable with the received max k */
uint8_t received_max_k;

#ifdef SENSLAB
/* Wait for the broadcast message of all neighbors */
uint8_t count_neighbors = 0;
#endif

/*---------------------------------------------------------------------------*/

/* processes */
PROCESS(broadcast_process, "Broadcast process");
PROCESS(multihop_process, "Multihop process");
PROCESS(boot_process, "Boot process");

/*---------------------------------------------------------------------------*/

/* events */
process_event_t broadcast_pos_event;
process_event_t newpacket_event;
process_event_t multipacket_event;

/*---------------------------------------------------------------------------*/

/* The neighbors_list is a Contiki list that holds the neighbors we have seen thus far. */
LIST(neighbors_list);

/* The my_sink_list is a Contiki list that holds the sinks info. */
LIST(my_sink_list);

/* This MEMB() definition defines a memory pool from which we allocate neighbor entries. */
MEMB(neighbors_memb, struct kneighbor, MAX_NEIGHBORS);

/* This MEMB() definition defines a memory pool from which we allocate sink entries. */
MEMB(sinks_memb, struct ksink, MAX_SINKS);


/*---------------------------------------------------------------------------*/
/*
 * Get received external id.
 */
uint8_t get_received_external_id(){
	return myid;
}

/*---------------------------------------------------------------------------*/
/*
 * Get received max k.
 */
uint8_t get_received_max_k(){
	return received_max_k;
}

/*---------------------------------------------------------------------------*/
/*
 * Get node's all local nodes.
 */

allnodes * get_all_local_nodes(){

	return my_all_local_nodes;
}

/*---------------------------------------------------------------------------*/
/*
 * Get current node index in the all local nodes list.
 */

int8_t get_my_index(){

	return get_index_by_addr(&rimeaddr_node_addr, my_all_local_nodes->addr, ALL_LOCAL_NODES);
}

/*---------------------------------------------------------------------------*/
/*
 * Get node's entitydata.


struct entitydata * get_entitydata(){
	return &entitydata;
}
*/

/*---------------------------------------------------------------------------*/
/*
 * Get neighbors list.
 */

list_t get_neighbor_list(){
	return neighbors_list;
}

/*---------------------------------------------------------------------------*/
/*
 * Get sinks list.
 */

list_t get_sink_list(){
	return my_sink_list;
}

/*---------------------------------------------------------------------------*/
/*
 * Get node's offset for network sync.
 */

long int get_node_offset(){
	return my_offset;
}

/*---------------------------------------------------------------------------*/
/*
 * Get node's type.
 */

int8_t is_sink(){
	return i_am_sink;
}

/*---------------------------------------------------------------------------*/
/*
 * This function prints the neighbor list.
 */
void
print_neighbors() {
	struct kneighbor *n;

	printf("Neighbors:\n");
	for(n = list_head(neighbors_list); n != NULL; n = list_item_next(n)) {
		printf("Rime Address: %d.%d | Pos: ", n->addr.u8[0], n->addr.u8[1]);
		print_pos(n->pos);
	}
}
/*---------------------------------------------------------------------------*/
/*
 * This function prints the sink list (all sinks and void sinks).
 */
void
print_sinks() {
	struct ksink *s;

	printf("Sink:\n");
	for(s = list_head(my_sink_list); s != NULL; s = list_item_next(s)) {
		printf("Rime Address: %d.%d - Pos: ", s->addr.u8[0], s->addr.u8[1]);
		print_pos(s->pos);
	}
}
/*---------------------------------------------------------------------------*/
/*
 * This function is called by the ctimer present in each neighbor
 * table entry. The function removes the neighbor from the table
 * because it has become too old.
 */
/*static void
remove_neighbor(void *n)
{
	struct kneighbor *tmp = n;

	//PRINTF("removing neighbor %d.%d\n", tmp->addr.u8[0], tmp->addr.u8[1]);

	list_remove(neighbors_list, tmp);
	memb_free(&neighbors_memb, tmp);
}*/

/*---------------------------------------------------------------------------*/

static struct ksink*
add_sink(pos_t pos, rimeaddr_t *addr)
{
	struct ksink *s;

	/* Check if we already know this sink. */
	for(s = list_head(my_sink_list); s != NULL; s = list_item_next(s)) {
		/* We break out of the loop if the address of the sink matches
       the address of the sink from which we received this
       broadcast message. */
		if(rimeaddr_cmp(&s->addr, addr)) {
			break;
		}
	}

	/* If n is NULL, this sink was not found in our list, and we
     allocate a new struct ksink from the sinks_memb memory
     pool. */
	if(s == NULL) {
		s = memb_alloc(&sinks_memb);

		/* If we could not allocate a new sink entry, we give up. */
		if(s == NULL) {
			return NULL;
		}

		//PRINTF("added sink: %d.%d\n", addr->u8[0], addr->u8[1]);

		/* Initialize the fields. */
		rimeaddr_copy(&s->addr, addr);

		/* Place the neighbor on the neighbor list. */
		list_add(my_sink_list, s);
	}

	/* set its position */
	s->pos = pos;

	PRINTF("DEBUG: sink %d.%d at (%d.%d;%d.%d) included. \n", addr->u8[0], addr->u8[1], (int)s->pos.x, decimals(s->pos.x), (int)s->pos.y, decimals(s->pos.y));

	if(rimeaddr_cmp(&s->addr, &rimeaddr_node_addr)) {
		i_am_sink = 1;
		PRINTF("DEBUG: I am sink!\n");
	}

	return s;
};

/*---------------------------------------------------------------------------*/

static struct kneighbor*
add_neighbor(pos_t pos, rimeaddr_t *addr)
{
	struct kneighbor *n;

	/* Check if we already know this neighbor. */
	for(n = list_head(neighbors_list); n != NULL; n = list_item_next(n)) {
		/* We break out of the loop if the address of the neighbor matches
       the address of the neighbor from which we received this
       broadcast message. */
		if(rimeaddr_cmp(&n->addr, addr)) {
			break;
		}
	}

	/* If n is NULL, this neighbor was not found in our list, and we
     allocate a new struct kneighbor from the neighbors_memb memory
     pool. */

#ifdef SENSLAB
	if(n == NULL && count_neighbors == 0){
#else
	if(n == NULL) {
#endif
		n = memb_alloc(&neighbors_memb);

		/* If we could not allocate a new neighbor entry, we give up. We
       could have reused an old neighbor entry, but we do not do this
       for now. */ // TODO ----> look for a worst neighbor to replace
		if(n == NULL) {
			return NULL;
		}


		//PRINTF("added neighbor: %d.%d\n", addr->u8[0], addr->u8[1]);

		/* Initialize the fields. */
		rimeaddr_copy(&n->addr, addr);

		/* Place the neighbor on the neighbor list. */
		list_add(neighbors_list, n);

		/* set its position */
		n->pos.x = pos.x;
		n->pos.y = pos.y;

		PRINTF_LOG("Node %d.%d added neighbor %d.%d \n", rimeaddr_node_addr.u8[0], rimeaddr_node_addr.u8[1], addr->u8[0], addr->u8[1]);
	}

#ifdef SENSLAB
	if(n != NULL) {
		if(n->pos.x != pos.x || n->pos.y != pos.y){
			/* set its position */
			n->pos.x = pos.x;
			n->pos.y = pos.y;
			printf("DEBUG: node %d.%d updated neighbor %d.%d \n", rimeaddr_node_addr.u8[0], rimeaddr_node_addr.u8[1], addr->u8[0], addr->u8[1]);
			//count_neighbors++;
		}
	}
#endif

	return n;
};

/*****************************************************************************/
/* Broadcast Process                                                         */
/*****************************************************************************/

/*---------------------------------------------------------------------------*/
/* This function is called whenever a broadcast message is received. */
static void
broadcast_recv(struct broadcast_conn *c, const rimeaddr_t *from)
{
	struct kneighbor *n;
	kanycast_hdr_t broadcast_hdr;

	register_energy_cost(ENERGY_COST_RX(broadcast_pkt_t), RX, CONTROL);

	/* copying to avoid unalignment issues */
	memcpy(&broadcast_hdr, packetbuf_dataptr(), sizeof(kanycast_hdr_t));

	PRINTF("received broadcast from: %d.%d\n", from->u8[0], from->u8[1]);
	//PRINTF("ver: %d, type: %d\n", broadcast_hdr.ver, broadcast_hdr.type);

	if(broadcast_hdr.ver != KANGUROU_VERSION) {
		return;
	}

	/* have to memcpy to avoid memory unalignment :( */
	memcpy(&broadcast_pkt, packetbuf_dataptr(), sizeof(broadcast_pkt_t));

	if(broadcast_hdr.type == KANGUROU_BROADCAST_POS) {

		//PRINTF("From packet: ");
		//print_pos(broadcast_pkt.hdr.pos);

#ifdef FORCED_NEIGHBORS
		if(distance(&broadcast_pkt.hdr.pos, &own_pos)<=8.0){
			n = add_neighbor(broadcast_pkt.hdr.pos, (rimeaddr_t*)from);
		}
#else
		n = add_neighbor(broadcast_pkt.hdr.pos, (rimeaddr_t*)from);
#endif

		//PRINTF("updated neighbor: %d.%d, ", n->addr.u8[0], n->addr.u8[1]);
		//PRINTF("From memory: ");
		//print_pos(n->pos);
	}
}
/*---------------------------------------------------------------------------*/
/* Declare the broadcast  structures */
static struct broadcast_conn broadcast;
/* This is where we define what function to be called when a broadcast
   is received. We pass a pointer to this structure in the
   broadcast_open() call below. */
static const struct broadcast_callbacks broadcast_call = {broadcast_recv};
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(broadcast_process, ev, data)
{
	PROCESS_EXITHANDLER(broadcast_close(&broadcast);)

	PROCESS_BEGIN();

	static struct etimer et, et_bootstrap;

	broadcast_pos_event = process_alloc_event();

	broadcast_open(&broadcast, BROADCAST_CHANNEL, &broadcast_call);

	etimer_set(&et, CLOCK_SECOND + random_rand()%(MAX_NEIGHBORS*CLOCK_SECOND));
	etimer_set(&et_bootstrap, BOOTSTRAP_TIME*CLOCK_SECOND + random_rand()%(MAX_NEIGHBORS*CLOCK_SECOND));

	while(1) {
		PROCESS_WAIT_EVENT();
		// uint16_t period = clock_seconds() < BOOTSTRAP_TIME ? BROADCAST_PERIOD : 2*BROADCAST_PERIOD;
		if(etimer_expired(&et)) {
			/* Send a broadcast every BROADCAST_PERIOD */
			if(etimer_expired(&et_bootstrap)){
				etimer_set(&et, CLOCK_SECOND*BROADCAST_PERIOD + random_rand()%((BROADCAST_PERIOD+1)*CLOCK_SECOND));
			}
			else{
				etimer_set(&et, CLOCK_SECOND*MAX_NEIGHBORS + random_rand()%((MAX_NEIGHBORS+1)*CLOCK_SECOND));
			}

			PRINTF("broadcasting at %lu\n", clock_seconds());
			//PRINTF("next broadcast at: %lu\n", etimer_expiration_time(&et)/CLOCK_SECOND);

			// prepare the broadcast packet
			broadcast_pkt.hdr.ver = KANGUROU_VERSION;
			broadcast_pkt.hdr.type = KANGUROU_BROADCAST_POS;
			broadcast_pkt.hdr.pos.x = own_pos.x;
			broadcast_pkt.hdr.pos.y = own_pos.y;

			// PRINTF("bcast s: %d, %d, %d\n", broadcast_pkt.hdr.ver, broadcast_pkt.hdr.type, broadcast_pkt.hdr.len );

			/* log the time of the broadcast */
			// PRINTF("[BC] @%lu\n", clock_seconds());

			packetbuf_copyfrom(&broadcast_pkt, sizeof(broadcast_pkt_t));

			register_energy_cost(ENERGY_COST_TX_CTRL(MAX_COMMUNICATION_RANGE, broadcast_pkt_t), TX, CONTROL);

			broadcast_send(&broadcast);
		}
	}

	PROCESS_END();
}

/*---------------------------------------------------------------------------*/

/*****************************************************************************/
/* Multihop Process                                                          */
/*****************************************************************************/

/*---------------------------------------------------------------------------*/
/*
 * This function is called at the final recipient of the message.
 */
static void
recv(struct multihop_conn *c, const rimeaddr_t *sender,
		const rimeaddr_t *prevhop,
		uint8_t hops)
{
	PRINTF("Multihop message received\n");

	reading_pkt_t reading_pkt;

	memcpy(&reading_pkt, packetbuf_dataptr(), sizeof(reading_pkt_t));

	if(reading_pkt.reading_hdr.hdr.ver != KANGUROU_VERSION) {
		return;
	}

	if(reading_pkt.reading_hdr.hdr.type == KANGUROU_MULTIHOP) {
		//TODO move the packet to the application layer
		rimeaddr_t sender_rcv, prevhop_rcv;

		rimeaddr_copy(&sender_rcv, sender);
		rimeaddr_copy(&prevhop_rcv, prevhop);

		register_energy_cost(ENERGY_COST_RX(reading_pkt_t),RX, DATA);
		packet_received(&reading_pkt);

		return;
	}


	return;
}
/*---------------------------------------------------------------------------*/
/*
 * This function is called to forward a packet. The function picks the
 * neighbor closest to the destination from the neighbor list. If no
 * neighbor is found, the function returns NULL to signal to the multihop layer
 * that the packet should be dropped.
 */
static rimeaddr_t *
forward(struct multihop_conn *c,
		const rimeaddr_t *originator, const rimeaddr_t *dest,
		const rimeaddr_t *prevhop, uint8_t hops)
{
	static reading_pkt_t reading_pkt;
	rimeaddr_t sinks[MAX_SINKS];
	int i;

	memcpy(&reading_pkt, packetbuf_dataptr(), sizeof(reading_pkt_t));

	if(reading_pkt.reading_hdr.prep==0 && !rimeaddr_cmp(&reading_pkt.reading_hdr.hdr.originator, &rimeaddr_node_addr)){
		register_energy_cost(ENERGY_COST_RX(reading_pkt_t),RX, DATA);
		reading_pkt.energy_cost=reading_pkt.energy_cost+ENERGY_COST_RX(reading_pkt_t);
	}

	my_all_local_nodes = all_local_nodes();

	for(i=0; i<MAX_SINKS; i++){
		if(reading_pkt.reading_hdr.sinks[i]==1){
			rimeaddr_copy(&sinks[i], &my_all_local_nodes->addr[i]);
		}
		else{
			rimeaddr_copy(&sinks[i], &rimeaddr_null);
		}
	}

	PRINTF("**Multihop process started!** | reading_pkt.reading_hdr.prep=%d\n", reading_pkt.reading_hdr.prep);

	if(reading_pkt.reading_hdr.k<=0){
		return NULL;
	}

	if(reading_pkt.reading_hdr.prep==0){

		PRINTF("**Calculating route!**\n");

		if(get_index_by_addr(&rimeaddr_node_addr, sinks, MAX_SINKS)>=0){
			packet_received(&reading_pkt);
		}

		//clock_time_t calc_time;
		//calc_time = (clock_time_t)(SYSNOW);

		kanycasting(&reading_pkt.reading_hdr.src, &reading_pkt.reading_hdr.prev, reading_pkt.reading_hdr.sinks, reading_pkt.reading_hdr.k, reading_pkt.reading_hdr.dist, &reading_pkt.reading_hdr.ref, reading_pkt.reading_hdr.ref_pos, &reading_pkt, hops, &reading_pkt.reading_hdr.ref_dest);

		//calc_time = (clock_time_t)(SYSNOW - calc_time);
		//printf("Processing time of the kanycasting() function for a received packet: %lu ms\n", calc_time);

		return NULL;
	}
	else{
		reading_pkt.reading_hdr.prep = 0;

		clock_time_t latency;
		latency = (clock_time_t)(((long int)SYSNOW + my_offset) - (long int)reading_pkt.timestamp);

		//printf("\nDEBUG: timestamp: %lu , %lu | latency: %lu , %lu | offset: %lu , %lu \n\n", reading_pkt.timestamp, TICKSTOMS(reading_pkt.timestamp), latency, TICKSTOMS(latency), my_offset, TICKSTOMS(my_offset));

		TIMESTAMP;
		PRINTF_LOG("[KanG] Forwarding packet ID: %d-%d-%lu to %d.%d, ", reading_pkt.reading_hdr.hdr.originator.u8[0], reading_pkt.reading_hdr.hdr.originator.u8[1], reading_pkt.timestamp_original, reading_pkt.reading_hdr.dest.u8[0], reading_pkt.reading_hdr.dest.u8[1]);
		PRINTF_LOG("k: %d, sinks: ", reading_pkt.reading_hdr.k);
		for(i=0; i<MAX_SINKS; i++){
			if(!rimeaddr_cmp(&sinks[i], &rimeaddr_null)){
				PRINTF_LOG("%d.%d, ", sinks[i].u8[0], sinks[i].u8[1]);
			}
		}
		PRINTF_LOG("hops: %d | Latency: %lu ms | ", reading_pkt.reading_hdr.hdr.hops, TICKSTOMS(latency));
		PRINTF_LOG("Originator: %d.%d | Packet size: %d bytes", reading_pkt.reading_hdr.hdr.originator.u8[0], reading_pkt.reading_hdr.hdr.originator.u8[1], PKTSIZE(reading_pkt_t));
		if(reading_pkt.reading_hdr.recovery_flag==1){
			PRINTF_LOG("| Recovery");
		}
		PRINTF_LOG("| dest: %d.%d", reading_pkt.reading_hdr.ref_dest.u8[0], reading_pkt.reading_hdr.ref_dest.u8[1]);
		PRINTF_LOG("\n");

		//PRINTF("**Sending message!**\n");

		struct kneighbor *ind=get_neighbor_pointer_by_addr(&reading_pkt.reading_hdr.dest);
		register_energy_cost(ENERGY_COST_TX(&own_pos,&ind->pos,reading_pkt_t), TX, DATA);
		register_forwarded_packet();
		reading_pkt.energy_cost=reading_pkt.energy_cost+ENERGY_COST_TX(&own_pos,&ind->pos,reading_pkt_t);

		/* Copy the reading to the packet buffer. */
		packetbuf_copyfrom(&reading_pkt, sizeof(reading_pkt_t));

		if(get_index_by_addr(&reading_pkt.reading_hdr.dest, sinks, MAX_SINKS)>=0 && reading_pkt.reading_hdr.k==1){
			packetbuf_set_addr(PACKETBUF_ADDR_ERECEIVER, &reading_pkt.reading_hdr.dest);
		}

		  packetbuf_set_attr(PACKETBUF_ATTR_RELIABLE, 1);
		  packetbuf_set_attr(PACKETBUF_ATTR_PACKET_TYPE, PACKETBUF_ATTR_PACKET_TYPE_DATA);
		  packetbuf_set_attr(PACKETBUF_ATTR_MAX_MAC_TRANSMISSIONS, 10);

		//PRINTF_LOG("BUFFER: %d.%d, PACKET: %d.%d \n", reading_pkt.reading_hdr.dest.u8[0], reading_pkt.reading_hdr.dest.u8[1], ((reading_pkt_t*)(packetbuf_dataptr()))->reading_hdr.dest.u8[0], ((reading_pkt_t*)(packetbuf_dataptr()))->reading_hdr.dest.u8[1]);

		return &reading_pkt.reading_hdr.dest;
	}


	return NULL;
}

/*---------------------------------------------------------------------------*/

allnodes * all_local_nodes(){

	static allnodes all;

	int8_t i, exist;
	struct kneighbor *ind;
	struct ksink *s;
	list_t list_n = get_neighbor_list();

	for(i=0;i<ALL_LOCAL_NODES;i++){
		rimeaddr_copy(&all.addr[i], &rimeaddr_null);
		all.issink[i] = -1;
		all.isneighbor[i] = -1;
	}

	i=0;
	for(s = list_head(my_sink_list); s != NULL; s = list_item_next(s)) {
		if(!rimeaddr_cmp(&s->addr, &rimeaddr_null)){
			rimeaddr_copy(&all.addr[i], &s->addr);
			all.pos[i] = s->pos;
			all.issink[i] = 1;
			all.isneighbor[i] = 0;
			if(rimeaddr_cmp(&s->addr, &rimeaddr_node_addr)){
				all.self = i;
			}
			//printf("LOG: Sink: %d.%d -> addr: %d\n", all.addr[i].u8[0], all.addr[i].u8[1], i);
			i++;
		}
	}

	for(ind = list_head(list_n); ind != NULL; ind = list_item_next(ind)) {
		if(!rimeaddr_cmp(&ind->addr, &rimeaddr_null)){
			exist=get_index_by_addr(&ind->addr, all.addr, ALL_LOCAL_NODES);
			if(exist==-1){
				rimeaddr_copy(&all.addr[i], &ind->addr);
				all.pos[i] = ind->pos;
				all.issink[i] = 0;
				all.isneighbor[i] = 1;
				//printf("LOG: Neighbor: %d.%d -> addr: %d\n", all.addr[i].u8[0], all.addr[i].u8[1], i);
				i++;
			}
			else{
				all.isneighbor[exist] = 1;
				//printf("LOG: Neighbor/Sink: %d.%d -> addr: %d\n", all.addr[exist].u8[0], all.addr[exist].u8[1], exist);
			}
		}
	}

	if(!i_am_sink){
		rimeaddr_copy(&all.addr[ALL_LOCAL_NODES-1], &rimeaddr_node_addr);
		all.pos[ALL_LOCAL_NODES-1] = own_pos;
		all.issink[ALL_LOCAL_NODES-1] = i_am_sink;
		all.isneighbor[ALL_LOCAL_NODES-1] = 0;
		all.self = ALL_LOCAL_NODES-1;
	}

	return &all;
}

/*---------------------------------------------------------------------------*/

void new_packet_generation(void *data){

	process_post(&multihop_process, newpacket_event, data);
	process_poll(&multihop_process);

	//UPDATE NODE METRICS
	register_generated_packet();
}

/*---------------------------------------------------------------------------*/

void send_packet_multihop(void *data){

	/* Send the broadcast message with the sink void list */
	process_post(&multihop_process, multipacket_event, data);
}

/*---------------------------------------------------------------------------*/

void packet_received(reading_pkt_t *reading_pkt_in){

	clock_time_t now, latency;

	now = (clock_time_t)((long int)SYSNOW + my_offset);
	latency = now - reading_pkt_in->timestamp;

	register_received_packet();
	register_latency(latency);

	TIMESTAMP;
	PRINTF_LOG("[KanG] Latency: %lu ms | ", TICKSTOMS(latency));
	PRINTF_LOG("Originator: %d.%d | ", reading_pkt_in->reading_hdr.hdr.originator.u8[0], reading_pkt_in->reading_hdr.hdr.originator.u8[1]);
	PRINTF_LOG("Previous Sender: %d.%d | ", reading_pkt_in->reading_hdr.prev.u8[0], reading_pkt_in->reading_hdr.prev.u8[1]);
	PRINTF_LOG("Hops: %d | ", reading_pkt_in->reading_hdr.hdr.hops);
	PRINTF_LOG("Consumed Energy (TX+RX): ");
	NOEXP(reading_pkt_in->energy_cost);
	PRINTF_LOG(" J | Packet ID: %d-%d-%lu | ", reading_pkt_in->reading_hdr.hdr.originator.u8[0], reading_pkt_in->reading_hdr.hdr.originator.u8[1], reading_pkt_in->timestamp_original);
	PRINTF_LOG("Packet Size: %d bytes\n", PKTSIZE(reading_pkt_t));

}

/*---------------------------------------------------------------------------*/
/* Declare multihop structures */
static const struct multihop_callbacks multihop_call = {recv, forward};
static struct multihop_conn multihop;
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(multihop_process, ev, data)
{
	PROCESS_EXITHANDLER(multihop_close(&multihop);)

		  PROCESS_BEGIN();

	reading_hdr_t reading_hdr;
	static reading_pkt_t reading_pkt;
	static rimeaddr_t to;
	static struct etimer et;

	etimer_stop(&et);

	multipacket_event = process_alloc_event();
	newpacket_event = process_alloc_event();

	/* Activate the button sensor. We use the button to drive traffic -
	     when the button is pressed, a packet is sent. */
		//SENSORS_ACTIVATE(button_sensor);


	/* Open a multihop connection on Rime channel MULTIHOP_CHANNEL. */
	multihop_open(&multihop, MULTIHOP_CHANNEL, &multihop_call);

	/* Set the Rime address of the final receiver of the packet to
     254.254 as we rely on the x,y coordinates to deliver the packet.
     once the recipient is found the address of the destination is updated. */
	to.u8[0] = 254;
	to.u8[1] = 254;

	/* Loop forever, send a packet when the button is pressed. */
	while(1) {
		/* Wait until we get a sensor event with the button sensor as data. */
		PROCESS_WAIT_EVENT();
		if (ev == newpacket_event) {
			/* this event is just for debug */
			PRINTF("Creating multihop packet\n");

			/* prepare the multihop packet */
			reading_hdr.hdr.ver = KANGUROU_VERSION;
			reading_hdr.hdr.type = KANGUROU_MULTIHOP;
			reading_hdr.hdr.pos.x = own_pos.x;
			reading_hdr.hdr.pos.y = own_pos.y;
			reading_hdr.hdr.hops = 0;
			rimeaddr_copy(&reading_hdr.hdr.originator, &rimeaddr_node_addr);
			reading_hdr.k = MAX_K;
			reading_hdr.prep = 0;

			prepare_reading_pkt(&reading_hdr, &reading_pkt);
			reading_pkt.reading_hdr.recovery_flag = 0;

			/* Send the packet. */
			rimeaddr_t tmp_null;
			rimeaddr_copy(&tmp_null, &rimeaddr_null);
			my_all_local_nodes = all_local_nodes();

			//clock_time_t calc_time;
			//calc_time = (clock_time_t)(SYSNOW);

			kanycasting(&rimeaddr_node_addr, &rimeaddr_node_addr, my_all_local_nodes->issink, MAX_K, -1, &rimeaddr_node_addr, own_pos, &reading_pkt, 0, &tmp_null);

			//calc_time = (clock_time_t)(SYSNOW - calc_time);
			//printf("Processing time of the kanycasting() function for a created packet: %lu ms\n", calc_time);

		}
		else if (ev == multipacket_event) {
				PRINTF("Multipacket event!\n");

				/* Copy the reading to the packet buffer. */
				packetbuf_copyfrom((reading_pkt_t *)data, sizeof(reading_pkt_t));

				/* Send the packet. */
				multihop_send(&multihop, &to);
			}
	}

	PROCESS_END();
}

/*---------------------------------------------------------------------------*/

/*****************************************************************************/
/* Boot Process                                                              */
/*****************************************************************************/

void receive_info(char *data_copy){

	int c;
	char str[SERIAL_LINE_CONF_BUFSIZE], *aux;
	pos_t rcvd_pos = {0.0 , 0.0};
	rimeaddr_t rime_addr;
	clock_time_t ref=0;

	//Initialize string
	memset(str, '\0', sizeof(char)*SERIAL_LINE_CONF_BUFSIZE);
	memcpy(str, data_copy, SERIAL_LINE_CONF_BUFSIZE*sizeof(char));

	c=0;
	while(str[c]!='#'){
		//aux=&str[c];
		//printf("c: %d, STRING: %s\n", c, aux);
		if(str[c]=='t'){
			c+=2;
			aux=&str[c];
			while(str[c]!='|') c++;
			str[c]='\0';
			ref = (clock_time_t)atol((char *)aux);
			//memcpy(str, data_copy, SERIAL_LINE_CONF_BUFSIZE*sizeof(char));
			printf("Reference time received: %lu.\n", ref);
		}
#ifdef SENSLAB
		if(str[c]=='i'){
			c+=2;
			aux=&str[c];
			while(str[c]!='|') c++;
			str[c]='\0';
			myid = (uint8_t)atoi((char *)aux);
			//memcpy(str, data_copy,SERIAL_LINE_CONF_BUFSIZE*sizeof(char));
			printf("Received Node ID: %d.\n", myid);
		}
		if(str[c]=='v'){
			while(str[c]!=':') c++;
			c++;
			aux=&str[c];
			while(str[c]!='.') c++;
			str[c]='\0';
			rime_addr.u8[0]=(unsigned char)atoi((char *)aux);
			//memcpy(str, data_copy,SERIAL_LINE_CONF_BUFSIZE*sizeof(char));
			c+=1;
			aux=&str[c];
			while(str[c]!='|') c++;
			str[c]='\0';
			rime_addr.u8[1]=(unsigned char)atoi((char *)aux);
			//memcpy(str, data_copy,SERIAL_LINE_CONF_BUFSIZE*sizeof(char));
			rcvd_pos.x=own_pos.x;
			rcvd_pos.y=own_pos.y;
			if(add_neighbor(rcvd_pos, &rime_addr)==NULL){
				printf("ERROR: Neighbor %d.%d was not included.\n", rime_addr.u8[0], rime_addr.u8[1]);
			}
		}
#endif
		if(str[c]=='k'){
			c+=2;
			aux=&str[c];
			while(str[c]!='|') c++;
			str[c]='\0';
			//received_max_k = (uint8_t)stoi((char *)aux);
			received_max_k = (uint8_t)atoi((char *)aux);
			//memcpy(str, data_copy,SERIAL_LINE_CONF_BUFSIZE*sizeof(char));
			printf("Maximum K received: %d.\n", received_max_k);
		}
		if(str[c]=='n'){
			c+=2;
			aux=&str[c];
			while(str[c]!=';') c++;
			str[c]='\0';
			c++;
#ifdef SENSLAB
			own_pos.x = stof((char *)aux);
#else
			own_pos.x = atof((char *)aux);
#endif
			//memcpy(str, data_copy,SERIAL_LINE_CONF_BUFSIZE*sizeof(char));
			aux=&str[c];
			while(str[c]!='|') c++;
			str[c]='\0';
#ifdef SENSLAB
			own_pos.y = stof((char *)aux);
#else
			own_pos.y = atof((char *)aux);
#endif
			//memcpy(str, data_copy,SERIAL_LINE_CONF_BUFSIZE*sizeof(char));
			printf("Coords X,Y received.\n");
		}
		if(str[c]=='s'){
			c+=2;
			aux=&str[c];
			while(str[c]!='.') c++;
			str[c]='\0';
			rime_addr.u8[0]=(unsigned char)atoi((char *)aux);
			//memcpy(str, data_copy,SERIAL_LINE_CONF_BUFSIZE*sizeof(char));
			c+=1;
			aux=&str[c];
			while(str[c]!=';') c++;
			str[c]='\0';
			rime_addr.u8[1]=(unsigned char)atoi((char *)aux);
			//memcpy(str, data_copy,SERIAL_LINE_CONF_BUFSIZE*sizeof(char));
			c++;
			aux=&str[c];
			while(str[c]!=';') c++;
			str[c]='\0';
			c++;
#ifdef SENSLAB
			rcvd_pos.x = stof((char *)aux);
#else
			rcvd_pos.x = atof((char *)aux);
#endif
			//memcpy(str, data_copy,SERIAL_LINE_CONF_BUFSIZE*sizeof(char));
			aux=&str[c];
			while(str[c]!='|') c++;
			str[c]='\0';
#ifdef SENSLAB
			rcvd_pos.y = stof((char *)aux);
#else
			rcvd_pos.y = atof((char *)aux);
#endif
			//memcpy(str, data_copy,SERIAL_LINE_CONF_BUFSIZE*sizeof(char));
			if(add_sink(rcvd_pos, &rime_addr)==NULL){
				printf("Sink %d.%d was not included.\n", rime_addr.u8[0], rime_addr.u8[1]);
			}
			else{
				printf("Sink %d.%d was included.\n", rime_addr.u8[0], rime_addr.u8[1]);
				if(rimeaddr_cmp(&rimeaddr_node_addr, &rime_addr)){
					i_am_sink = 1;
				}
			}
		}
		c++;
	}

	if(ref>0){
		// Calculate the offset for network synchronization
		my_offset = (long int)ref - my_offset;
		printf("My offset: %ld, External Ref.: %lu, Now: %lu, Ext_now: %lu \n", my_offset, ref, SYSNOW, TICKSTOMS((long int)SYSNOW+my_offset));
	}
}

/*---------------------------------------------------------------------------*/

void
kangurou_init() {

	//starts the serial line process
	serial_line_init();

	process_start(&boot_process, NULL);
}

/*---------------------------------------------------------------------------*/

PROCESS_THREAD(boot_process, ev, data)
{

	PROCESS_BEGIN();

	char data1[SERIAL_LINE_CONF_BUFSIZE];
#ifndef SENSLAB
	char data2[SERIAL_LINE_CONF_BUFSIZE], data3[SERIAL_LINE_CONF_BUFSIZE], data4[SERIAL_LINE_CONF_BUFSIZE];
#endif

	received_max_k = 0;

	/* Initialize the memory for the sink table entries. */
	memb_init(&sinks_memb);

	/* Initialize the list used for the sink table. */
	list_init(my_sink_list);

	/* Initialize the memory for the neighbor table entries. */
	memb_init(&neighbors_memb);

	/* Initialize the list used for the neighbor table. */
	list_init(neighbors_list);

	memset(data1, '\0', sizeof(char)*SERIAL_LINE_CONF_BUFSIZE);
#ifndef SENSLAB
	memset(data2, '\0', sizeof(char)*SERIAL_LINE_CONF_BUFSIZE);
	memset(data3, '\0', sizeof(char)*SERIAL_LINE_CONF_BUFSIZE);
	memset(data4, '\0', sizeof(char)*SERIAL_LINE_CONF_BUFSIZE);
#endif

	// SINK DETECTION
	// get the Sink list and coordinates from COOJA
	// get the node coordinates from cooja, adapted from:
	// https://sourceforge.net/p/contiki/mailman/message/34696644/
	printf("Waiting for node coordinates from Cooja...\n");
	PROCESS_WAIT_EVENT_UNTIL(ev == serial_line_event_message);
	my_offset=SYSNOW;
	memcpy(data1,(char *)data,SERIAL_LINE_CONF_BUFSIZE*sizeof(char));
#ifndef SENSLAB
	printf("data1: %s\n", data1);
#endif
	printf("Received-1...\n");

#ifndef SENSLAB
	PROCESS_WAIT_EVENT_UNTIL(ev == serial_line_event_message);
	memcpy(data2,(char *)data,SERIAL_LINE_CONF_BUFSIZE*sizeof(char));
	printf("data2: %s\n", data2);
	printf("Received-2...\n");

  	PROCESS_WAIT_EVENT_UNTIL(ev == serial_line_event_message);
  	memcpy(data3,(char *)data,SERIAL_LINE_CONF_BUFSIZE*sizeof(char));
	printf("data3: %s\n", data3);
	printf("Received-3...\n");

	PROCESS_WAIT_EVENT_UNTIL(ev == serial_line_event_message);
	memcpy(data4,(char *)data,SERIAL_LINE_CONF_BUFSIZE*sizeof(char));
	printf("data4: %s\n", data4);
	printf("Received-4...\n");
#endif

	receive_info(data1);
#ifndef SENSLAB
	receive_info(data2);
	receive_info(data3);
	receive_info(data4);
#endif

#ifdef SENSLAB
	count_neighbors = 1;
#endif

	//Initialize the metrics variables
	my_metrics_init(i_am_sink);

	/* start broadcast process */
	process_start(&broadcast_process, NULL);

	/* start runicast process */
	process_start(&multihop_process, NULL);

	printf("Booting completed: %d.%d\n", rimeaddr_node_addr.u8[0], rimeaddr_node_addr.u8[1]);

	PROCESS_END();
}
/*---------------------------------------------------------------------------*/
