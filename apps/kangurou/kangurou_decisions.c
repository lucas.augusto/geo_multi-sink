/**
 * \file
 *         kangurou_decisions.c
 * \author
 *         Lucas Leao <lucas.leao@femto-st.fr>
 *
 *  Based on the KanGuRou routing protocol from:
 *  Nathalie Mitton
 *  http://researchers.lille.inria.fr/~mitton/home.html
 *
 */

/* contiki includes */
#include "net/rime.h"

/* standard library includes */
#include <stdio.h> /* For printf() */
#include <string.h> /* For memcpy */
#include <stdlib.h>
#include <math.h>

/* project includes */
#include "kangurou.h"
#include "kangurou_decisions.h"
#include "kangurou_functions.h"
#include "kangurou_packets.h"
#include "support.h"

#define PI 3.14159265

/* Set DEBUG to 1 to include debug output */
#define DEBUG 0
#if DEBUG
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

#define ISVALID(addr)			(get_all_local_nodes()->issink[addr]==1?1:(get_all_local_nodes()->isneighbor[addr]==1?1:0))

#define MAX_PKT					MAX_SINKS

#define MAX_ENTRIES             ALL_LOCAL_NODES

//sNeighborsList p[ALL_LOCAL_NODES];
//float *l[ALL_LOCAL_NODES];

/*********k-anycasting****************/
void kanycasting(rimeaddr_t *src_addr, rimeaddr_t *prev_addr, int8_t *sinks, int8_t local_k, float d, rimeaddr_t *r_addr, pos_t ref_pos, reading_pkt_t *reading_pkt, int hops, rimeaddr_t *dest_addr){

	int8_t Tree[ALL_LOCAL_NODES], recovery_flag;
	int8_t addr, addr2, newsplit = FALSE, prev, i, dest;
	int8_t local_sinks[ALL_LOCAL_NODES];
	//array1d targets[ALL_LOCAL_NODES];
	s_tree res, res1;
	float dist;

	static reading_pkt_t pkt[MAX_PKT];
	memset(pkt, 0, sizeof(reading_pkt_t)*MAX_PKT);
	static uint8_t pkt_count=0;

	allnodes *all = get_all_local_nodes();

	if(list_length(get_neighbor_list())<=0){
		PRINTF("No neighbors found! \n");
		return;
	}

	/*************initialization*********************/
	for(i=0;i<ALL_LOCAL_NODES;i++){
		local_sinks[i] = sinks[i];
		//if(all->addr[i].u8[0]!=0 && i<MAX_SINKS)
			//printf("S(%d.0)=%d | ", all->addr[i].u8[0], sinks[i]);
		//targets[i].m = NULL;
	}

	static array1d *targets=NULL;

	if(targets==NULL){
		targets = (array1d *)malloc(sizeof(array1d));
		targets->next=NULL;
		as_reset(targets);
	}

	//printf("\n");

	prev = get_index_by_addr(prev_addr, all->addr, ALL_LOCAL_NODES);
	dest = get_index_by_addr(dest_addr, all->addr, ALL_LOCAL_NODES);
	/************************************************/

	//printf("*****START k-anycasting in node %d.%d towards %d sinks \n", rimeaddr_node_addr.u8[0],rimeaddr_node_addr.u8[1],local_k);

	if (is_sink()){
		//printf("*******%d.0 is a sink\n", rimeaddr_node_addr.u8[0]);
		local_k --;
		local_sinks[all->self]=0;
	}

	if (local_k == 0){
		return;
	}

#ifdef CALCULATION_TIME
		clock_time_t first_mark;
		first_mark = (clock_time_t)(SYSNOW+get_node_offset());
#endif

	//decide how many messages, local node should send
	k_MST(Tree, all->self, local_sinks, local_k);

/*	printf("kanycast (k: %d): ", local_k);
	for(i=0;i<ALL_LOCAL_NODES;i++){
		if(all->addr[Tree[i]].u8[0]!=0)
			printf("T(%d.0)=%d.0 | ", all->addr[i].u8[0], all->addr[Tree[i]].u8[0]);
	}
	printf("\n");
*/



	PRINTF("*********First k-mst computing ok\n");

	AllocateSinks(targets, local_sinks, Tree);

	PRINTF("************allocate sinks ok\n");

	res = nbTargets(all->self, Tree);

	//printf("min_weight (%d.%d) >= (%d.%d) node_weight\n", (int)d, decimals(d), (int)res.weight, decimals(res.weight));
	if ((d < 0) || (d > res.weight)){
		//packet is either in greedy mode or can exit the recovery mode
		i=0;
		addr = ALL_LOCAL_NODES;

		int8_t temp[ALL_LOCAL_NODES], a;

		while(addr --){
			if ((Tree[addr] == all->self) && (addr!=all->self)){

				//addr is a branche in the tree rooted in c->node
				PRINTF("*******split towards sink %d.0\n", all->addr[addr].u8[0]);

				if (newsplit==FALSE){
					newsplit = TRUE;
				}

				res1 = nbTargets(addr, Tree);
				PRINTF("Greedy: addr = %d.0, res1.size = %d\n",all->addr[addr].u8[0], res1.size);

				for(a=0; a<ALL_LOCAL_NODES; a++){
					if(a<MAX_SINKS){
						//temp[a] = targets[addr].m[a];
						temp[a] = as_get(targets, addr, a);
					}
					else{
						temp[a] = -1;
					}
				}

				//addr2 = greedy(targets[addr].m, (res1.weight+distance(get_node_position(all->self),get_node_position(addr))), res1.size);
				addr2 = greedy(temp, (res1.weight+distance(get_node_position(all->self),get_node_position(addr))), res1.size);

				recovery_flag = 0;
				dist = -1.0;
				if (addr2 == -1){
					PRINTF("Greedy has failed - launch recovery\n");

					addr2 = recovery(addr, own_pos, addr);

					dist = res.weight;
					recovery_flag = 1;
				}
				PRINTF("Sending to %d.0 with k: %d || dist = %d || Ref: (%d , %d)\n", all->addr[addr2].u8[0], res1.size, (int)dist, (int)own_pos.x, (int)own_pos.y);
				PRINTF("********va envoyer le message\n");
				memcpy(&pkt[pkt_count], reading_pkt, sizeof(reading_pkt_t));

#ifdef CALCULATION_TIME
				pkt[pkt_count].timestamp = (clock_time_t)(pkt[pkt_count].timestamp + (SYSNOW + get_node_offset() - first_mark));
				//printf("SYSNOW: %lu | pkt[pkt_count].timestamp: %lu | Latency: %lu \n", SYSNOW+get_node_offset(), pkt[pkt_count].timestamp, (SYSNOW+get_node_offset()-pkt[pkt_count].timestamp));
#endif

				for(a=0; a<ALL_LOCAL_NODES; a++){
					if(a<MAX_SINKS){
						//temp[a] = targets[addr].m[a];
						temp[a] = as_get(targets, addr, a);
					}
					else{
						temp[a] = 0;
					}
				}

				//Send(&all->addr[addr2], res1.size, targets[addr].m, dist, &rimeaddr_node_addr, own_pos, src_addr, &pkt[pkt_count], &all->addr[addr], recovery_flag);
				Send(&all->addr[addr2], res1.size, temp, dist, &rimeaddr_node_addr, own_pos, src_addr, &pkt[pkt_count], &all->addr[addr], recovery_flag);

				pkt_count++;
				if(pkt_count==MAX_PKT){
					pkt_count = 0;
				}
				PRINTF("********message envoye\n");
			}
		}
	}
	else {

		//packet was in recovery mode and can not exit this mode
		PRINTF("Packet was in recovery mode and can not exit this mode. Ref: (%d , %d)\n", (int)ref_pos.x, (int)ref_pos.y);
		addr2 = recovery(prev, ref_pos, dest);
		dist = d;
		memcpy(&pkt[0], reading_pkt, sizeof(reading_pkt_t));
		PRINTF("********R va envoyer le message\n");
		Send(&all->addr[addr2], local_k, sinks, dist, r_addr, ref_pos, src_addr, &pkt[0], &all->addr[dest], 1);
		PRINTF("********R message envoye\n");

	}

	return;

}//end k-anycasting

/********************************************************/
void Send(rimeaddr_t *dest, int8_t k, int8_t *sinks, float d, rimeaddr_t *ref, pos_t ref_pos, rimeaddr_t *src, reading_pkt_t *reading_pkt, rimeaddr_t *ref_dest, int8_t recovery_flag){

	//Source address
	rimeaddr_copy(&reading_pkt->reading_hdr.src, src);

	//Previous address
	rimeaddr_copy(&reading_pkt->reading_hdr.prev, &rimeaddr_node_addr);

	//Distance
	reading_pkt->reading_hdr.dist = d;

	//Destination address
	rimeaddr_copy(&reading_pkt->reading_hdr.dest, dest);

	//Available sinks
	memcpy(reading_pkt->reading_hdr.sinks,sinks,ALL_LOCAL_NODES*sizeof(int8_t));

	//number of sinks to be reached
	reading_pkt->reading_hdr.k = k;

	//Reference node
	rimeaddr_copy(&reading_pkt->reading_hdr.ref, ref);
	reading_pkt->reading_hdr.ref_pos.x = ref_pos.x;
	reading_pkt->reading_hdr.ref_pos.y = ref_pos.y;
	reading_pkt->reading_hdr.recovery_flag = recovery_flag;

	//Packet is ready to be sent
	reading_pkt->reading_hdr.prep = 1;

	//Reference final destination
	rimeaddr_copy(&reading_pkt->reading_hdr.ref_dest, ref_dest);

	reading_pkt->reading_hdr.hdr.hops = reading_pkt->reading_hdr.hdr.hops + 1;

	PRINTF("Packet ready to be sent.\n");

	send_packet_multihop(reading_pkt);

	return;

}//end send


void sm_insert(array3d *first, int8_t p, int8_t k, int8_t node, int8_t value){

	array3d *free=NULL, *exist=NULL, *last_node=NULL, *curr=first;
	//static int count=1;

	//printf("list[");
	while(curr!=NULL){
		//printf("%d,%d,%d,%d | ", curr->p, curr->k, curr->node, curr->value);
		if(curr->p==-1 && curr->k==-1 && curr->node==-1 && curr->value==-1 && free==NULL){
			free=curr;
			//printf(" free:%p ", free);
		}
		if(curr->p==p && curr->k==k && curr->node==node && node!=-1 && exist==NULL){
			exist=curr;
			//printf(" exist:%p ", exist);
		}
		if(curr->p==p && curr->k==k && curr->node>=0 && node==-1 && (last_node==NULL || (last_node->node<curr->node))){
			last_node=curr;
			//printf(" last:%p ", last);
		}
		if(curr->next!=NULL){
			curr=curr->next;
		}
		else{
			break;
		}
	}
	//printf(" ] \n ");

	if(node==-1){
		if(last_node==NULL){
			node=0;
		}
		else{
			node=last_node->node+1;
		}
		//printf("LAST (node: %d) - ", node);
	}

	if(exist!=NULL){
		exist->value=value;
		//printf("Value updated (exist=%p)! p=%d, k=%d, node=%d, value=%d\n", exist, p, k, node, value);
	}
	else{
		if(free==NULL){
			free = (array3d *)malloc(sizeof(array3d));
			curr->next = free;
			free->next=NULL;
			//count++;
			//printf("[sm_insert] No free space found! Creating new entry! Count: %d\n", count);
		}
		free->p=p;
		free->k=k;
		free->node=node;
		free->value=value;
		//printf("Value saved to free (free=%p) space! p=%d, k=%d, node=%d, value=%d\n", free, p, k, node, value);
	}

	return;
}

void sm_copy(array3d *first, int8_t p1, int8_t k1, int8_t p2, int8_t k2){

	array3d *curr=first;

	while(curr!=NULL){
		if(curr->p==p1 && curr->k==k1){
			sm_insert(first, p2, k2, curr->node, curr->value);
			//printf("sm_insert(first:%p, p2:%d, k2:%d, curr->node:%d, curr->value:%d)\n", first, p2, k2, curr->node, curr->value);
		}
		curr=curr->next;
	}

	return;
}

int8_t sm_get(array3d *first, int8_t p, int8_t k, int8_t node){

	array3d *curr=first;

	while(curr!=NULL){
		if(curr->p==p && curr->k==k && curr->node==node){
			return curr->value;
		}
		curr=curr->next;
	}

	//printf("ERROR: Index not found!!!\n");

	return -1;
}

int8_t sm_exist(array3d *first, int8_t p, int8_t k){

	array3d *curr=first;

	while(curr!=NULL){
		if(curr->p==p && curr->k==k && curr->node>=0 && curr->node<=1 && curr->value>=0){
			return 1;
		}
		curr=curr->next;
	}

	return 0;
}

int8_t sm_count_used(array3d *first){

	array3d *curr=first;
	uint8_t count=0;

	while(curr!=NULL){
		if(curr->p>=0 && curr->k>=0 && curr->node>=0 && curr->value>=0){
			count++;
		}
		curr=curr->next;
	}

	return count;
}

void sm_reset(array3d *first){

	array3d *curr=first;//, *prev;

	//curr->p=-1;
	//curr->k=-1;
	//curr->node=-1;
	//curr->value=-1;
	//curr=curr->next;

	while(curr!=NULL){
		curr->p=-1;
		curr->k=-1;
		curr->node=-1;
		curr->value=-1;
		//prev=curr;
		curr=curr->next;
		//free(prev);
	}

	//first->next=NULL;

	return;
}


/******compute the k-mst**********/
void k_MST(int8_t *Tree, int8_t addr, int8_t *S, int8_t k){
	//compute the k-MST rooted in addr over nodes in S

	int8_t A[ALL_LOCAL_NODES], kk, i, j, w, m, x, v, z, node0, node1;
	float d, d1, ltemp;

	static float l[ALL_LOCAL_NODES][MAX_SINKS];
	static array3d *mp=NULL;

	if(mp==NULL){
		mp = (array3d *)malloc(sizeof(array3d));
		mp->next=NULL;
		sm_reset(mp);
	}

	//printf("------------------ Entered in function!\n");

	//p and l are global variables, because they are too large and they are causing memory overflow

	//PRINTF("--> Compute %d-MST rooted on node %d.0\n", k, get_all_local_nodes()->addr[addr].u8[0]);

	sm_reset(mp);

	kk = ALL_LOCAL_NODES;
	while(kk--){
		A[kk] = S[kk];
		//if(l[kk]!=NULL){
			for(z=0;z<MAX_K;z++){
				l[kk][z] = 1000.0;
			}
		//}
		Tree[kk] = -1;
	}

	Tree[addr] = addr;

	kk = k+1;
	while((kk--) && (kk>0)){

		//printf("---- RESET!\n");
		sm_reset(mp);
		memset(l, 1000.0, sizeof(float)*ALL_LOCAL_NODES*MAX_SINKS);

		v = ALL_LOCAL_NODES;
		while(v--){
			if(ISVALID(v)==1){
				if (A[v] == 1){
					node0 = closerInTree(v, Tree, 1);
					//calculer w, noeud dans l'arbre
					sm_insert(mp,v,0,0,node0);
					//if(l[v]==NULL){
						//l[v] = (float*)malloc(sizeof(float)*MAX_K);
						//for(z=0;z<MAX_K;z++){
						//	l[v][z] = 100000000.0;
						//}
					//}
					l[v][0] = distance(get_node_position(v),get_node_position(node0));
				}
			}
		}

		if (kk > 1){
			for (i = 2; i<kk+1; i++){
				v = ALL_LOCAL_NODES;
				while(v--){
					if(ISVALID(v)==1){
						if (A[v] == 1){
							node0 = closerInTree(v, Tree, 1);
							d = distance(get_node_position(v),get_node_position(node0));
							ltemp = 10000000.0;
							j = ALL_LOCAL_NODES;
							w = -1;
							while(j--){
								if(ISVALID(j)==1){
									if((j!=v) && (A[j] == 1)){
										node1 = closerInTree(j, Tree, 1);
										d1 = distance(get_node_position(j),get_node_position(node1));
										if(d1 < d){
											if((l[j][i-2]+distance(get_node_position(v),get_node_position(j)))/i < ltemp){
												ltemp = l[j][i-2]+distance(get_node_position(v),get_node_position(j))/i;
												w = j;
											}
										}
									}
								}
							}
							if (w>=0){
								//if(l[v]==NULL){
								//	l[v] = (float*)malloc(sizeof(float)*MAX_K);
								//	for(z=0;z<MAX_K;z++){
								//		l[v][z] = 100000000.0;
								//	}
								//}
								l[v][i-1] = l[w][i-2]+distance(get_node_position(v),get_node_position(w));
								sm_copy(mp, v, i-1, w, i-2);
								sm_insert(mp, v, i-1, -1, w);
							}

						}
					}
				}
			}
		}

		ltemp = 100000;
		j = ALL_LOCAL_NODES;
		v=-1;
		m=-1;
		while(j--){
			if(ISVALID(j)==1 && A[j]==1){
				i = kk;
				while(i--){
					if((l[j][i]/(float)(i+1) < ltemp) && sm_exist(mp, j, i)){
						if(i==0 || (i>0 && sm_get(mp,j,i,1)>=0)){
							ltemp = l[j][i]/(float)(i+1);
							v = j;
							m = i;
						}
					}
				}
			}
		}

		if(v>=0 && m>=0){
			w = sm_get(mp,v,m,0);
			if (m==0){
				x = v;
			}
			else{
				x = sm_get(mp,v,m,1);
			}
			if(x>=0 && w>=0){
				Tree[x]=w;
				A[x] = 0;
				//printf("ok! ");
			}
			//else{
			//	printf("bad! ");
			//	printf("m=%d, v=%d, w=%d, x=%d! \n", m, v, w, x);
			//}
		}
		//printf("List size: %d\n", sm_count_used(mp));
	}

	return;
}//end k_mst

/******compute the k-mst**********/
/*
void k_MST2(int8_t *Tree, int8_t addr, int8_t *S, int8_t k){
	//compute the k-MST rooted in addr over nodes in S

	int8_t A[ALL_LOCAL_NODES], kk, i, j, w, m, x, v, z, node0, node1;
	float d, d1, ltemp;

	//p and l are global variables, because they are too large and they are causing memory overflow

	//PRINTF("--> Compute %d-MST rooted on node %d.0\n", k, get_all_local_nodes()->addr[addr].u8[0]);

	kk = ALL_LOCAL_NODES;
	while(kk--){
		A[kk] = S[kk];
		if(l[kk]!=NULL){
			for(z=0;z<MAX_K;z++){
				l[kk][z] = 100000000.0;
			}
		}
		Tree[kk] = -1;
	}

	Tree[addr] = addr;

	kk = k+1;
	while((kk--) && (kk>0)){
		v = ALL_LOCAL_NODES;
		while(v--){
			if(p[v].k!=NULL){
				for(z=0;z<MAX_K;z++){
					if(l[v]!=NULL){
						l[v][z] = 100000000.0;
					}
					if(p[v].k[z].node!=NULL){
						for(j=0;j<MAX_K;j++){
							p[v].k[z].node[j] = -1;
						}
					}
				}
			}
		}

		v = ALL_LOCAL_NODES;
		while(v--){
			if(ISVALID(v)==1){
				if (A[v] == 1){
					if(p[v].k==NULL){
						p[v].k = (slist*)malloc(sizeof(slist)*MAX_K);
						for(z=0;z<MAX_K;z++){
							p[v].k[z].node = NULL;
						}
					}
					if(p[v].k[0].node==NULL){
						p[v].k[0].node = (int8_t*)malloc(sizeof(int8_t)*MAX_K);
						for(z=0;z<MAX_K;z++){
							p[v].k[0].node[z] = -1;
						}
					}

					node0 = closerInTree(v, Tree, 1);
					//calculer w, noeud dans l'arbre
					p[v].k[0].node[0] = node0;
					if(l[v]==NULL){
						l[v] = (float*)malloc(sizeof(float)*MAX_K);
						for(z=0;z<MAX_K;z++){
							l[v][z] = 100000000.0;
						}
					}
					l[v][0] = distance(get_node_position(v),get_node_position(node0));
				}
			}
		}

		if (kk > 1){
			for (i = 2; i<kk+1; i++){
				v = ALL_LOCAL_NODES;
				while(v--){
					if(ISVALID(v)==1){
						if (A[v] == 1){
							node0 = closerInTree(v, Tree, 1);

							d = distance(get_node_position(v),get_node_position(node0));
							ltemp = 10000000.0;
							j = ALL_LOCAL_NODES;
							w = -1;
							while(j--){
								if(ISVALID(j)==1){
									if((j!=v) && (A[j] == TRUE)){
										node1 = closerInTree(j, Tree, 1);
										d1 = distance(get_node_position(j),get_node_position(node1));
										if(d1 < d){
											if((l[j][i-2]+distance(get_node_position(v),get_node_position(j)))/i < ltemp){
												ltemp = l[j][i-2]+distance(get_node_position(v),get_node_position(j))/i;
												w = j;
											}
										}
									}
								}
							}
							if (w>=0){
								if(l[v]==NULL){
									l[v] = (float*)malloc(sizeof(float)*MAX_K);
									for(z=0;z<MAX_K;z++){
										l[v][z] = 100000000.0;
									}
								}
								l[v][i-1] = l[w][i-2]+distance(get_node_position(v),get_node_position(w));

								if(p[v].k==NULL){
									p[v].k = (slist*)malloc(sizeof(slist)*MAX_K);
									for(z=0;z<MAX_K;z++){
										p[v].k[z].node = NULL;
									}
								}
								if(p[v].k[i-1].node==NULL){
									p[v].k[i-1].node = (int8_t*)malloc(sizeof(int8_t)*MAX_K);
									for(z=0;z<MAX_K;z++){
										p[v].k[i-1].node[z] = -1;
									}
								}


								if(p[w].k[i-2].node!=NULL){
									copy(p[v].k[i-1].node, p[w].k[i-2].node);
								}
								insert_end(w, p[v].k[i-1].node);
							}

						}
					}
				}
			}
		}

		ltemp = 100000;
		j = ALL_LOCAL_NODES;
		v=-1;
		m=-1;
		while(j--){
			if(ISVALID(j)==1 && p[j].k!=NULL && A[j]==1){
				i = kk;
				while(i--){
					if((p[j].k[i].node!=NULL) && (l[j][i]/(float)(i+1) < ltemp)){
						ltemp = l[j][i]/(float)(i+1);
						v = j;
						m = i;
					}
				}
			}
		}
//		if(v==-1 || m==-1){
//			printf("BUG: v=%d | m=%d ",v,m);
//		}
//		else{
//			printf("OK: v=%d | m=%d ",v,m);
//		}
//
		if(v>=0 && m>=0){
			w = p[v].k[m].node[0];
			//printf("| w=%d ",w);

			if (m==0){
				x = v;
			}
			else{
				x = p[v].k[m].node[1];
			}
			//printf("| x=%d\n",x);
			if(x>=0 && w>=0){
				Tree[x]=w;
				A[x] = 0;
			}
		}
//		else{
//			printf("\nBUG: TREE - ");
//			for(i=0;i<ALL_LOCAL_NODES;i++){
//				if(Tree[i]!=-1)
//					printf("T(%d): %d, ", i, Tree[i]);
//			}
//			printf("\nBUG: Sinks - ");
//			for(i=0;i<ALL_LOCAL_NODES;i++){
//				if(Tree[i]==1)
//					printf("A(%d): %d, ", i, A[i]);
//			}
//			printf("\n");
//		}
	}

	return;
}//end k_mst
*/

/*********copy**************/
/*void copy(int8_t *a, int8_t *b){

	unsigned char j=0;

	while(b[j]!=-1 && j<MAX_K){
		insert_end(b[j], a);
		j++;
	}

	return;
}*/

/*************************************************************/
/*void insert_end(int8_t val, int8_t *head)  {

	unsigned char j=0;

	if(head[j]==-1)    //List is empty. Initialize
	{
		head[j] = val;
	}
	else{

		while(head[j]!=-1 && j<MAX_K){
			if(head[j]==val){
				//value was already inserted
				return;
			}
			j++;
		}
		//The end of the queue has been reached. New item is added at the end.
		if(j<MAX_K){
			head[j]=val;
		}
		else{
			return;
		}
	}

	return;
}//end insert_end
*/

/*****compute_tree_weight*********/
float  compute_tree_weight(int8_t * t){

  float w = 0.0;
  int8_t addr =  ALL_LOCAL_NODES;

  while(addr--){
	  if (t[addr]>=0){
		  w += distance(get_node_position(addr),get_node_position(t[addr]));
	  }
  }

  return w;

}//end  compute_tree_weight

/*********closer_in_tree**************/
int8_t closerInTree(int8_t a, int8_t *T, uint8_t case2){
  //return the closer node in T from a and this dist
  int8_t res = -1, i;
  float d = 100000.0;


  i = ALL_LOCAL_NODES;
  while(i--)
    if (T[i] >= 0 && (i!=get_all_local_nodes()->self || case2)) //there is a case when it should not return the node itself
      //i is in the tree
      if (distance(get_node_position(a),get_node_position(i)) < d){
        res = i;
        d = distance(get_node_position(a),get_node_position(i));
      }

  return res;

}//end read_queue


void as_insert(array1d *first, int8_t node, int8_t sink, int8_t value){

	array1d *free=NULL, *exist=NULL, *curr=first;
	static int count=1;

	while(curr!=NULL){
		if(curr->node==-1 && curr->sink==-1 && curr->value==-1 && free==NULL){
			free=curr;
		}
		if(curr->node==node && curr->sink==sink && node!=-1 && exist==NULL){
			exist=curr;
		}
		if(curr->next!=NULL){
			curr=curr->next;
		}
		else{
			break;
		}
	}


	if(exist!=NULL){
		exist->value=value;
	}
	else{
		if(free==NULL){
			free = (array1d *)malloc(sizeof(array1d));
			curr->next = free;
			free->next=NULL;
			count++;
			printf("[as_insert] No free space found! Creating new entry! Count: %d\n", count);
		}
		free->node=node;
		free->sink=sink;
		free->value=value;
	}

	return;
}

int8_t as_get(array1d *first, int8_t node, int8_t sink){

	array1d *curr=first;

	while(curr!=NULL){
		if(curr->node==node && curr->sink==sink){
			return curr->value;
		}
		curr=curr->next;
	}

	return -1;
}

void as_reset(array1d *first){

	array1d *curr=first;

	while(curr!=NULL){
		curr->node=-1;
		curr->sink=-1;
		curr->value=-1;
		curr=curr->next;
	}

	return;
}

/******************AllocateSinks***************/
void AllocateSinks(array1d *sinksPerBranche, int8_t *sinks, int8_t *Tree){

	allnodes *all = get_all_local_nodes();

	int8_t branches[ALL_LOCAL_NODES];
	//branches is the list of branches from source
	int8_t addr, addr2, b, OK, source=all->self;

	PRINTF("!!!! Run allocate sinks to branch rooted in %d.0 \n", all->addr[source].u8[0]);

	//init
	addr = ALL_LOCAL_NODES;
	while(addr--){
		branches[addr] = FALSE;
	}
	as_reset(sinksPerBranche);

	//identifies branches
	addr = ALL_LOCAL_NODES;
	while(addr--){
		if(ISVALID(addr)==1){
			if ((source!=addr) && (Tree[addr] == source)) {
				if(addr<MAX_SINKS){
					branches[addr] = TRUE;
					as_insert(sinksPerBranche, addr, addr, TRUE);
				}
			}
		}
	}

	//assigns sinks
	//first assign branches to nodes in tree
	addr = ALL_LOCAL_NODES;
	while(addr--){
		if(ISVALID(addr)==1){
			if ((sinks[addr] == 1) && (Tree[addr]>=0) && (branches[addr]==FALSE)){
				//addr is a sink in the k-MST.  It is attached to its ancestor
				addr2 = Tree[addr];
				while(branches[addr2] == FALSE){
					addr2 = Tree[addr2];
				}

				if(addr<MAX_SINKS){
					as_insert(sinksPerBranche, addr2, addr, TRUE);
				}
				//printf("(a) sinksPerBranche[%d.0].m[%d.0]: %d | ", all->addr[addr2].u8[0], all->addr[addr].u8[0], sinksPerBranche[addr2].m[addr]);
			}
		}
	}
//printf("\n");


	//now assign branches to sinks not in tree
	addr = ALL_LOCAL_NODES;
	while(addr--){
		if(ISVALID(addr)==1){
			if ((sinks[addr] == 1) && (Tree[addr] < 0)){
				addr2 = closerInTree(addr, Tree, 0);
				//find to what branche addr2 belongs
				b = ALL_LOCAL_NODES;
				OK = 0;
				while((b--) && (OK == 0)){
					if(ISVALID(b)==1){
						if (branches[b] == TRUE){
							if(addr<MAX_SINKS && addr2<MAX_SINKS){
								if (as_get(sinksPerBranche, b, addr2) == TRUE){
									as_insert(sinksPerBranche, b, addr, TRUE);
									OK = 1;
									//printf("(b) sinksPerBranche[%d.0].m[%d.0]: %d | ", all->addr[b].u8[0], all->addr[addr].u8[0], sinksPerBranche[b].m[addr]);
								}
							}
						}
					}
				}
				//printf("\n");
			}
		}
	}

	return;

}//end AllocateSinks

/******************AllocateSinks***************/
/*
void AllocateSinks(array1d *sinksPerBranche, int8_t *sinks, int8_t *Tree){

	allnodes *all = get_all_local_nodes();

	int8_t branches[ALL_LOCAL_NODES];
	//branches is the list of branches from source
	int8_t addr, addr2, addr3, b, OK, source=all->self;

	PRINTF("!!!! Run allocate sinks to branch rooted in %d.0 \n", all->addr[source].u8[0]);

	//init
	addr = ALL_LOCAL_NODES;
	while(addr--){
		branches[addr] = FALSE;

		//addr2 = ALL_LOCAL_NODES;
		addr2 = MAX_SINKS;
		if(sinksPerBranche[addr].m!=NULL){
			while(addr2--){
				sinksPerBranche[addr].m[addr2] = 0;
			}
		}
	}

	//identifies branches
	addr = ALL_LOCAL_NODES;
	while(addr--){
		if(ISVALID(addr)==1){
			if ((source!=addr) && (Tree[addr] == source)) {
				if(sinksPerBranche[addr].m==NULL){
					//sinksPerBranche[addr].m = (int8_t*)malloc(sizeof(int8_t)*ALL_LOCAL_NODES);
					sinksPerBranche[addr].m = (int8_t*)malloc(sizeof(int8_t)*MAX_SINKS);
					//addr3 = ALL_LOCAL_NODES;
					addr3 = MAX_SINKS;
					while(addr3--){
						sinksPerBranche[addr].m[addr3] = 0;
					}
				}
				if(addr<MAX_SINKS){
					branches[addr] = TRUE;
					sinksPerBranche[addr].m[addr] = TRUE;
				}
			}
		}
	}

	//assigns sinks
	//first assign branches to nodes in tree
	addr = ALL_LOCAL_NODES;
	while(addr--){
		if(ISVALID(addr)==1){
			if ((sinks[addr] == 1) && (Tree[addr]>=0) && (branches[addr]==FALSE)){
				//addr is a sink in the k-MST.  It is attached to its ancestor
				addr2 = Tree[addr];
				while(branches[addr2] == FALSE){
					addr2 = Tree[addr2];
				}

				if(sinksPerBranche[addr2].m==NULL){
					//sinksPerBranche[addr2].m = (int8_t*)malloc(sizeof(int8_t)*ALL_LOCAL_NODES);
					sinksPerBranche[addr2].m = (int8_t*)malloc(sizeof(int8_t)*MAX_SINKS);
					//addr3 = ALL_LOCAL_NODES;
					addr3 = MAX_SINKS;
					while(addr3--){
						sinksPerBranche[addr2].m[addr3] = 0;
					}
				}

				if(addr<MAX_SINKS){
					sinksPerBranche[addr2].m[addr] = TRUE;
				}
				//printf("(a) sinksPerBranche[%d.0].m[%d.0]: %d | ", all->addr[addr2].u8[0], all->addr[addr].u8[0], sinksPerBranche[addr2].m[addr]);
			}
		}
	}
//printf("\n");


	//now assign branches to sinks not in tree
	addr = ALL_LOCAL_NODES;
	while(addr--){
		if(ISVALID(addr)==1){
			if ((sinks[addr] == 1) && (Tree[addr] < 0)){
				addr2 = closerInTree(addr, Tree, 0);
				//find to what branche addr2 belongs
				b = ALL_LOCAL_NODES;
				OK = 0;
				while((b--) && (OK == 0)){
					if(ISVALID(b)==1){
						if(sinksPerBranche[b].m==NULL){
							//sinksPerBranche[b].m = (int8_t*)malloc(sizeof(int8_t)*ALL_LOCAL_NODES);
							sinksPerBranche[b].m = (int8_t*)malloc(sizeof(int8_t)*MAX_SINKS);
							//addr3 = ALL_LOCAL_NODES;
							addr3 = MAX_SINKS;
							while(addr3--){
								sinksPerBranche[b].m[addr3] = 0;
							}
						}

						if (branches[b] == TRUE){
							if(addr<MAX_SINKS && addr2<MAX_SINKS){
								if (sinksPerBranche[b].m[addr2] == TRUE){
									sinksPerBranche[b].m[addr] = TRUE;
									OK = 1;
									//printf("(b) sinksPerBranche[%d.0].m[%d.0]: %d | ", all->addr[b].u8[0], all->addr[addr].u8[0], sinksPerBranche[b].m[addr]);
								}
							}
						}
					}
				}
				//printf("\n");
			}
		}
	}

	return;

}//end AllocateSinks
*/

/***********nbTargets*********************/
s_tree nbTargets(int8_t s, int8_t *T){
	//return the number of nodes in the subtree of T rooted in s of the weight of subTree

	int8_t k = 0, k1, addr, addr1, addr2, OK;
	s_tree res;
	int8_t Marque[ALL_LOCAL_NODES], p1[ALL_LOCAL_NODES], p2[ALL_LOCAL_NODES];
	float w1 = 0.00, w = 0.0;

	for(addr=0;addr<ALL_LOCAL_NODES;addr++){
		Marque[addr] = 0;
		p1[addr]=0;
		p2[addr]=0;
	}

	addr = ALL_LOCAL_NODES;
	while(addr--){
		if((T[addr]>=0) && (Marque[addr] == 0)){
			OK = 0;
			if(addr == s){
				k++;
				OK = 1;
			}
			Marque[addr] = 1;
			addr1 = T[addr];
			addr2 = addr;
			k1 = 1;
			w1 = 0.0;
			while(OK == 0){
				if (addr1!=addr2 && (p1[addr1]!=1 || p2[addr2]!=1)){
					w1+= distance(get_node_position(addr1),get_node_position(addr2));
					p1[addr1]=1;
					p2[addr2]=1;
					//printf("[%d.0; %d.0] = w1: %d || ", get_all_local_nodes()->addr[addr1].u8[0], get_all_local_nodes()->addr[addr2].u8[0], (int)w1);
				}
				if (Marque[addr1] == 0) {
					k1++;
					Marque[addr1] = 1;
				}
				if (addr1==s){
					OK = 1;
					k+=k1;
					//printf(" w1: %d + W: %d = %d \n", (int)w1, (int)w, (int)(w1+w));
					w+=w1;
				}
				addr2 = addr1;
				addr1 = T[addr1];
				if (addr1 == addr2){
					OK = 1;
				}
			}
		}
	}

	res.size = k;
	res.weight = w;

	PRINTF("???? Subtree rooted in %d.%d contains %d targets for a weight of %d\n", get_all_local_nodes()->addr[s].u8[0], get_all_local_nodes()->addr[s].u8[1], res.size, (int)res.weight);

	return res;
}//end nbTargets

/**********************greedy*************/
int8_t greedy(int8_t *dest, float w, int8_t k){

  int8_t addr, u = -1;
  int8_t T[ALL_LOCAL_NODES];
  float COP=10000000, COP_temp, temp;
  s_tree res;

  allnodes *all = get_all_local_nodes();

  PRINTF("START GREEDY from %d.0 towards %d sinks: ", all->addr[all->self].u8[0], k);

 /* unsigned char i;
  for(i=0;i<ALL_LOCAL_NODES;i++){
	  if(dest[i]>0){
		  PRINTF("%d.0, ", all->addr[i].u8[0]);
	  }
  }
  PRINTF("\n");*/

  addr=ALL_LOCAL_NODES;
  while(addr--){
	  if (addr!=all->self && all->isneighbor[addr]==1){
		  //printf("Testing neighbor %d.0 | ", all->addr[addr].u8[0]);
		  k_MST(T, addr, dest, k);
		  /*
		  int i;
			printf("kanycast (k: %d): ", k);
			for(i=0;i<ALL_LOCAL_NODES;i++){
				if(all->addr[T[i]].u8[0]!=0)
					printf("T(%d.0)=%d.0 | ", all->addr[i].u8[0], all->addr[T[i]].u8[0]);
			}
			printf("\n");
		  */
		  res = nbTargets(addr, T);
		  temp = (float)(w - res.weight); //INCLUDED based on the paper, not the code
		  //printf("w: %d | res.w: %d | ", (int)w, (int)res.weight);
		  if(temp>0){ //INCLUDED based on the paper, not the code
			  COP_temp = sendingCost(distance(get_node_position(all->self),get_node_position(addr)))/temp;
			  //printf("COP_temp: %d | sendCost: %d | ", (int)COP_temp, (int)temp);
			  if ((u == -1)||(COP_temp < COP)){
				  //printf("selecionado! \n");
				  COP = COP_temp;
				  u = addr;
			  }
			 /* else{
			  	  printf("ignored! \n");
			  }*/
		  }
		/*  else{
		  	  printf("ignored - negative progress! \n");
		  }*/
	  }
  }

  //printf("\nEND GREEDY from %d.0 towards %d sinks -- RETURN %d.0\n", all->addr[all->self].u8[0], k, (u==-1?0:all->addr[u].u8[0]));

  return u;
}//end greedy

/***************************************************/
float sendingCost(float dist){

  //struct entitydata *entitydata = get_entitydata();

  if (dist == 0)
    return 0;
  else
	  return (float)ENERGY_COST(dist, reading_pkt_t);
    //return (float)(fastPrecisePow(dist,entitydata->ALPHA_NRJ)+entitydata->C_NRJ);
}

/****************recovery***************************/
int8_t recovery(int8_t ref, pos_t recovery_src, int8_t dest){

	allnodes *all = get_all_local_nodes();
	int8_t addr,  OK, OK2 = TRUE,  max=all->self, winner=all->self, ref2, GG[ALL_LOCAL_NODES];
	float d1, d2, angle, vect, angle_min = 361.0, angle_max = -1.0, a;

	computeGG(GG, all->isneighbor);

	d1 = distance(get_node_position(ref),get_node_position(all->self));
	PRINTF("Sink: %d.0 \n", all->addr[dest].u8[0]);
	ref2 = ref;
	while(OK2 == TRUE){
		addr = ALL_LOCAL_NODES;
		while(addr--){
			if(ISVALID(addr)){
				if ((GG[addr] == 1) && (addr!=ref)){
					/*
					if(dest==ref2){
						PRINTF("Testing - n: %d.0, sink: %d.0, self: %d.0 | ", all->addr[addr].u8[0], all->addr[ref2].u8[0], all->addr[all->self].u8[0]);
					}
					else{
						if(ref2==ref){
							PRINTF("Testing - n: %d.0, sender: %d.0, self: %d.0 | ", all->addr[addr].u8[0], all->addr[ref2].u8[0], all->addr[all->self].u8[0]);
						}
						else{
							PRINTF("Testing - n: %d.0, v: %d.0, self: %d.0 | ", all->addr[addr].u8[0], all->addr[ref2].u8[0], all->addr[all->self].u8[0]);
						}
					}
					*/
					d2 = distance(get_node_position(addr),get_node_position(all->self));
					a = (get_node_position(all->self)->x-get_node_position(ref2)->x)*(get_node_position(all->self)->x-get_node_position(addr)->x) + (get_node_position(all->self)->y-get_node_position(ref2)->y)*(get_node_position(all->self)->y-get_node_position(addr)->y);
					angle = acos(a/(float)(d1*d2))*(180.0/PI);
					vect = (((get_node_position(addr)->x-get_node_position(all->self)->x)*(get_node_position(ref2)->y-get_node_position(all->self)->y)) - ((get_node_position(addr)->y-get_node_position(all->self)->y)*(get_node_position(ref2)->x-get_node_position(all->self)->x)));
					PRINTF("angle: %d | vect: %d.%d | ", (int)angle, (int)vect, (int)decimals(vect));
					//vect = 3e composante du produit vectoriel  (c->node addr)^(c->node ref)
					OK = 0;

					if((int)vect >=0){
						OK = 1;
					}
					//To choose the proper side (right hand rule), we need vect >= 0l

					if((OK==1) && (angle < angle_min))
					{
						angle_min = angle;
						winner = addr;
						PRINTF("MIN: v_min = %d.0 | ", all->addr[addr].u8[0]);
					}
					if((OK==0) && (angle > angle_max))
					{
						angle_max = angle;
						max = addr;
						PRINTF("MAX: v_max = %d.0 | ", all->addr[addr].u8[0]);
					}
					PRINTF("\n");
				}
			}
		}

		if (winner == all->self){ //we have to take the exterior side face
			if(max != all->self){
				PRINTF("opposite side \n");
				return max;
			}
			else{
				//the current  node is a leaf in the planar graph
				PRINTF("go back \n");
				return ref; //we have to go back
			}
		}

		//needs too check whether winner makes the routing crossing the line src-dest
		OK2 = ChangeFace(recovery_src, dest, winner);
		PRINTF("flag: %d | v_min = %d.0 | v = %d.0\n", OK2, all->addr[winner].u8[0], all->addr[ref2].u8[0]);
		if(ref2==winner){
			OK2 = 0;
		}
		ref2 = winner;


	}

	//printf("XXXXXXXXXXXXX Exit recovery toward node %d.0\n", all->addr[winner].u8[0]);

	return winner;
}//end recovery

/********************************************************/
void computeGG(int8_t *GG, int8_t *CDS){

	int8_t addr, addr2;
	float d1, d2, d3;

	//struct entitydata *entitydata = get_entitydata();

	allnodes *all = get_all_local_nodes();

	addr = ALL_LOCAL_NODES;
	while(addr--){
		GG[addr] = FALSE;
	}
//printf("GG: ");
	addr = ALL_LOCAL_NODES;
	while(addr--){
		if(ISVALID(addr)){
			if(addr!=all->self && all->isneighbor[addr]){
				GG[addr] = TRUE;
				addr2 = ALL_LOCAL_NODES;
				while(addr2--){
					if(ISVALID(addr2)){
						if(addr!=addr2 && addr2!=all->self && all->isneighbor[addr]){
							d1 = distance(get_node_position(all->self), get_node_position(addr));
							d2 = distance(get_node_position(all->self), get_node_position(addr2));
							d3 = distance(get_node_position(addr), get_node_position(addr2));
							if(d1*d1 > (d2*d2 + d3*d3)){
								GG[addr] = FALSE;
							}
						}
					}
				}
				/*if(GG[addr] == TRUE){
					printf("Yes: %d.0 | ", all->addr[addr].u8[0]);
				}
				else{
					printf("No: %d.0 | ", all->addr[addr].u8[0]);
				}*/
			}
		}
	}
//printf("\n");

	return;

}//end Compute GG

/*********************ChangeFace***********************************/
int8_t ChangeFace(pos_t recovery_src, int8_t dest, int8_t next){

	float vect1, vect2, b1, b2, x, y, d;
	int8_t OK = TRUE;
	allnodes *all = get_all_local_nodes();

	//We change face if angle(src-current, src-dest) is of different sign of angle(Src-next,SrcDest)
	vect1 = (((get_node_position(all->self)->x-recovery_src.x)*(get_node_position(dest)->y-recovery_src.y)) - ((recovery_src.y-get_node_position(all->self)->y)*(recovery_src.x-get_node_position(dest)->x)));
	//vect1 = produit vectoriel SC^SD

	if (vect1 == 0.0){
		return TRUE; //source, current node and destination nodes are aligned.
	}

	vect2 = (((get_node_position(next)->x-recovery_src.x)*(get_node_position(dest)->y-recovery_src.y)) - ((recovery_src.y-get_node_position(next)->y)*(recovery_src.x-get_node_position(dest)->x)));
	//vect2 = produit vectoriel SN^SD

	if(((vect1<0) && (vect2<0)) || ((vect1>0) && (vect2>0))){
		OK = FALSE;
	}

	if (OK == TRUE){
		//line C->nodeNext and SrcDest cross each other.
		//We have to determine if intersection in on segment SrcDest
		OK = FALSE;

		vect1 = (get_node_position(dest)->y-recovery_src.y)/(float)(get_node_position(dest)->x-recovery_src.x);
		vect2 = (get_node_position(next)->y-get_node_position(all->self)->y)/(float)(get_node_position(next)->x-get_node_position(all->self)->x);

		b1 = recovery_src.y - vect1*recovery_src.x;
		b2 = get_node_position(all->self)->y - vect2*get_node_position(all->self)->x;

		x = (b2-b1)/(float)(vect1-vect2);
		y = (float)(vect1*x)+b1;


		d = sqrtf(fastPrecisePow(recovery_src.x-x,2) + fastPrecisePow(recovery_src.y-y,2)) + sqrtf(fastPrecisePow(get_node_position(dest)->x-x,2) + fastPrecisePow(get_node_position(dest)->y-y,2));
		if ((int)(distance(&recovery_src,get_node_position(dest)) == (int)(fastPrecisePow(10,6)* d))){
			//intersection point is  on segment, we change face
			OK = TRUE;
		}
	}

	return OK;

}//end ChangeFace

