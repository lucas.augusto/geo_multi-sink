#ifndef KANGUROU_FUNCTIONS_H
#define KANGUROU_FUNCTIONS_H

#include "kangurou.h"

int get_index_by_addr(const rimeaddr_t *item_addr, const rimeaddr_t *list_addr, int list_size);

struct kneighbor * get_neighbor_pointer_by_addr(const rimeaddr_t *item_addr);

struct ksink * get_sink_pointer_by_addr(const rimeaddr_t *item_addr);

void rimeaddr_null_init(rimeaddr_t *list_addr, int size);

pos_t * get_node_position_by_addr(const rimeaddr_t *item_addr);

pos_t * get_node_position(int index);

#endif
