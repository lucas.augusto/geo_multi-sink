#ifndef KANGUROU_DECISIONS_H
#define KANGUROU_DECISIONS_H

#include "kangurou.h"
#include "support.h"
#include "kangurou_packets.h"

/* LinkedList datastrcut */
typedef struct {
	int8_t *node;
} slist;

typedef struct {
	slist *k;
} sNeighborsList;

typedef struct s_tree{
	int8_t size;
   float weight;
} s_tree;

/*
typedef struct {
	int8_t *m;
} array1d;
*/

typedef struct array1d_s {
	struct array1d_s *next;
	int8_t node;
	int8_t sink;
	int8_t value;
} array1d;


typedef struct array3d_s {
	struct array3d_s *next;
	int8_t value;
	int8_t p;
	int8_t k;
	int8_t node;
} array3d;

void kanycasting(rimeaddr_t *src_addr, rimeaddr_t *prev_addr, int8_t *sinks, int8_t k, float d, rimeaddr_t *r_addr, pos_t ref_pos, reading_pkt_t *reading_pkt, int hops, rimeaddr_t *dest_addr);

void Send(rimeaddr_t *dest, int8_t k, int8_t *sinks, float d, rimeaddr_t *ref, pos_t ref_pos, rimeaddr_t *src, reading_pkt_t *reading_pkt, rimeaddr_t *ref_dest,int8_t recovery_flag);

void k_MST(int8_t *Tree, int8_t addr, int8_t *S, int8_t k);

float  compute_tree_weight(int8_t *t);

int8_t closerInTree(int8_t a, int8_t *T, uint8_t case2);

void copy(int8_t *a, int8_t *b);

void insert_end(int8_t val, int8_t *head);

void AllocateSinks(array1d *sinksPerBranche, int8_t* sinks, int8_t* Tree);

s_tree nbTargets(int8_t s, int8_t* T);

int8_t greedy(int8_t* dest, float w, int8_t k);

float sendingCost(float dist);

int8_t recovery(int8_t ref, pos_t recovery_src, int8_t dest);

void computeGG(int8_t * GG, int8_t * CDS);

int8_t ChangeFace(pos_t recovery_src, int8_t dest, int8_t next);


void as_reset(array1d *first);
int8_t as_get(array1d *first, int8_t node, int8_t sink);
void as_insert(array1d *first, int8_t node, int8_t sink, int8_t value);

#endif
