#ifndef SUPPORT_H
#define SUPPORT_H


#ifndef INT_MAX
#define INT_MAX 	(powi(2,(sizeof(int)*8))-1)-1
#endif

#ifndef INT_MIN
#define INT_MIN 	-powi(2,((sizeof(int)*8)-1))
#endif

/* Definition of the type pos_t that holds the position structure. */
typedef struct {
  float x;
  float y;
} pos_t;

void print_pos(pos_t pos);

float distance(const pos_t *a, const pos_t *b);

int pos_cmp(pos_t a, pos_t b);

unsigned decimals(float f);

float stof(const char* s);

int stoi(char* str);

double fastPrecisePow(double a, double b);

uint32_t powi(uint16_t x, uint16_t y);

#endif
