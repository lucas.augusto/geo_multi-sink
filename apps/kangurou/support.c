#include "net/rime.h"

#include <stdio.h> /* For printf() */
#include <math.h>  /* For sqrt() */
#include <stdio.h>  /* For printf() */
#include <string.h>  /* For strlen() */
#include <stdlib.h>  /* For atoi() and atof() */

#include "support.h"

//-----------------------------------------------------------------------------
void print_pos(pos_t pos) {
    printf("x - %ld.%03u  y - %ld.%03u\n", (long)pos.x, decimals(pos.x), (long)pos.y, decimals(pos.y));
}

//-----------------------------------------------------------------------------
float distance(const pos_t *a, const pos_t *b) {
	float diff_x = a->x - b->x;
	float diff_y = a->y - b->y;

	return sqrtf(diff_x*diff_x + diff_y*diff_y);
}

/*
//-----------------------------------------------------------------------------
int pos_cmp(pos_t a, pos_t b) {
	return (a.x == b.x) && (a.y == b.y);
}
*/

//-----------------------------------------------------------------------------
// returns the fractional part of the floating point number as unsigned integer
// needed for printing since printf implementation in contiki does not support
// floating point numbers
unsigned decimals(float f) {
	return (unsigned)((f-floor(f))*1000);
}

//-----------------------------------------------------------------------------
// string to float implementation since atof is not present in contiki stdlib
// taken from http://stackoverflow.com/a/4392789
float stof(const char* s){

  float rez = 0, fact = 1;
  int point_seen;
  if (*s == '-'){
    s++;
    fact = -1;
  };
  for (point_seen = 0; *s; s++){
    if (*s == '.'){
      point_seen = 1;
      continue;
    };
    int d = *s - '0';
    if (d >= 0 && d <= 9){
      if (point_seen) fact /= 10.0f;
      rez = rez * 10.0f + (float)d;
    };
  };
  return rez * fact;
}


//-----------------------------------------------------------------------------
// math.h pow not present, so simple, no checking
uint32_t powi(uint16_t x, uint16_t y) {
  uint32_t res = x;
  int i;

  if (y == 0) {
    res = 1;
  }
  else if (y > 1) {
    for(i = 1; i<y; i++) {
      res *= x;
    }
  }

  return res;
}


//-----------------------------------------------------------------------------
// Optimized Approximative pow() in C
// taken from https://martin.ankerl.com/2012/01/25/optimized-approximative-pow-in-c-and-cpp/
// should be much more precise with large b
double fastPrecisePow(double a, double b){
  // calculate approximation with fraction of the exponent
  int e = (int) b;
  union {
    double d;
    int x[2];
  } u = { a };
  u.x[1] = (int)((b - e) * (u.x[1] - 1072632447) + 1072632447);
  u.x[0] = 0;

  // exponentiation by squaring with the exponent's integer part
  // double r = u.d makes everything much slower, not sure why
  double r = 1.0;
  while (e) {
    if (e & 1) {
      r *= a;
    }
    a *= a;
    e >>= 1;
  }

  return r * u.d;
}

//-----------------------------------------------------------------------------
// my crappy version of atoi
int stoi(char* str) {

#if SENSLAB
	return atoi(str);
#endif

    char *str_to_int;
    int i=0, size, sign=1;
    long int num=0, check1;
    unsigned long long check2;

    do{
        str_to_int = &str[i];
        i++;
    }while(str[i-1]==' ');

    size=strlen(str_to_int);

    if(size==0)
        return 0;

    for(i=0; i<size; i++){
        if((str_to_int[i]=='-' || str_to_int[i]=='+') && i==0){
            if(str_to_int[i]=='-')
                sign=-1;
        }
        else{
            if(str_to_int[i]<'0' || str_to_int[i]>'9')
                return(num*sign);
            else{
                check1=num+((int)str_to_int[i]-48);
                if(check1<num || check1>INT_MAX)
                    return((sign==1?INT_MAX:INT_MIN));
                num = check1;
                if(i<(size-1) && (str_to_int[i+1]>='0' && str_to_int[i+1]<='9')){
                    check2=(unsigned long long)num*10;
                    if(check2>0xffffffff)
                        return((sign==1?INT_MAX:INT_MIN ));
                    num=(int)check2;
                    if(num<0)
                        return((sign==1?INT_MAX:INT_MIN ));
                }
            }
        }
    }

    return num*sign;
}
