/*
 * Copyright (c) 2006, Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

/**
 * \file
 *         geo-kanycast.c
 * \author
 *         Lucas Leao <lucas.leao@femto-st.fr>
 *
 *  Based on the geoware_app:
 *  https://github.com/noname77/geoware
 *
 */

/* contiki includes */
#include "contiki.h"
#include "net/rime.h"
#include "dev/serial-line.h"
#include "dev/button-sensor.h"
#include "lib/random.h"
#include "sys/rtimer.h"

/* standard library includes */
#include <stdio.h>  /* For printf() */
#include <string.h> /* For memcpy */
#include <stdlib.h>

/* project includes */
#include "support.h"
#include "packets.h"
#include "kfunctions.h"
#include "kanycast_decision.h"
#include "my_metrics.h"
#include "geo-kanycast.h"

/* Set DEBUG to 1 to include debug output */
#define DEBUG 0
#if DEBUG
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

/* Set LOGS to 1 to include log output */
#define LOGS 1
#if LOGS
#define PRINTF_LOG(...) printf(__VA_ARGS__)
#else
#define PRINTF_LOG(...)
#endif

/*---------------------------------------------------------------------------*/

/* node's position */
pos_t own_pos;

/* the list of sinks in void */
int8_t my_sink_void_list[MAX_SINKS];

/* time offset for network sync */
clock_time_t my_offset;

/* if node is sink */
uint8_t i_am_sink = 0;

/* variable with the received max k */
uint8_t received_max_k;

/* Loop avoidance */
struct pkt_loop count_loop;


/*---------------------------------------------------------------------------*/

/* processes */
PROCESS(broadcast_process, "Broadcast process");
PROCESS(multihop_process, "Multihop process");
PROCESS(boot_process, "Boot process");

/*---------------------------------------------------------------------------*/

/* events */
process_event_t broadcast_pos_event;
process_event_t newpacket_event;
process_event_t multipacket_event;

/*---------------------------------------------------------------------------*/

/* List of all neighbors */
kneighbor neighbors_list;

/* List of sinks */
ksink my_sink_list;

/*---------------------------------------------------------------------------*/
/*
 * Get Black List.
 */
int8_t get_black_list(uint8_t node, reading_pkt_t *reading_pkt){

	if(count_loop.id_1==reading_pkt->reading_hdr.hdr.originator.u8[0] && count_loop.id_2==reading_pkt->reading_hdr.hdr.originator.u8[1] && count_loop.id_3==reading_pkt->timestamp_original){
		return count_loop.black_list[node];
	}
	return -1;
}

/*---------------------------------------------------------------------------*/
/*
 * Get received max k.
 */
uint8_t get_received_max_k(){
	return received_max_k;
}

/*---------------------------------------------------------------------------*/
/*
 * Get node's sink void list.
 */
int8_t * get_sink_void_list(){
	return &my_sink_void_list;
}

/*---------------------------------------------------------------------------*/
/*
 * Get neighbors list.
 */
kneighbor * get_neighbor_list(){
	return &neighbors_list;
}

/*---------------------------------------------------------------------------*/
/*
 * Get sinks list.
 */
ksink * get_sink_list(){
	return &my_sink_list;
}

/*---------------------------------------------------------------------------*/
/*
 * Get node's offset for network sync.
 */
clock_time_t get_node_offset(){
	return my_offset;
}

/*---------------------------------------------------------------------------*/
/*
 * Get node's type.
 */
uint8_t is_sink(){

	return i_am_sink;
}

/*---------------------------------------------------------------------------*/
/*
 * This function prints the neighbor list.
 */
void
print_neighbors() {
	uint8_t i, j;
	printf("Neighbors:\n");
	for(j=0; j<MAX_NEIGHBORS; j++) {
		if(!rimeaddr_cmp(&neighbors_list.addr[j],&rimeaddr_null)){
			printf("Rime Address: %d.%d | Neighbor is sink: %c | # closest sinks: %d | sink list: ", neighbors_list.addr[j].u8[0], neighbors_list.addr[j].u8[1], (neighbors_list.is_sink[j]>=0?'Y':'N'), count_valid(neighbors_list.sink_list[j], MAX_SINKS));
			for(i=0;i<MAX_SINKS;i++){
				if(neighbors_list.sink_list[j][i]>=0){
					printf("%d.%d, ", my_sink_list.addr[i].u8[0], my_sink_list.addr[i].u8[1]);
				}
			}
			printf("Position: ");
			print_pos(neighbors_list.pos[j]);
		}
	}
}
/*---------------------------------------------------------------------------*/
/*
 * This function prints the sink list (all sinks and void sinks).
 */
void
print_sinks() {
	uint8_t i;
	for(i=0;i<MAX_SINKS;i++){
		if(!rimeaddr_cmp(&my_sink_list.addr[i], &rimeaddr_null)){
			printf("%d.%d, ", my_sink_list.addr[i].u8[0], my_sink_list.addr[i].u8[1]);
		}
	}
}
/*---------------------------------------------------------------------------*/
/*
 * This function is called by the ctimer present in each neighbor
 * table entry. The function removes the neighbor from the table
 * because it has become too old.
 */
static void
remove_neighbor(uint8_t index)
{

	rimeaddr_copy(&neighbors_list.addr[index],&rimeaddr_null);

}

/*---------------------------------------------------------------------------*/

static uint8_t
add_sink(pos_t pos, rimeaddr_t *addr)
{
	int8_t sink;

	sink = get_index_by_addr(addr, my_sink_list.addr, MAX_SINKS);

	/* If sink is -1, this sink was not found in our list, and we
     allocate the new sink */
	if(sink == -1) {
		sink = get_index_by_addr(&rimeaddr_null, my_sink_list.addr, MAX_SINKS);

		/* If we could not allocate a new sink entry, we give up. */
		if(sink == -1) {
			return -1;
		}

		/* Initialize the fields. */
		rimeaddr_copy(&my_sink_list.addr[sink], addr);

		my_sink_list.sinks[sink] = sink;

		/* Register the sink position */
		my_sink_list.pos[sink].x = pos.x;
		my_sink_list.pos[sink].y = pos.y;

		if(sink == 0){
			my_sink_list.count = 1;
		}
		else{
			my_sink_list.count++;
		}

	}

	return sink;
};

/*---------------------------------------------------------------------------*/

static uint8_t
//add_neighbor(pos_t pos, rimeaddr_t *addr, int8_t *sink_conn)
add_neighbor(pos_t pos, rimeaddr_t *addr)
{
	int8_t node;

	node = get_index_by_addr(addr, neighbors_list.addr, MAX_NEIGHBORS);

	/* If node is -1, this neighbor was not found in our list, and we
	     allocate the new neighbor. */
	if(node == -1) {
		node = get_index_by_addr(&rimeaddr_null, neighbors_list.addr, MAX_NEIGHBORS);

		/* If we could not allocate a new neighbor entry, we give up. We
       could have reused an old neighbor entry, but we do not do this
       for now. */ // TODO ----> look for a worst neighbor to replace
		if(node == -1) {
			return -1;
		}

		/* Initialize the fields. */
		rimeaddr_copy(&neighbors_list.addr[node], addr);

		/* set its position */
		neighbors_list.pos[node].x = pos.x;
		neighbors_list.pos[node].y = pos.y;

		/* initialize the sink_list and the sink_void_list */
		memset(neighbors_list.sink_list[node], -1, sizeof(int8_t)*MAX_SINKS);

		/* set the sink_list */
		closest_sinks(neighbors_list.sink_list[node], neighbors_list.pos[node], own_pos);

		/* set the is_sink field */
		neighbors_list.is_sink[node] = neighbor_is_sink(addr);

		/* set the packet counter */
		neighbors_list.packet_count[node] = 0;

		/* set the neighbor's energy consumption */
		neighbors_list.consumed_energy[node] = 0;
	}

	/* set the list of connected sinks */
	//memcpy(neighbors_list.sink_conn[node], sink_conn, MAX_SINKS*sizeof(int8_t));

	struct encounter *enc = get_encounter_pointer_by_addr(addr);

	if(enc!=NULL){
		neighbors_list.enc[node] = enc;
	}

	return node;
};

/*****************************************************************************/
/* Broadcast Process                                                         */
/*****************************************************************************/

/*---------------------------------------------------------------------------*/
/* This function is called whenever a broadcast message is received. */
static void
broadcast_recv(struct broadcast_conn *c, const rimeaddr_t *from)
{
	broadcast_pkt_t broadcast_pkt;

	register_energy_cost(ENERGY_COST_RX(broadcast_pkt_t), RX, CONTROL);

	/* copying to avoid unalignment issues */
	memcpy(&broadcast_pkt, packetbuf_dataptr(), sizeof(broadcast_pkt_t));

	//PRINTF("received broadcast from: %d.%d\n", from->u8[0], from->u8[1]);
	//PRINTF("ver: %d, type: %d\n", broadcast_pkt.hdr.ver, broadcast_pkt.hdr.type);

	if(broadcast_pkt.hdr.ver != KANYCAST_VERSION) {
		return;
	}

	if(broadcast_pkt.hdr.type == KANYCAST_BROADCAST_POS) {

		uint8_t node, i, j;
		//int8_t sink_conn[MAX_SINKS];

		//memcpy(sink_conn, broadcast_pkt.sink_conn, sizeof(uint8_t)*MAX_SINKS);

		//node = add_neighbor(broadcast_pkt.hdr.pos, (rimeaddr_t*)from, sink_conn);
		node = add_neighbor(broadcast_pkt.hdr.pos, (rimeaddr_t*)from);
		//PRINTF("Neighbor %d.%d added under index: %d. \n", neighbors_list.addr[node].u8[0], neighbors_list.addr[node].u8[1], node);

		if(node>=0){
			//PRINTF("updated neighbor: %d.%d, ", neighbors_list.addr[node].u8[0], neighbors_list.addr[node].u8[1]);

			//List of voids or unreachable sinks
			update_sink_void(broadcast_pkt.sink_void_list, node);

			neighbors_list.consumed_energy[node] = broadcast_pkt.consumed_energy;
		}
	}
}
/*---------------------------------------------------------------------------*/
/* Declare the broadcast  structures */
static struct broadcast_conn broadcast;
/* This is where we define what function to be called when a broadcast
   is received. We pass a pointer to this structure in the
   broadcast_open() call below. */
static const struct broadcast_callbacks broadcast_call = {broadcast_recv};
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(broadcast_process, ev, data)
{
	PROCESS_EXITHANDLER(broadcast_close(&broadcast);)

	PROCESS_BEGIN();

	static struct etimer et;

	broadcast_pos_event = process_alloc_event();

	broadcast_open(&broadcast, BROADCAST_CHANNEL, &broadcast_call);

	/* Send a broadcast every BROADCAST_PERIOD with a jitter of half of BROADCAST_PERIOD */
	etimer_set(&et, CLOCK_SECOND + random_rand()%(2*CLOCK_SECOND));

	while(1) {
		PROCESS_WAIT_EVENT();
		// uint16_t period = clock_seconds() < BOOTSTRAP_TIME ? BROADCAST_PERIOD : 2*BROADCAST_PERIOD;
		if(etimer_expired(&et)) {

			broadcast_pkt_t broadcast_pkt;

			/* Send a broadcast every BROADCAST_PERIOD with a jitter of half of BROADCAST_PERIOD */
			etimer_set(&et, CLOCK_SECOND*BROADCAST_PERIOD + random_rand()%((BROADCAST_PERIOD+1)*CLOCK_SECOND));

			// prepare the broadcast packet
			broadcast_pkt.hdr.ver = KANYCAST_VERSION;
			broadcast_pkt.hdr.type = KANYCAST_BROADCAST_POS;
			broadcast_pkt.hdr.pos = own_pos;
			broadcast_pkt.consumed_energy = sensor_metrics()->energy_cost_tx + sensor_metrics()->energy_cost_rx + sensor_metrics()->ctrl_energy_cost_tx + sensor_metrics()->ctrl_energy_cost_rx;
			//memset(broadcast_pkt.sink_conn, -1, sizeof(int8_t)*MAX_SINKS);

			memcpy(broadcast_pkt.sink_void_list, my_sink_void_list, sizeof(int8_t)*MAX_SINKS);

			/*uint8_t i;
			for(i=0; i<MAX_SINKS; i++){
				if(get_index_by_addr(&my_sink_list.addr[i], neighbors_list.addr, MAX_NEIGHBORS)>=0){
					broadcast_pkt.sink_conn[i]=i;
				}
			}*/

			//printf("Broadcast from %d.%d sent. Packet size: %d\n", rimeaddr_node_addr.u8[0], rimeaddr_node_addr.u8[1], sizeof(broadcast_pkt_t));

			packetbuf_copyfrom(&broadcast_pkt, sizeof(broadcast_pkt_t));

			register_energy_cost(ENERGY_COST_TX_CTRL(MAX_COMMUNICATION_RANGE, broadcast_pkt_t), TX, CONTROL);

			broadcast_send(&broadcast);
		}
	}

	PROCESS_END();
}

/*---------------------------------------------------------------------------*/

void sink_void_notification(const int8_t *found_sinks, const int8_t *allowed_sinks){

	if(VOID_CONTROL_MESSAGE){
		int8_t i, void_added=0;

		i = count_valid(allowed_sinks, MAX_SINKS)-count_valid(found_sinks, MAX_SINKS);
		if(i>0){
			//printf("Node %d.%d detected %d sinks in the void list: ",rimeaddr_node_addr.u8[0], rimeaddr_node_addr.u8[1], i);
			for(i=0; i<MAX_SINKS; i++){
				if(allowed_sinks[i]>=0 && found_sinks[i]==-1){
					if(my_sink_void_list[i]==-1){
						my_sink_void_list[i]=i;
						void_added=1;
						//printf("*");
					}
					//printf(" %d.%d | ", my_sink_list.addr[i].u8[0], my_sink_list.addr[i].u8[1]);
				}
			}
			//printf("\n");
		}
	}
}

/*---------------------------------------------------------------------------*/

void update_sink_void(int8_t *sink_void_list, int8_t node){

	if(VOID_CONTROL_MESSAGE){
		int8_t OK, i, j;

		if(node>=0){
			for(i=0;i<MAX_SINKS;i++){
				if(sink_void_list[i]>=0){
					neighbors_list.sink_list[node][i] = -1;
					OK = 0;
					for(j=0;j<MAX_NEIGHBORS;j++){
						if(neighbors_list.sink_list[j][i]>=0){
							OK = 1;
						}
					}
					if(OK==0){
						my_sink_void_list[i]=i;
					}
				}
			}
		}
	}
}

/*---------------------------------------------------------------------------*/

/*****************************************************************************/
/* Multihop Process                                                          */
/*****************************************************************************/

/*---------------------------------------------------------------------------*/
/*
 * This function is called at the final recipient of the message.
 */
static void
recv(struct multihop_conn *c, const rimeaddr_t *sender,
		const rimeaddr_t *prevhop,
		uint8_t hops)
{

	reading_pkt_t reading_pkt;

	memcpy(&reading_pkt, packetbuf_dataptr(), sizeof(reading_pkt_t));

	if(reading_pkt.reading_hdr.hdr.ver != KANYCAST_VERSION) {
		return;
	}

	if(reading_pkt.reading_hdr.hdr.type == KANYCAST_MULTIHOP) {
		register_energy_cost(ENERGY_COST_RX(reading_pkt_t), RX, DATA);
		packet_received(&reading_pkt);
		PRINTF("Packet received from: %d.%d with originator: %d.%d\n", prevhop->u8[0], prevhop->u8[1], reading_pkt.reading_hdr.hdr.originator.u8[0],reading_pkt.reading_hdr.hdr.originator.u8[1]);
	}
}
/*---------------------------------------------------------------------------*/
/*
 * This function is called to forward a packet. The function picks the
 * neighbor closest to the destination from the neighbor list. If no
 * neighbor is found, the function returns NULL to signal to the multihop layer
 * that the packet should be dropped.
 */
static rimeaddr_t *
forward(struct multihop_conn *c,
		const rimeaddr_t *originator, const rimeaddr_t *dest,
		const rimeaddr_t *prevhop, uint8_t hops)
{

	reading_pkt_t reading_pkt;
	// The packetbuf_dataptr() returns a pointer to the first data byte in the received packet.
	memcpy(&reading_pkt, (reading_pkt_t *)packetbuf_dataptr(), sizeof(reading_pkt_t));

	if(reading_pkt.reading_hdr.prep==0 && !rimeaddr_cmp(&reading_pkt.reading_hdr.hdr.originator, &rimeaddr_node_addr)){

		PRINTF("--- Packet received! \n");

		register_energy_cost(ENERGY_COST_RX(reading_pkt_t), RX, DATA);
		reading_pkt.energy_cost=reading_pkt.energy_cost+ENERGY_COST_RX(reading_pkt_t);
	}

	PRINTF("**Multihop process started!**\n");

	int8_t i, node;
	//Check if it's a packet that was duplicated
	if(reading_pkt.reading_hdr.prep==1){

		node = get_index_by_addr(&reading_pkt.reading_hdr.forwarder, neighbors_list.addr, MAX_NEIGHBORS);
		neighbors_list.packet_count[node]++;

		clock_time_t latency;

		latency = (clock_time_t)((RTIMER_NOW()/(RTIMER_SECOND/1000)) + my_offset) - reading_pkt.timestamp;

		PRINTF_LOG("[GeoK] Forwarding packet ID: %d-%d-%lu to %d.%d, ", reading_pkt.reading_hdr.hdr.originator.u8[0], reading_pkt.reading_hdr.hdr.originator.u8[1], reading_pkt.timestamp_original, reading_pkt.reading_hdr.forwarder.u8[0], reading_pkt.reading_hdr.forwarder.u8[1]);
		PRINTF_LOG("k: %d, sinks: ", reading_pkt.reading_hdr.k);
		for(i=0;i<MAX_SINKS;i++){
			if(!rimeaddr_cmp(&rimeaddr_null, &reading_pkt.reading_hdr.allowed_sinks[i])){
				PRINTF_LOG("%d.%d, ", reading_pkt.reading_hdr.allowed_sinks[i].u8[0], reading_pkt.reading_hdr.allowed_sinks[i].u8[1]);
			}
		}
		PRINTF_LOG("hops: %d | ", reading_pkt.reading_hdr.hdr.hops);
		PRINTF_LOG("Latency: %lu ms | ", latency);
		PRINTF_LOG("Originator: %d.%d | Packet size: %d bytes", reading_pkt.reading_hdr.hdr.originator.u8[0], reading_pkt.reading_hdr.hdr.originator.u8[1], sizeof(reading_pkt_t));
		if(reading_pkt.reading_hdr.recovery_flag==1){
			PRINTF_LOG("| Recovery");
		}
		PRINTF_LOG(" | dest: %d.%d", my_sink_list.addr[reading_pkt.reading_hdr.dest].u8[0], my_sink_list.addr[reading_pkt.reading_hdr.dest].u8[1]);
		PRINTF_LOG("\n");

		reading_pkt.reading_hdr.prep = 0;

		packetbuf_copyfrom(&reading_pkt, sizeof(reading_pkt_t));

		if(get_index_by_addr(&reading_pkt.reading_hdr.forwarder, reading_pkt.reading_hdr.allowed_sinks, MAX_SINKS)>=0 && reading_pkt.reading_hdr.k==1){
			packetbuf_set_addr(PACKETBUF_ADDR_ERECEIVER, &reading_pkt.reading_hdr.forwarder);
		}

		register_forwarded_packet();
		register_energy_cost(ENERGY_COST_TX(own_pos,neighbors_list.pos[node],reading_pkt_t), TX, DATA);

		PRINTF("Packet ready to be sent! \n");
		//print_pkt_info(&reading_pkt);

		return &reading_pkt.reading_hdr.forwarder;
	}

	if(reading_pkt.reading_hdr.hdr.ver != KANYCAST_VERSION) {
		PRINTF("ERROR: Wrong packet type! \n");
		return NULL;
	}

	//Check if the node is within the destination sinks
	i=get_index_by_addr(&rimeaddr_node_addr, reading_pkt.reading_hdr.allowed_sinks, MAX_SINKS);
	if(i>=0){
		rimeaddr_copy(&reading_pkt.reading_hdr.allowed_sinks[i], &rimeaddr_null);
		reading_pkt.reading_hdr.k = reading_pkt.reading_hdr.k - 1;

		packet_received(&reading_pkt);
	}

	if(reading_pkt.reading_hdr.k==0){
		PRINTF("ERROR: k is already 0! \n");
		return NULL;
	}

	int8_t allowed_sinks[MAX_SINKS];
	memset(allowed_sinks, -1, MAX_SINKS*sizeof(int8_t));
	for(i=0; i<MAX_SINKS; i++){
		if(!rimeaddr_cmp(&reading_pkt.reading_hdr.allowed_sinks[i], &rimeaddr_null)){
			allowed_sinks[i] = i;
		}
	}

	//List of Candidates
	kcandidates cand_fwdrs;
	memset(&cand_fwdrs, 0, sizeof(kcandidates));
	memset(cand_fwdrs.neighbors, -1, sizeof(int8_t)*MAX_NEIGHBORS);
	memset(cand_fwdrs.sinks, -1, sizeof(int8_t)*MAX_NEIGHBORS*MAX_SINKS);

	PRINTF("Selecting candidate neighbors! \n");
	cand_fwdrs.recovery_ref.x = reading_pkt.reading_hdr.recovery_ref.x;
	cand_fwdrs.recovery_ref.y = reading_pkt.reading_hdr.recovery_ref.y;
	cand_fwdrs.recovery_flag = reading_pkt.reading_hdr.recovery_flag;
	rimeaddr_copy(&cand_fwdrs.recovery_trigger_addr, &reading_pkt.reading_hdr.recovery_trigger_addr);

	//DETECT LOOP
	if(count_loop.id_1==reading_pkt.reading_hdr.hdr.originator.u8[0] && count_loop.id_2==reading_pkt.reading_hdr.hdr.originator.u8[1] && count_loop.id_3==reading_pkt.timestamp && memcmp(allowed_sinks, count_loop.sinks, MAX_SINKS*sizeof(uint8_t))==0){
		count_loop.count++;
		if(count_loop.count==4){
			if(reading_pkt.reading_hdr.dest>=0){
				int8_t black_list_sinks[MAX_SINKS];
				memset(black_list_sinks, -1, MAX_SINKS*sizeof(int8_t));
				black_list_sinks[reading_pkt.reading_hdr.dest]=reading_pkt.reading_hdr.dest;
				for(i=0; i<MAX_NEIGHBORS; i++){
					if(count_loop.black_list[i]==0){
						update_sink_void(black_list_sinks, i);
					}
				}
			}
		}
		if(count_loop.count>5){
			PRINTF_LOG("[GeoK] Loop detected with packet ID: %d-%d-%lu. Packet was dropped.\n", reading_pkt.reading_hdr.hdr.originator.u8[0], reading_pkt.reading_hdr.hdr.originator.u8[1], reading_pkt.timestamp_original);
			return NULL;
		}
	}
	else{
		count_loop.id_1=reading_pkt.reading_hdr.hdr.originator.u8[0];
		count_loop.id_2=reading_pkt.reading_hdr.hdr.originator.u8[1];
		count_loop.id_3=reading_pkt.timestamp_original;
		memcpy(count_loop.sinks, allowed_sinks, MAX_SINKS*sizeof(uint8_t));
		count_loop.count=1;
	}

	//clock_time_t calc_time;
	//calc_time = (clock_time_t)((RTIMER_NOW()/(RTIMER_SECOND/1000)));

	int8_t sender = get_index_by_addr(&reading_pkt.reading_hdr.prev, neighbors_list.addr, MAX_NEIGHBORS);
	candidate_forwarders(&cand_fwdrs, reading_pkt.reading_hdr.min_weight, updated_weight(allowed_sinks, -1, reading_pkt.reading_hdr.k), sender, allowed_sinks, reading_pkt.reading_hdr.k, &reading_pkt);

	PRINTF("Candidates selected! \n");

	//calc_time = (clock_time_t)((RTIMER_NOW()/(RTIMER_SECOND/1000))) - calc_time;
	//printf("total calc time: %d ms\n", calc_time);

	return NULL;
}

/*---------------------------------------------------------------------------*/

//void send_packet(kforwarders *fwdrs, kcandidates *cand, reading_pkt_t *reading_pkt, int8_t count_from, double node_weight) {
void send_packet(kforwarders *fwdrs, reading_pkt_t *reading_pkt, int8_t count_from, double node_weight) {

#ifdef CALCULATION_TIME
	clock_time_t first_mark, original_timestamp;
	first_mark = (clock_time_t)(SYSNOW+get_node_offset());
	original_timestamp = reading_pkt->timestamp;
#endif

	if(fwdrs->count>=1){

			PRINTF("Preparing packet to be sent!\n");

			// balance the packet count to avoid overflow
			balance_packet_count();

			static reading_pkt_t *new_packet=NULL;

			if(new_packet==NULL){
				new_packet = (reading_pkt_t *)calloc(MAX_K, sizeof(reading_pkt_t));
			}

			int8_t sent[MAX_NEIGHBORS], node, i;
			memset(sent, -1, sizeof(int8_t)*MAX_NEIGHBORS);
			for(i=0;i<fwdrs->count;i++){

				node = next_by_awake_time(fwdrs, sent, fwdrs->count-i);
				sent[node] = node;

				//Configuring the packet
				new_packet[count_from+i].reading_hdr.hdr.ver = reading_pkt->reading_hdr.hdr.ver;
				new_packet[count_from+i].reading_hdr.hdr.type = reading_pkt->reading_hdr.hdr.type;
				new_packet[count_from+i].reading_hdr.hdr.hops = reading_pkt->reading_hdr.hdr.hops + 1;
				new_packet[count_from+i].reading_hdr.hdr.pos.x = reading_pkt->reading_hdr.hdr.pos.x;
				new_packet[count_from+i].reading_hdr.hdr.pos.y = reading_pkt->reading_hdr.hdr.pos.y;
				rimeaddr_copy(&new_packet[count_from+i].reading_hdr.hdr.originator, &reading_pkt->reading_hdr.hdr.originator);

				if(fwdrs->recovery_flag[node]==1){
					new_packet[count_from+i].reading_hdr.dest = reading_pkt->reading_hdr.dest;
					new_packet[count_from+i].reading_hdr.min_weight = reading_pkt->reading_hdr.min_weight;
				}
				else{
					new_packet[count_from+i].reading_hdr.dest = fwdrs->dest[node];
					new_packet[count_from+i].reading_hdr.min_weight = updated_weight(fwdrs->sinks[node], -1, fwdrs->k[node]);
				}

				new_packet[count_from+i].reading_hdr.k = fwdrs->k[node];
				//new_packet[count_from+i].reading_hdr.recovery_ref.x = cand->recovery_ref.x;
				//new_packet[count_from+i].reading_hdr.recovery_ref.y = cand->recovery_ref.y;
				new_packet[count_from+i].reading_hdr.recovery_ref.x = reading_pkt->reading_hdr.recovery_ref.x;
				new_packet[count_from+i].reading_hdr.recovery_ref.y = reading_pkt->reading_hdr.recovery_ref.y;
				new_packet[count_from+i].reading_hdr.recovery_flag = fwdrs->recovery_flag[node];
				//rimeaddr_copy(&new_packet[count_from+i].reading_hdr.recovery_trigger_addr, &cand->recovery_trigger_addr);
				rimeaddr_copy(&new_packet[count_from+i].reading_hdr.recovery_trigger_addr, &reading_pkt->reading_hdr.recovery_trigger_addr);
				memset(new_packet[count_from+i].reading_hdr.allowed_sinks, 0, sizeof(rimeaddr_t)*MAX_SINKS);
				uint8_t j;
				for(j=0; j<MAX_SINKS; j++){
					if(fwdrs->sinks[node][j]>=0){
						rimeaddr_copy(&new_packet[count_from+i].reading_hdr.allowed_sinks[j], &my_sink_list.addr[j]);
					}
				}
				rimeaddr_copy(&new_packet[count_from+i].reading_hdr.forwarder, &neighbors_list.addr[node]);
				rimeaddr_copy(&new_packet[count_from+i].reading_hdr.prev, &rimeaddr_node_addr);
				new_packet[count_from+i].reading_hdr.prep = 1;

				new_packet[count_from+i].value = reading_pkt->value;
				new_packet[count_from+i].timestamp = reading_pkt->timestamp;
				new_packet[count_from+i].timestamp_original = reading_pkt->timestamp_original;

				//Calculate the energy cost
				new_packet[count_from+i].energy_cost=reading_pkt->energy_cost+ENERGY_COST_TX(own_pos,neighbors_list.pos[node],reading_pkt_t);

			#ifdef CALCULATION_TIME
				new_packet[count_from+i].timestamp = (clock_time_t)(original_timestamp + (SYSNOW + get_node_offset() - first_mark));
			#endif

				//send out the duplication
				if(process_post(&multihop_process, multipacket_event, (void*)&new_packet[count_from+i])==PROCESS_ERR_OK){
					process_poll(&multihop_process);
					PRINTF("New multihop event posted to %d.%d, k = %d! \n", new_packet[count_from+i].reading_hdr.forwarder.u8[0], new_packet[count_from+i].reading_hdr.forwarder.u8[1], new_packet[count_from+i].reading_hdr.k);
				}
				else{
					PRINTF("ERROR: Multihop event for packet duplication FAILED (Neighbor: %d.%d, k = %d)! \n", neighbors_list.addr[node].u8[0], neighbors_list.addr[node].u8[1], fwdrs->k[node]);
				}

				if(count_loop.id_1==reading_pkt->reading_hdr.hdr.originator.u8[0] && count_loop.id_2==reading_pkt->reading_hdr.hdr.originator.u8[1] && count_loop.id_3==reading_pkt->timestamp_original && memcmp(fwdrs->sinks[node], count_loop.sinks, MAX_SINKS*sizeof(uint8_t))==0){
					count_loop.black_list[node] = 0;
				}
			}
		}
}

/*---------------------------------------------------------------------------*/

void new_packet_generation(void *data) {

	/* Send the broadcast message with the sink void list */
	process_post(&multihop_process, newpacket_event, data);
	process_poll(&multihop_process);

	//UPDATE NODE METRICS
	register_generated_packet();
}

/*---------------------------------------------------------------------------*/

void packet_received(reading_pkt_t *reading_pkt_in){

	clock_time_t latency;

	latency = (clock_time_t)((RTIMER_NOW()/(RTIMER_SECOND/1000)) + my_offset) - reading_pkt_in->timestamp;

	register_received_packet();
	register_latency(latency);

	PRINTF_LOG("[GeoK] Latency: %lu ms | ", latency);
	PRINTF_LOG("Originator: %d.%d | ", reading_pkt_in->reading_hdr.hdr.originator.u8[0], reading_pkt_in->reading_hdr.hdr.originator.u8[1]);
	PRINTF_LOG("Previous Sender: %d.%d | ", reading_pkt_in->reading_hdr.prev.u8[0], reading_pkt_in->reading_hdr.prev.u8[1]);
	PRINTF_LOG("Hops: %d | ", reading_pkt_in->reading_hdr.hdr.hops);
	PRINTF_LOG("Consumed Energy (TX+RX): %e J | ", reading_pkt_in->energy_cost);
	PRINTF_LOG("Packet ID: %d-%d-%lu | ", reading_pkt_in->reading_hdr.hdr.originator.u8[0], reading_pkt_in->reading_hdr.hdr.originator.u8[1], reading_pkt_in->timestamp_original);
	PRINTF_LOG("Packet Size: %d bytes\n", sizeof(reading_pkt_t));
}

/*---------------------------------------------------------------------------*/
/* Declare multihop structures */
static const struct multihop_callbacks multihop_call = {recv, forward};
static struct multihop_conn multihop;
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(multihop_process, ev, data)
{
	PROCESS_EXITHANDLER(multihop_close(&multihop);)

		  PROCESS_BEGIN();

	static rimeaddr_t to;

	multipacket_event = process_alloc_event();
	newpacket_event = process_alloc_event();

	/* Open a multihop connection on Rime channel MULTIHOP_CHANNEL. */
	multihop_open(&multihop, MULTIHOP_CHANNEL, &multihop_call);

	/* Set the Rime address of the final receiver of the packet to
     254.254 as we rely on the x,y coordinates to deliver the packet.
     once the recipient is found the address of the destination is updated. */
	to.u8[0] = 254;
	to.u8[1] = 254;

	/* Loop forever, send a packet when the button is pressed. */
	while(1) {
		/* Wait until we get a sensor event with the button sensor as data. */
		PRINTF("Waiting for multihop packet event.\n");
		PROCESS_WAIT_EVENT();
		if (ev == newpacket_event) {
			/* this event is just for debug */
			PRINTF("Creating multihop packet\n");

			reading_pkt_t reading_pkt;

			memset(&reading_pkt, 0, sizeof(reading_pkt_t));

			memset(reading_pkt.reading_hdr.allowed_sinks, 0, sizeof(rimeaddr_t)*MAX_SINKS);
			memcpy(reading_pkt.reading_hdr.allowed_sinks, my_sink_list.addr, sizeof(rimeaddr_t)*MAX_SINKS);

			/* prepare the multihop packet */
			reading_pkt.reading_hdr.hdr.ver = KANYCAST_VERSION;
			reading_pkt.reading_hdr.hdr.type = KANYCAST_MULTIHOP;
			reading_pkt.reading_hdr.hdr.pos.x = own_pos.x;
			reading_pkt.reading_hdr.hdr.pos.y = own_pos.y;
			reading_pkt.reading_hdr.hdr.hops = 0;
			rimeaddr_copy(&reading_pkt.reading_hdr.hdr.originator, &rimeaddr_node_addr);
			reading_pkt.reading_hdr.k = MAX_K;
			reading_pkt.reading_hdr.min_weight = -1;//updated_weight(my_sink_list.sinks, -1, MAX_K);
			reading_pkt.reading_hdr.prep = 0;
			reading_pkt.reading_hdr.recovery_ref.x = 0.0;
			reading_pkt.reading_hdr.recovery_ref.y = 0.0;
			reading_pkt.reading_hdr.recovery_flag = 0;
			reading_pkt.reading_hdr.dest = -1;

			prepare_reading_pkt(&reading_pkt);

			/* Copy the reading to the packet buffer. */
			packetbuf_copyfrom(&reading_pkt, sizeof(reading_pkt_t));

			/* Send the packet. */
			multihop_send(&multihop, &to);

			PRINTF("Created multihop packet posted.\n");

		}
		else{
			if (ev == multipacket_event) {

				reading_pkt_t *reading_pkt;

				reading_pkt = (reading_pkt_t *)data;

				PRINTF("Sending multihop packet\n");

				/* Copy the reading to the packet buffer. */
				packetbuf_copyfrom((reading_pkt_t *)data, sizeof(reading_pkt_t));

				/* Send the packet. */
				multihop_send(&multihop, &to);

				PRINTF("Multihop packet sent.\n");
			}
		}
	}

	PROCESS_END();
}

/*---------------------------------------------------------------------------*/

/*****************************************************************************/
/* Boot Process                                                              */
/*****************************************************************************/

void receive_info(char *data_copy, clock_time_t internal_ref_time){

	int c;
	char str[SERIAL_LINE_CONF_BUFSIZE], *aux;
	pos_t rcvd_pos = {0.0 , 0.0};
	rimeaddr_t sink_addr;
	clock_time_t ref=0;

	//Initialize string
	memset(str, NULL, sizeof(char)*SERIAL_LINE_CONF_BUFSIZE);
	strcpy(str, data_copy);

	c=0;
	while(str[c]!='#'){
		if(str[c]=='t'){
			c+=2;
			aux=&str[c];
			while(str[c]!='|') c++;
			str[c]='\0';
			ref = (clock_time_t)stoi((char *)aux);
			memcpy(str, data_copy,SERIAL_LINE_CONF_BUFSIZE*sizeof(char));
			printf("Reference time received: %d.\n", ref);
		}
		if(str[c]=='k'){
			c+=2;
			aux=&str[c];
			while(str[c]!='|') c++;
			str[c]='\0';
			received_max_k = (uint8_t)stoi((char *)aux);
			memcpy(str, data_copy,SERIAL_LINE_CONF_BUFSIZE*sizeof(char));
			printf("Maximum K received: %d.\n", received_max_k);
		}
		if(str[c]=='n'){
			c+=2;
			aux=&str[c];
			while(str[c]!=';') c++;
			str[c]='\0';
			c++;
			own_pos.x = stof((char *)aux);
			memcpy(str, data_copy,SERIAL_LINE_CONF_BUFSIZE*sizeof(char));
			aux=&str[c];
			while(str[c]!='|') c++;
			str[c]='\0';
			own_pos.y = stof((char *)aux);
			memcpy(str, data_copy,SERIAL_LINE_CONF_BUFSIZE*sizeof(char));
			printf("Coords X,Y received.\n");
		}
		if(str[c]=='s'){
			c+=2;
			aux=&str[c];
			sink_addr.u8[0]=(unsigned char)stoi((char *)aux);
			c+=2;
			aux=&str[c];
			sink_addr.u8[1]=(unsigned char)stoi((char *)aux);
			while(str[c]!=';') c++;
			str[c]='\0';
			c++;
			memcpy(str, data_copy,SERIAL_LINE_CONF_BUFSIZE*sizeof(char));
			aux=&str[c];
			while(str[c]!=';') c++;
			str[c]='\0';
			c++;
			rcvd_pos.x = stof((char *)aux);
			memcpy(str, data_copy,SERIAL_LINE_CONF_BUFSIZE*sizeof(char));
			aux=&str[c];
			while(str[c]!='|') c++;
			str[c]='\0';
			rcvd_pos.y = stof((char *)aux);
			memcpy(str, data_copy,SERIAL_LINE_CONF_BUFSIZE*sizeof(char));
			if(add_sink(rcvd_pos, &sink_addr)==-1){
				printf("Sink %d.%d was not included.\n", sink_addr.u8[0], sink_addr.u8[1]);
			}
			else{
				printf("Sink %d.%d was included.\n", sink_addr.u8[0], sink_addr.u8[1]);
				if(rimeaddr_cmp(&rimeaddr_node_addr, &sink_addr)){
					i_am_sink = 1;
				}
			}
		}
		c++;
	}

	if(ref>0){
		// Calculate the offset for network synchronization
		my_offset = ref - internal_ref_time;
		printf("My offset: %d\n", my_offset);
	}
}

/*---------------------------------------------------------------------------*/

void geo_kanycast_init() {

	//starts the serial line process
	serial_line_init();

	process_start(&boot_process, NULL);
}

/*---------------------------------------------------------------------------*/

PROCESS_THREAD(boot_process, ev, data)
{

	PROCESS_BEGIN();

	char data1[SERIAL_LINE_CONF_BUFSIZE], data2[SERIAL_LINE_CONF_BUFSIZE], data3[SERIAL_LINE_CONF_BUFSIZE], data4[SERIAL_LINE_CONF_BUFSIZE];
	clock_time_t internal_ref_time;

	/* Initialize the list used for the neighbor table. */
	memset(&neighbors_list, 0, sizeof(kneighbor));
	//memset(neighbors_list.sink_conn, -1, sizeof(int8_t)*MAX_NEIGHBORS*MAX_SINKS);
	memset(neighbors_list.sink_list, -1, sizeof(int8_t)*MAX_NEIGHBORS*MAX_SINKS);

	/* Initialize the list used for the sink table. */
	memset(&my_sink_list, 0, sizeof(ksink));
	memset(my_sink_list.sinks, -1, sizeof(int8_t)*MAX_SINKS);

	//initialize my_sink_void_list
	memset(my_sink_void_list, -1, sizeof(int8_t)*MAX_SINKS);

	memset(data1, NULL, sizeof(char)*SERIAL_LINE_CONF_BUFSIZE);
	memset(data2, NULL, sizeof(char)*SERIAL_LINE_CONF_BUFSIZE);
	memset(data3, NULL, sizeof(char)*SERIAL_LINE_CONF_BUFSIZE);
	memset(data4, NULL, sizeof(char)*SERIAL_LINE_CONF_BUFSIZE);

	// SINK DETECTION
	// get the Sink list and coordinates from COOJA
	// get the node coordinates from cooja, adapted from:
	// https://sourceforge.net/p/contiki/mailman/message/34696644/
	printf("Waiting for node coordinates from Cooja...\n");
	PROCESS_WAIT_EVENT_UNTIL(ev == serial_line_event_message);
	internal_ref_time = (clock_time_t)(RTIMER_NOW()/(RTIMER_SECOND/1000));
	strcpy(data1, (char *)data);
	printf("data1: %s\n", data1);

	printf("Received-1 - waiting for Part 2...\n");
	PROCESS_WAIT_EVENT_UNTIL(ev == serial_line_event_message);
	strcpy(data2, (char *)data);
	printf("data2: %s\n", data2);

	printf("Received-2 - waiting for Part 3...\n");
	PROCESS_WAIT_EVENT_UNTIL(ev == serial_line_event_message);
	strcpy(data3, (char *)data);
	printf("data3: %s\n", data3);

	printf("Received-3 - waiting for Part 4...\n");
	PROCESS_WAIT_EVENT_UNTIL(ev == serial_line_event_message);
	strcpy(data4, (char *)data);
	printf("data4: %s\n", data4);

	received_max_k = 0;

	receive_info(data1, internal_ref_time);
	receive_info(data2, 0);
	receive_info(data3, 0);
	receive_info(data4, 0);

	count_loop.id_1 = 0;
	count_loop.id_2 = 0;
	count_loop.id_3 = 0;
	count_loop.count = 0;
	memset(count_loop.black_list, -1, sizeof(int8_t)*MAX_NEIGHBORS);

	//Initialize the metrics variables
	my_metrics_init();

	/* start broadcast process */
	process_start(&broadcast_process, NULL);

	/* start multihop process */
	process_start(&multihop_process, NULL);

	printf("%d.%d: Booting completed.\n", rimeaddr_node_addr.u8[0], rimeaddr_node_addr.u8[1]);

	PROCESS_END();
}
/*---------------------------------------------------------------------------*/
