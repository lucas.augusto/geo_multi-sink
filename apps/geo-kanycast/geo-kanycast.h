#ifndef KANYCAST_H
#define KANYCAST_H

#include "lib/list.h"
#include "lib/memb.h"
#include "net/rime.h"
#include "sys/rtimer.h"

#include <stdint.h>

#include "support.h"
#include "packets.h"

#define KANYCAST_VERSION 		1
#define KANYCAST_BROADCAST_POS  1
#define KANYCAST_MULTIHOP 		2

/* This structure holds information about sinks. */
typedef struct {
  /* The ->addr field holds the Rime address of the sink. */
  rimeaddr_t addr[MAX_SINKS];

  /* The list of sinks */
  int8_t sinks[MAX_SINKS];

  /* The ->pos field holds the location of the sink */
  pos_t pos[MAX_SINKS];

  /* Count of sinks */
  uint8_t count;

} ksink;

/* This structure holds information about neighbors. */
typedef struct  {
  /* The ->addr field holds the Rime address of the neighbor. */
  rimeaddr_t addr[MAX_NEIGHBORS];

  /* The ->pos field holds the location of the neighbor */
  pos_t pos[MAX_NEIGHBORS];

  /* The ->is_sink holds the info if the neighbor is a sink */
  int8_t is_sink[MAX_NEIGHBORS];

  /* The ->sink_conn holds the info if the neighbor is connected to a sink */
  //int8_t sink_conn[MAX_NEIGHBORS][MAX_SINKS];

  /* The ->sink_list holds the list of sinks */
  int8_t sink_list[MAX_NEIGHBORS][MAX_SINKS];

  /* The ->closest_to_sink holds the min distance of the neighbor neighbors to each sinks */
  float closest_to_sink[MAX_NEIGHBORS][MAX_SINKS];

  /* The ->packet_count contains the total number of packets sent to neighbor */
  uint8_t packet_count[MAX_NEIGHBORS];

  /* The ->consumed_energy contains the the value of the consumed energy by the neighbor nodes */
  double consumed_energy[MAX_NEIGHBORS];

  /* The pointer to the encounter list */
  struct encounter *enc[MAX_NEIGHBORS];

} kneighbor;

/* Structure of the Candidate Forwarders */
typedef struct {
	int8_t neighbors[MAX_NEIGHBORS];
	int8_t sinks[MAX_NEIGHBORS][MAX_SINKS];
	pos_t recovery_ref;
	rimeaddr_t recovery_trigger_addr;
	uint8_t recovery_flag;
	uint8_t count;
} kcandidates;

/* Structure of the Forwarders */
typedef struct {
	int8_t neighbors[MAX_NEIGHBORS];
	int8_t sinks[MAX_NEIGHBORS][MAX_SINKS];
	uint8_t k[MAX_NEIGHBORS];
	int8_t dest[MAX_NEIGHBORS];
	uint8_t recovery_flag[MAX_NEIGHBORS];
	uint8_t count;
} kforwarders;

/* Avoid Loop */
struct pkt_loop {
	int8_t id_1;
	int8_t id_2;
	unsigned long id_3;
	uint8_t sinks[MAX_SINKS];
	uint8_t count;
	int8_t black_list[MAX_NEIGHBORS];
};

extern pos_t own_pos;

extern process_event_t broadcast_event;

PROCESS_NAME(broadcast_process);
PROCESS_NAME(multihop_process);

uint8_t get_received_max_k();

int8_t * get_sink_void_list();

kneighbor * get_neighbor_list();

ksink * get_sink_list();

clock_time_t get_node_offset();

uint8_t is_sink();

void geo_kanycast_init();

void print_neighbors();

void print_sinks();

void sink_void_notification(const int8_t *found_sinks, const int8_t *allowed_sinks);

//void send_packet(kforwarders *fwdrs, kcandidates *cand, reading_pkt_t *reading_pkt, int8_t count_from, double node_weight);
void send_packet(kforwarders *fwdrs, reading_pkt_t *reading_pkt, int8_t count_from, double node_weight);

void new_packet_generation(void *data);

void packet_received(reading_pkt_t *pkt);

#endif
