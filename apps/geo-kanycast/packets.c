/* contiki includes */
#include "contiki.h"
#include "net/rime.h"
#include "sys/rtimer.h"

/* standard library includes */
#include <stdio.h>  /* For printf() */
#include <string.h> /* For memcpy */

/* Project Includes */
#include "packets.h"
#include "geo-kanycast.h"

/*---------------------------------------------------------------------------*/

void prepare_reading_pkt(reading_pkt_t *reading_pkt){

	reading_pkt->value = 0;
	reading_pkt->timestamp = (clock_time_t)((RTIMER_NOW()/(RTIMER_SECOND/1000)) + get_node_offset());
	reading_pkt->timestamp_original = reading_pkt->timestamp;

	return;
}

/*---------------------------------------------------------------------------*/

void print_pkt_info(reading_pkt_t *reading_pkt){

	uint8_t i;

	printf("Packet Header: \n");
	printf("    Version: %d \n", reading_pkt->reading_hdr.hdr.ver);
	printf("    Type: %d \n", reading_pkt->reading_hdr.hdr.type);
	printf("    Pos: (%d , %d) \n", (int)reading_pkt->reading_hdr.hdr.pos.x, (int)reading_pkt->reading_hdr.hdr.pos.y);
	printf("    Originator: %d.%d \n", reading_pkt->reading_hdr.hdr.originator.u8[0], reading_pkt->reading_hdr.hdr.originator.u8[1]);
	printf("    Hops: %d \n", reading_pkt->reading_hdr.hdr.hops);

	printf("Data Packet Header: \n");
	printf("    Allowed sinks: ");
	for(i=0; i<MAX_SINKS; i++){
		if(!rimeaddr_cmp(&reading_pkt->reading_hdr.allowed_sinks[i], &rimeaddr_null)){
			printf("%d.%d, ", reading_pkt->reading_hdr.allowed_sinks[i].u8[0],reading_pkt->reading_hdr.allowed_sinks[i].u8[1]);
		}
	}
	printf("\n");

	printf("    Forwarder: %d.%d \n", reading_pkt->reading_hdr.forwarder.u8[0], reading_pkt->reading_hdr.forwarder.u8[1]);
	printf("    Prev: %d.%d \n", reading_pkt->reading_hdr.prev.u8[0], reading_pkt->reading_hdr.prev.u8[1]);
	printf("    k: %d \n", reading_pkt->reading_hdr.k);
	printf("    Prep: %d \n", reading_pkt->reading_hdr.prep);
	printf("    Recovery Reference: (%d , %d) \n", (int)reading_pkt->reading_hdr.recovery_ref.x, (int)reading_pkt->reading_hdr.recovery_ref.y);
	printf("    Recovery Trigger Address: %d \n", reading_pkt->reading_hdr.recovery_trigger_addr.u8[0], reading_pkt->reading_hdr.recovery_trigger_addr.u8[1]);
	printf("    Recovery Flag: %d \n", reading_pkt->reading_hdr.recovery_flag);
	printf("    Dest: %d \n", reading_pkt->reading_hdr.dest);
	printf("    min weight: %d.%d \n", (int)reading_pkt->reading_hdr.min_weight, decimals(reading_pkt->reading_hdr.min_weight));

	printf("Data Packet fields: \n");
	printf("    timestamp: %lu \n", reading_pkt->timestamp);
	printf("    Energy Cost: %e \n", reading_pkt->energy_cost);
	printf("    Value: %d.%d \n", (int)reading_pkt->value, decimals(reading_pkt->value));

	return;
}

