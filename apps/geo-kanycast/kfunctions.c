#include "net/rime.h"
#include "net/mac/geo_kanycast_cxmac.h"
#include "sys/rtimer.h"

#include <stdio.h> /* For printf() */

#include "kfunctions.h"
#include "support.h"
#include "geo-kanycast.h"
#include "packets.h"

//-----------------------------------------------------------------------------
/* Verifies if the neighbor is a sink */
int8_t neighbor_is_sink(const rimeaddr_t *neighbor_addr) {

	int8_t s;

	/* Check if we know this sink. */
	for(s=0; s<MAX_SINKS; s++) {
		if(!rimeaddr_cmp(&(get_sink_list()->addr[s]), &rimeaddr_null)){
			if(rimeaddr_cmp(&(get_sink_list()->addr[s]), neighbor_addr)) {
				return s;
			}
		}
	}
	return -1;
}

//-----------------------------------------------------------------------------
/* Verifies if the sink is a neighbor */
int8_t sink_is_neighbor(const rimeaddr_t *sink_addr) {

	int8_t n;

	/* Check if we know this sink. */
	for(n=0; n<MAX_NEIGHBORS; n++) {
		if(!rimeaddr_cmp(&(get_neighbor_list()->addr[n]), &rimeaddr_null)){
			if(rimeaddr_cmp(&(get_neighbor_list()->addr[n]), sink_addr)) {
				return n;
			}
		}
	}
	return -1;
}

//-----------------------------------------------------------------------------
/* Returns the list of sinks that are closer to the neighbor */
void closest_sinks(int8_t *neighbor_sink_list, pos_t neighbor, pos_t node){

	uint8_t s=0;
	float neighbor_sink=0.0, node_sink=0.0;

	for(s=0; s<MAX_SINKS; s++) {
		if(!rimeaddr_cmp(&(get_sink_list()->addr[s]), &rimeaddr_null)){
			neighbor_sink=distance(neighbor, get_sink_list()->pos[s]);
			node_sink=distance(node, get_sink_list()->pos[s]);
			if(neighbor_sink<node_sink && (node_sink>distance(node, neighbor) || neighbor_sink==0)) {
				neighbor_sink_list[s] = s;
			}
	    }
	}

	return;
}

//-----------------------------------------------------------------------------
/* Returns the count of sinks */
uint8_t count_valid(const int8_t *node_list, uint8_t list_size){

	uint8_t i=0, s_count=0;

	for(i=0;i<list_size;i++){
	    if(node_list[i]>=0){
	    	s_count++;
	    }
	}

	return s_count;
}

//-----------------------------------------------------------------------------
/* Returns the index of an array based on the rime address */
int8_t get_index_by_addr(const rimeaddr_t *item_addr, const rimeaddr_t *list_addr, uint8_t list_size){

	if(item_addr==NULL || list_addr==NULL){
		return -1;
	}

	uint8_t i;
	for(i=0;i<list_size;i++){
		if(rimeaddr_cmp(item_addr, &list_addr[i])){
			return (int)i;
		}
	}
	return -1;
}

//-----------------------------------------------------------------------------
/* Returns the pointer of a list_t based on the rime address */
struct encounter * get_encounter_pointer_by_addr(const rimeaddr_t *item_addr){

	struct encounter *ind;
	for(ind = list_head(get_encounter_list()); ind != NULL; ind = list_item_next(ind)) {
		if(rimeaddr_cmp(&ind->neighbor, item_addr)){
			return ind;
		}
	}
	return NULL;
}

//-----------------------------------------------------------------------------
/* Update the list of available sinks */
void update_available_sinks(int8_t *available_sinks, const kforwarders *fwdrs){
	uint8_t i,index;

	for(index=0;index<MAX_NEIGHBORS;index++){
		if(fwdrs->neighbors[index]>=0){
			for(i=0;i<MAX_SINKS;i++){
				if(fwdrs->sinks[index][i]>=0){
					if(available_sinks[i]>=0){
						available_sinks[i]=-1;
					}
				}
			}
		}
	}

	return;
}

//-----------------------------------------------------------------------------
/* Remove from the node's list of sinks the unavailable sinks */
void remove_unavailable_sinks(int8_t *neighbor_sink_list, const int8_t *available_sinks){

	uint8_t n;

	for(n=0;n<MAX_SINKS;n++){
		if(neighbor_sink_list[n]>=0 && available_sinks[n]==-1){
			neighbor_sink_list[n]=-1;
		}
	}

	return;
}

//-----------------------------------------------------------------------------
/* Remove from the candidate neighbors which have no more valid sinks */
void remove_invalid_candidates(int8_t *cand, const kcandidates *cdts){

	uint8_t n;

	for(n=0;n<MAX_NEIGHBORS;n++){
		if(cand[n]>=0){
			if(count_valid(cdts->sinks[n], MAX_SINKS)==0){
				cand[n] = -1;
			}
		}
	}

	return;
}

//-----------------------------------------------------------------------------
/* balance the value of the packet_count field in neighbors */
void balance_packet_count(){

	char flag=1;
	uint8_t fmin=0;
	int8_t n;

	flag=1;
	fmin=0;
	for(n=0; n<MAX_NEIGHBORS; n++) {
		if(!rimeaddr_cmp(&(get_neighbor_list()->addr[n]), &rimeaddr_null)){
			if(get_neighbor_list()->packet_count[n]>0 && (get_neighbor_list()->packet_count[n]<fmin || flag)){
				fmin = get_neighbor_list()->packet_count[n];
			}
		}
	}

	for(n=0; n<MAX_NEIGHBORS; n++) {
		if(!rimeaddr_cmp(&(get_neighbor_list()->addr[n]), &rimeaddr_null)){
			if(get_neighbor_list()->packet_count[n]>=fmin){
				get_neighbor_list()->packet_count[n]-=fmin;
			}
		}
	}

	return;
}

//-----------------------------------------------------------------------------
/* Return the closest sink to the current node or to an already selected sink*/
int8_t get_the_closest2(const int8_t *available_sinks, const int8_t *ignore_list, const int8_t *selected_sinks){

	int8_t s1, s2;
	float min_d1=0, min_d2=0, aux;
	int8_t min1=-1, min2=-1;

	if(count_valid(selected_sinks, MAX_SINKS)>0){
		for(s1=0;s1<MAX_SINKS;s1++){
			if(selected_sinks[s1]>=0){
				for(s2=0;s2<MAX_SINKS;s2++){
					if(available_sinks[s2]>=0 && ignore_list[s2]==-1){
						aux=distance(get_sink_list()->pos[s2], get_sink_list()->pos[s1]);
						if(min_d1>aux || min1==-1){
							min_d1 = aux;
							min1 = s2;
						}
					}
				}
			}
		}
	}

	if(min1>=0){
		return min1;
	}

	for(s1=0;s1<MAX_SINKS;s1++){
		if(available_sinks[s1]>=0 && ignore_list[s1]==-1){
			aux=distance(own_pos, get_sink_list()->pos[s1]);
			if(min_d2>aux || min2==-1){
				min_d2 = aux;
				min2 = s1;
			}
		}
	}

	return min2;

	//return (min1==-1?min2:(min_d1<min_d2?min1:min2));
}

//-----------------------------------------------------------------------------
/* Return the closest sink to the current node or to an already selected sink*/
int8_t get_the_closest(const int8_t *available_sinks, const int8_t *ignore_list, const int8_t *selected_sinks){

	int8_t s1, s2, n;
	float min_d1=0, min_d2=0, aux;
	int8_t min1=-1, min2=-1;

	if(count_valid(selected_sinks, MAX_SINKS)>0){
		for(s1=0;s1<MAX_SINKS;s1++){
			if(selected_sinks[s1]>=0){
				for(s2=0;s2<MAX_SINKS;s2++){
					if(available_sinks[s2]>=0 && ignore_list[s2]==-1){
						aux=distance(get_sink_list()->pos[s2], get_sink_list()->pos[s1]);
						if(min_d1>aux || min1==-1){
							min_d1 = aux;
							min1 = s2;
						}
					}
				}
			}
		}
	}

	for(s1=0;s1<MAX_SINKS;s1++){
		if(available_sinks[s1]>=0 && ignore_list[s1]==-1){
			aux=distance(own_pos, get_sink_list()->pos[s1]);
			if(min_d2>aux || min2==-1){
				min_d2 = aux;
				min2 = s1;
			}
		}
	}

	if(min1>=0){
		if(min_d1>min_d2){
			uint8_t *void_list = get_sink_void_list();
			if(void_list[min2]>=0){
				return min1;
			}
		}
		else{
			return min1;
		}
	}

	return min2;
}

//-----------------------------------------------------------------------------
/* Return the closest sink to the current node or to an already selected sink*/
int8_t get_next_sink(const float *weight){

	int8_t s, min1=-1;
	float min_d1=-1;

	for(s=0;s<MAX_SINKS;s++){
		if(weight[s]>-1 && (min_d1>weight[s] || min1==-1)){
			min_d1 = weight[s];
			min1 = s;
		}
	}

	return min1;
}

//-----------------------------------------------------------------------------
/* Create a tree containing all sinks */
void create_sink_tree(const int8_t *all_sinks, float *weight, int8_t k, int8_t *sinks_tree){

	int8_t available_sinks[MAX_SINKS];
	int8_t s1, s2, n, par;
	float min_d1=0, min_d2=0, aux;
	int8_t min1=-1, min2=-1, count=0;

	memcpy(available_sinks, all_sinks, MAX_SINKS*sizeof(int8_t));

	for(s1=0;s1<MAX_SINKS;s1++){
		if(available_sinks[s1]>=0){
			aux=ENERGY_COST(distance(own_pos, get_sink_list()->pos[s1]));
			if(min_d2>aux || min2==-1){
				min_d2 = aux;
				min2 = s1;
			}
		}
	}

	sinks_tree[min2] = min2;
	weight[min2] = min_d2;
	min2 = -1;
	min_d2 = -1;

	while(count_valid(available_sinks, MAX_SINKS)>0){
		for(s1=0;s1<MAX_SINKS;s1++){
			if(sinks_tree[s1]>=0){
				for(s2=0;s2<MAX_SINKS;s2++){
					if(available_sinks[s2]>=0){
						aux=ENERGY_COST(distance(get_sink_list()->pos[s2], get_sink_list()->pos[s1])) + weight[s1];
						//aux=ENERGY_COST(distance(get_sink_list()->pos[s2], get_sink_list()->pos[s1]));
						if(min_d1>aux || min1==-1){
							min_d1 = aux;
							min1 = s2;
							par = s1;
						}
					}
				}
			}
		}

		for(s1=0;s1<MAX_SINKS;s1++){
			if(available_sinks[s1]>=0){
				aux=ENERGY_COST(distance(own_pos, get_sink_list()->pos[s1]));
				if(min_d2>aux || min2==-1){
					min_d2 = aux;
					min2 = s1;
				}
			}
		}

		if(min_d1<min_d2 || count>k){
			sinks_tree[min1] = par;
			weight[min1] = min_d1;
			//weight[min1] = min_d1 + weight[par];
			available_sinks[min1] = -1;
		}
		else{
			sinks_tree[min2] = min2;
			weight[min2] = min_d2;
			available_sinks[min2] = -1;
		}

		min1 = -1;
		min_d1 = -1;
		min2 = -1;
		min_d2 = -1;
		par = -1;
		count++;
	}

}


//-----------------------------------------------------------------------------
/* Return the closest sink to the candidate forwarder or to an already selected sink*/
/*int8_t avoid_duplication(const kforwarders *fwdrs, int8_t n1, int8_t s1){

	int8_t s2;
	int8_t n2;
	float min_d, aux;
	int8_t min=-1;

	if(fwdrs->count==0){
		return -1;
	}

	min_d = distance(get_sink_list()->pos[s1], get_neighbor_list()->pos[n1]);
	for(n2=0;n2<MAX_NEIGHBORS;n2++){
		if(fwdrs->neighbors[n2]>=0){
			//if(get_neighbor_list()->sink_list[n2][s1]>=0){
				for(s2=0; s2<MAX_SINKS; s2++){
					if(fwdrs->sinks[n2][s2]>=0){
						aux = distance(get_sink_list()->pos[s1], get_sink_list()->pos[s2]);
						if(min_d>=aux){
							min_d = aux;
							min = n2;
						}
					}
				}
			//}
		}
	}

	return min;
}*/

//-----------------------------------------------------------------------------
/* Return the current node update weight */
double updated_weight(const int8_t *allowed_sinks, int8_t node, uint8_t k){

	uint8_t s1, s2;
	int8_t available[MAX_SINKS], selected[MAX_SINKS], index=-1;
	double my_updated_weight, d=0.0, d_min=-1;
	pos_t node_pos;

	memcpy(available, allowed_sinks, MAX_SINKS*sizeof(int8_t));
	memset(selected, -1, MAX_SINKS*sizeof(int8_t));

	if(node==-1){
		node_pos.x=own_pos.x;
		node_pos.y=own_pos.y;
	}
	else{
		node_pos.x=get_neighbor_list()->pos[node].x;
		node_pos.y=get_neighbor_list()->pos[node].y;
	}

	my_updated_weight=0.0;

	for(s1=0; s1<MAX_SINKS; s1++){
		//if(get_sink_list()->sinks[s1]>=0){
		if(available[s1]>=0){
			d = distance(node_pos, get_sink_list()->pos[s1]);
			if(d<d_min || d_min==-1){
				d_min = d;
				index = s1;
			}
		}
	}

	if(index>=0){
		my_updated_weight+=d_min;

		available[index] = -1;
		selected[index] = index;
		k--;
	}

	while(index>=0 && k>0){
		d_min=-1;
		index=-1;
		for(s1=0; s1<MAX_SINKS; s1++){
			if(available[s1]>=0){
				d = distance(node_pos, get_sink_list()->pos[s1]);
				if(d<d_min || d_min==-1){
					d_min = d;
					index = s1;
				}
			}
		}
		if(index>=0){
			for(s2=0; s2<MAX_SINKS; s2++){
				if(selected[s2]>=0){
					for(s1=0; s1<MAX_SINKS; s1++){
						if(available[s1]>=0){
							d = distance(get_sink_list()->pos[s2], get_sink_list()->pos[s1]);
							if(d<d_min || d_min==-1){
								d_min = d;
								index = s1;
							}
						}
					}
				}
			}
		}
		if(index>=0){
			my_updated_weight+=d_min;

			available[index] = -1;
			selected[index] = index;
			k--;
		}
	};

	if(k>0 || my_updated_weight==-1){
		my_updated_weight = 9999999;
	}
	return my_updated_weight;
}

//-----------------------------------------------------------------------------
// Return the current node update weight
/*double updated_weight2(const int8_t *allowed_sinks, int8_t node, uint8_t k, int8_t sender){

	uint8_t s1, s2;
	int8_t available[MAX_SINKS], selected[MAX_SINKS], index=-1, void_list[MAX_SINKS];
	double my_updated_weight, d=0.0, d_min=-1;
	pos_t node_pos;

	memcpy(available, allowed_sinks, MAX_SINKS*sizeof(int8_t));
	memset(selected, -1, MAX_SINKS*sizeof(int8_t));
	memset(void_list, -1, MAX_SINKS*sizeof(int8_t));

	if(node==-1){
		memcpy(void_list, get_sink_void_list(), MAX_SINKS*sizeof(int8_t));
		node_pos.x=own_pos.x;
		node_pos.y=own_pos.y;
	}
	else{
		for(s1=0; s1<MAX_SINKS; s1++){
			if(get_neighbor_list()->sink_list[node][s1]==-1){
				void_list[s1] = s1;
			}
		}
		node_pos.x=get_neighbor_list()->pos[node].x;
		node_pos.y=get_neighbor_list()->pos[node].y;
	}

	int8_t OK;
	for(s1=0;s1<MAX_SINKS;s1++){
		if(get_sink_list()->sinks[s1]>=0 && void_list[s1]==-1){
			OK = 0;
			for(s2=0;s2<MAX_NEIGHBORS;s2++){
				if(get_neighbor_list()->sink_list[s2][s1]>=0 && s2!=sender){
					OK = 1;
				}
			}
			if(OK==0){
				void_list[s1]=s1;
			}
		}
	}

	my_updated_weight=0.0;

	for(s1=0; s1<MAX_SINKS; s1++){
		if(void_list[s1]==-1 && get_sink_list()->sinks[s1]>=0){
			d = distance(node_pos, get_sink_list()->pos[s1]);
			if(d<d_min || d_min==-1){
				d_min = d;
				index = s1;
			}
		}
	}

	if(index>=0){
		my_updated_weight+=d_min;

		available[index] = -1;
		selected[index] = index;
		k--;

		printf("Weigh sinks: %d.0, ", get_sink_list()->addr[index].u8[0]);
	}
	else{
		printf("Fist sink not found!");
	}

	while(index>=0 && k>0){
		d_min=-1;
		index=-1;
		for(s1=0; s1<MAX_SINKS; s1++){
			if(available[s1]>=0){
				d = distance(node_pos, get_sink_list()->pos[s1]);
				if(d<d_min || d_min==-1){
					d_min = d;
					index = s1;
				}
			}
		}
		if(index>=0){
			for(s2=0; s2<MAX_SINKS; s2++){
				if(selected[s2]>=0){
					for(s1=0; s1<MAX_SINKS; s1++){
						if(available[s1]>=0){
							d = distance(get_sink_list()->pos[s2], get_sink_list()->pos[s1]);
							if(d<d_min || d_min==-1){
								d_min = d;
								index = s1;
							}
						}
					}
				}
			}
		}
		if(index>=0){
			my_updated_weight+=d_min;

			available[index] = -1;
			selected[index] = index;
			k--;
			printf("%d.0, ", get_sink_list()->addr[index].u8[0]);
		}
	};

	if(k>0 || my_updated_weight==-1){
		my_updated_weight = 9999999;
		printf(" insuficient sinks!");
	}
	printf("\n");
	return my_updated_weight;
}*/

//-----------------------------------------------------------------------------
/* Next by awake time */
int8_t next_by_awake_time(kforwarders *fwdrs, int8_t *sent, int8_t size){

	int8_t i, node=-1, first=-1;
	rtimer_clock_t now, wait, min=0;

	now = RTIMER_NOW();

	for(i=0;i<MAX_NEIGHBORS;i++){
		if(fwdrs->neighbors[i]>=0 && sent[i]==-1){
			if(first==-1){
				first = i;
			}
			if(get_neighbor_list()->enc[i]!=NULL){
				wait = ((rtimer_clock_t)((get_neighbor_list()->enc[i])->time - now)) % (get_default_peiod());

				//If waiting time is lesser than 1ms, it increases one cycle
				//if(wait<(1*(RTIMER_SECOND/1000))){
				//	wait = wait + get_default_peiod();
				//}

				if(wait<min || node==-1){
					node = i;
					min = wait;
				}
			}
		}
	}

	if(node==-1){
		node = first;
	}

	return node;
}
