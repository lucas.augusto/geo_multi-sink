#include "net/rime.h"
#include "net/mac/geo_kanycast_cxmac.h"
#include "sys/rtimer.h"

#include "kanycast_decision.h"
#include "support.h"
#include "kfunctions.h"
#include "geo-kanycast.h"

#include <stdio.h>  /* For PRINTF()         */
#include <string.h> /* For memcpy           */
//#include <math.h>   /* For acos(), sqrt()   */


#define PI 3.14159265

/* Set DEBUG to 1 to include debug output */
#define DEBUG 0
#if DEBUG
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

/*---------------------------------------------------------------------------*/
void candidate_forwarders(kcandidates *cdts, double min_weight, double node_weight, int8_t sender, const int8_t *allowed_sinks, uint8_t k, reading_pkt_t *reading_pkt){

#ifdef CALCULATION_TIME
	clock_time_t first_mark, original_timestamp;
	first_mark = (clock_time_t)(SYSNOW+get_node_offset());
	original_timestamp = reading_pkt->timestamp;
#endif

	int8_t j,h,sink_counter,k2=0, k3=0;
	int8_t found_sinks[MAX_SINKS], count_from = 0;

//DEBUG: print the list of neighbors ----------
//#if DEBUG == 1
//	print_neighbors();
//#endif
//---------------------------------------------

	//List of Forwarders
	kforwarders sel_fwdrs;
	memset(&sel_fwdrs, 0, sizeof(kforwarders));
	memset(sel_fwdrs.neighbors, -1, sizeof(int8_t)*MAX_NEIGHBORS);
	memset(sel_fwdrs.sinks, -1, sizeof(int8_t)*MAX_NEIGHBORS*MAX_SINKS);
	memset(sel_fwdrs.dest, -1, sizeof(int8_t)*MAX_NEIGHBORS);
	memset(sel_fwdrs.recovery_flag, 0, sizeof(uint8_t)*MAX_NEIGHBORS);

	memset(found_sinks, -1, sizeof(int8_t)*MAX_SINKS);

	//printf("min_weight (%d.%d) > (%d.%d) node_weight -- cdts->recovery_flag==%d\n", (int)min_weight, decimals(min_weight), (int)node_weight, decimals(node_weight), cdts->recovery_flag);

	// check whether the node should leave the recovery mode
	//if(min_weight>node_weight || cdts->recovery_flag==0){
	if(min_weight>node_weight || min_weight==-1){

		reading_pkt->reading_hdr.min_weight = node_weight;

		// go through the list of neighbors
		//printf("List of Neighbors and Sinks!");
		for(j=0; j<MAX_NEIGHBORS; j++) {
			if(j!=sender && !rimeaddr_cmp(&(get_neighbor_list()->addr[j]), &rimeaddr_null) && get_black_list(j, reading_pkt)!=1) {
				//printf("\nNeighbor: %d.0 has sinks: ", get_neighbor_list()->addr[j].u8[0]);
				sink_counter = 0;
				for(h=0;h<MAX_SINKS;h++){
					if(get_neighbor_list()->sink_list[j][h]>=0){
						//printf("  %d.0", get_sink_list()->addr[h].u8[0]);
						// select only the allowed sinks
						if(allowed_sinks[h]>=0){
							//PRINTF("*");
							// check if it is the first time this sink is seen -- we must find at least k sinks
							if(found_sinks[h]==-1){
								found_sinks[h]=h;
								k2++;
								//printf("%d.0, ", get_sink_list()->addr[h].u8[0]);
								//PRINTF("+");
							}
							cdts->sinks[j][h]=h;
							sink_counter++;
						}
					}
				}
				// the neighbor is closer to at least one allowed sink
				if(sink_counter>0){
					//printf("  --- added to candidate list.");
					cdts->neighbors[j] = j;
					cdts->count++;
				}
		    }
		}
		//printf("\n");

		if(k2>=k){
			if(cdts->recovery_flag){
				//printf("Leaving the Recovery Mode: recovery_flag = 0 \n");
				cdts->recovery_flag=0;
			}
			k3=k;
		}

		// notify the neighbors that the node is in a void for the sinks in allowed_sinks
		// call the recovery function if it was not possible to find at least k sinks
		if(k2<k){
			if(k2>0){
				//printf("Pre-selecting forwarder neighbors! \n");
				select_forwarders(&sel_fwdrs, cdts, k2, found_sinks);
				update_available_sinks(allowed_sinks, &sel_fwdrs);
			}
			sink_void_notification(found_sinks, allowed_sinks);
			reading_pkt->reading_hdr.recovery_ref.x = own_pos.x;
			reading_pkt->reading_hdr.recovery_ref.y = own_pos.y;
			cdts->recovery_ref.x = own_pos.x;
			cdts->recovery_ref.y = own_pos.y;
			rimeaddr_copy(&reading_pkt->reading_hdr.recovery_trigger_addr, &rimeaddr_node_addr);
			rimeaddr_copy(&cdts->recovery_trigger_addr, &rimeaddr_node_addr);
			cdts->recovery_flag = 1;
			sender=-1;

			int8_t null_list[MAX_SINKS], available_sinks[MAX_SINKS], s;
			memset(null_list, -1, sizeof(int8_t)*MAX_SINKS);
			memset(available_sinks, -1, sizeof(int8_t)*MAX_SINKS);

			for(s=0;s<MAX_SINKS;s++){
				if(allowed_sinks[s]>=0 && found_sinks[s]==-1){
					available_sinks[s] = s;
				}
				else{
					available_sinks[s] = -1;
				}
			}
			//printf("Recovery mode - not enough sinks! \n");
			s=-1;
			if(reading_pkt->reading_hdr.dest>=0){
				s = allowed_sinks[reading_pkt->reading_hdr.dest];
			}

			if(s==-1){
				s = get_the_closest(available_sinks, null_list, null_list);
				reading_pkt->reading_hdr.dest = s;
			}
			recovery(cdts, k-k2, available_sinks, sender, s);
			k3=k-k2;
		}
	}
	else{
		// continue with the recovery mode
		//printf("Recovery mode - higher weight! \n");
		if(cdts->recovery_flag == 0){
			reading_pkt->reading_hdr.recovery_ref.x = own_pos.x;
			reading_pkt->reading_hdr.recovery_ref.y = own_pos.y;
			cdts->recovery_ref.x = own_pos.x;
			cdts->recovery_ref.y = own_pos.y;
			rimeaddr_copy(&reading_pkt->reading_hdr.recovery_trigger_addr, &rimeaddr_node_addr);
			rimeaddr_copy(&cdts->recovery_trigger_addr, &rimeaddr_node_addr);
			cdts->recovery_flag = 1;
			sender=-1;
		}

		uint8_t dest = reading_pkt->reading_hdr.dest;
		if(rimeaddr_cmp(&rimeaddr_node_addr, &(get_sink_list()->addr[dest]))){
			int8_t null_list[MAX_SINKS];
			memset(null_list, -1, sizeof(int8_t)*MAX_SINKS);
			dest = get_the_closest(allowed_sinks, null_list, null_list);
		}

		recovery(cdts, k, allowed_sinks, sender, dest);
		k3=k;
	}

	if(cdts->count==0){
		PRINTF("ERROR: No candidates found! \n");
		return;
	}

	//printf("Selecting forwarder neighbors! \n");
	select_forwarders(&sel_fwdrs, cdts, k3, allowed_sinks);

	if(sel_fwdrs.count==0){
		PRINTF("ERROR: No forwarders found! \n");
		return;
	}


#ifdef CALCULATION_TIME
	reading_pkt->timestamp = (clock_time_t)(original_timestamp + (SYSNOW + get_node_offset() - first_mark));
#endif

	//send_packet(&sel_fwdrs, cdts, reading_pkt, count_from, node_weight);
	send_packet(&sel_fwdrs, reading_pkt, count_from, node_weight);

	return;
}

/*---------------------------------------------------------------------------*/

void recovery(kcandidates *cdts, uint8_t k, const int8_t *allowed_sinks, int8_t sender, int8_t dest){

	uint8_t j, h, flag, counter=0;
	int8_t f, n, v, v_min, v_max;
	double ang=0, ang_min, ang_max, a, b, c, d, m;
	struct gabriel_graph gg;

	// calculate the gabriel graph of the sub-graph formed by the node and its neighbors
	memset(&gg, 0, sizeof(struct gabriel_graph));
	memset(gg.vertice, -1, sizeof(int8_t)*MAX_NEIGHBORS);
	gabriel_graph_builder(&gg);

	v=-2;
	flag=1;
	v_max=-2;
	v_min=-2;
	ang=0;
	ang_min=361;
	ang_max=-1;
	if(sender==-1){
		b = distance(get_sink_list()->pos[dest],own_pos);
	}
	else{
		b = distance(get_neighbor_list()->pos[sender],own_pos);
	}
	do{
		for(n=0; n<MAX_NEIGHBORS; n++){
			// neighbor cannot be the sender nor the already selected node
			if(n!=sender && !rimeaddr_cmp(&(get_neighbor_list()->addr[n]), &rimeaddr_null)){
				// check if the edge exist in the gabriel graph
				if(gg.edge[n]==1){
					if(v==-2){
						if(sender==-1){
							a = (own_pos.x - get_sink_list()->pos[dest].x)*(own_pos.x - get_neighbor_list()->pos[n].x) + (own_pos.y - get_sink_list()->pos[dest].y)*(own_pos.y - get_neighbor_list()->pos[n].y);
							c = (get_neighbor_list()->pos[n].x - own_pos.x)*(get_sink_list()->pos[dest].y - own_pos.y) - (get_neighbor_list()->pos[n].y - own_pos.y)*(get_sink_list()->pos[dest].x - own_pos.x);
							//printf("Testing - n: %d.0, sink: %d.0, self: %d.0 | ", get_neighbor_list()->addr[n].u8[0], get_sink_list()->addr[dest].u8[0], rimeaddr_node_addr.u8[0]);
						}
						else{
							a = (own_pos.x - get_neighbor_list()->pos[sender].x)*(own_pos.x - get_neighbor_list()->pos[n].x) + (own_pos.y - get_neighbor_list()->pos[sender].y)*(own_pos.y - get_neighbor_list()->pos[n].y);
							c = (get_neighbor_list()->pos[n].x - own_pos.x)*(get_neighbor_list()->pos[sender].y - own_pos.y) - (get_neighbor_list()->pos[n].y - own_pos.y)*(get_neighbor_list()->pos[sender].x - own_pos.x);
							//printf("Testing - n: %d.0, sender: %d.0, self: %d.0 | ", get_neighbor_list()->addr[n].u8[0], get_neighbor_list()->addr[sender].u8[0], rimeaddr_node_addr.u8[0]);
						}
					}
					else{
						a = (own_pos.x - get_neighbor_list()->pos[v].x)*(own_pos.x - get_neighbor_list()->pos[n].x) + (own_pos.y - get_neighbor_list()->pos[v].y)*(own_pos.y - get_neighbor_list()->pos[n].y);
						c = (get_neighbor_list()->pos[n].x - own_pos.x)*(get_neighbor_list()->pos[v].y - own_pos.y) - (get_neighbor_list()->pos[n].y - own_pos.y)*(get_neighbor_list()->pos[v].x - own_pos.x);
						//printf("Testing - n: %d.0, v: %d.0, self: %d.0 | ", get_neighbor_list()->addr[n].u8[0], get_neighbor_list()->addr[v].u8[0], rimeaddr_node_addr.u8[0]);
					}
					d = distance(get_neighbor_list()->pos[n],own_pos);
					m = a/(b*d);
					ang = acos(m)*(180.0/PI);
					//printf("ang_min = %d.%d | ang_max = %d.%d | ang = %d.%d | c = %d.%d | ", (int)ang_min, decimals(ang_min), (int)ang_max, decimals(ang_max), (int)ang, decimals(ang), (int)c, decimals(c));
					if(ang<ang_min && (int)c>=0){
						ang_min = ang;
						v_min = n;
						//printf("MIN: v_min = %d.0 | ", get_neighbor_list()->addr[v_min].u8[0]);
					}
					if(ang>ang_max && (int)c<0){
						ang_max = ang;
						v_max = n;
						//printf("MAX: v_max = %d.0 | ", get_neighbor_list()->addr[v_max].u8[0]);
					}
					//printf("\n");
				}
			}
		}

		// check whether we need to take the opposite side or go back
		if(v_min==-2){
			if(v_max>=0){
				//printf("opposite side \n");
				v = v_max;
			}
			else{
				//printf("go back \n");
				v = sender;
			}
			break;
		}

		flag = change_face(&cdts->recovery_ref, &own_pos, &(get_sink_list()->pos[dest]), &(get_neighbor_list()->pos[v_min]));
		//printf("flag: %d | v_min = %d.0 | v = %d.0\n", flag, get_neighbor_list()->addr[v_min].u8[0], get_neighbor_list()->addr[v].u8[0]);
		if(v==v_min){
			flag=0;
		}
		v = v_min;

	}while(flag);

	// register the neighbor as the forwarder for the sink s
	if(v>=0){
		//printf("Neighbor: %d.0 selected!\n", get_neighbor_list()->addr[v].u8[0]);
		if(cdts->neighbors[v]==-1){
			cdts->neighbors[v]=v;
			cdts->count++;
		}
		memcpy(cdts->sinks[v], allowed_sinks, sizeof(int8_t)*MAX_SINKS);

	}

	return;
}

/*---------------------------------------------------------------------------*/

uint8_t change_face(const pos_t *src, const pos_t *curr, const pos_t *dest, const pos_t *next){

	uint8_t flag=1;
	float a, b, c, d, dist;
	pos_t cf;

	a = (curr->x - src->x)*(dest->y - src->y) - (src->y - curr->y)*(src->x - dest->x);

	if(a==0){
		return flag;
	}

	b = (next->x - src->x)*(dest->y - src->y) - (src->y - next->y)*(src->x - dest->x);

	if((a<0 && b<0) || (a>0 && b>0)){
		flag = 0;
	}

	if(flag){
		flag = 0;

		a = (dest->y - src->y)/(dest->x - src->x);
		b = (next->y - curr->y)/(next->x - curr->x);

		c = src->y - a*src->x;
		d = curr->y - b*curr->x;

		cf.x = (d-c)/(a-b);
		cf.y = (a*cf.x)+c;

		dist = distance(cf, *src) + distance(cf, *dest);

		if((int)(distance(*src,*dest)) == (int)(fastPrecisePow(10.0,6.0)*dist)){
			flag = 1;
		}
	}

	return flag;
}

/*---------------------------------------------------------------------------*/

void gabriel_graph_builder(struct gabriel_graph *gg){

	float d1, d2, d3;
	uint8_t n2, n3;

	// the process to calculate the gabriel graph was adapted from here: http://onlinelibrary.wiley.com/doi/10.1111/j.1538-4632.1980.tb00031.x/pdf
	//printf("GG: ");
	for(n2=0; n2<MAX_NEIGHBORS; n2++){
		if(!rimeaddr_cmp(&(get_neighbor_list()->addr[n2]), &rimeaddr_null)){
			gg->vertice[n2]=n2;
			gg->edge[n2] = 1;
			for(n3=0; n3<MAX_NEIGHBORS; n3++){
				if(n3!=n2 && !rimeaddr_cmp(&(get_neighbor_list()->addr[n3]), &rimeaddr_null)){
					d1 = distance(own_pos, get_neighbor_list()->pos[n2]);
					d2 = distance(own_pos, get_neighbor_list()->pos[n3]);
					d3 = distance(get_neighbor_list()->pos[n2], get_neighbor_list()->pos[n3]);
					if(d1*d1 > (d2*d2 + d3*d3)){
						gg->edge[n2] = 0;
					}
				}
			}
			/*if(get_neighbor_list()->addr[n2].u8[0]>0){
			if(gg->edge[n2] == 0){
				printf("No: %d.0 | ", get_neighbor_list()->addr[n2].u8[0]);
			}
			else{
				printf("Yes: %d.0 | ", get_neighbor_list()->addr[n2].u8[0]);
			}
		}*/
		}
	}
	//printf("\n");
	return;
}

/*---------------------------------------------------------------------------*/

void select_forwarders(kforwarders *fwdrs, const kcandidates *cdts, uint8_t k, int8_t *allowed_sinks){

	int8_t n, s, i, j=0, h=0, k2=0, count_sink=0;
	int8_t cand1[MAX_NEIGHBORS];

/*	printf("allowed_sinks: ");
	for(s=0;s<MAX_SINKS;s++){
		if(allowed_sinks[s]>=0){
			printf("%d.0 | ", get_sink_list()->addr[s].u8[0]);
		}
	}
	printf("\n"); */

	float weight_sink[MAX_SINKS];
	int8_t sinks_tree[MAX_SINKS];
	memset(cand1, -1, sizeof(int8_t)*MAX_NEIGHBORS);
	memset(weight_sink, -1, sizeof(float)*MAX_SINKS);
	memset(sinks_tree, -1, MAX_SINKS*sizeof(int8_t));
	create_sink_tree(allowed_sinks, weight_sink, k, sinks_tree);
	// check other candidates in case it was not possible to get all k sinks from the neighbor sinks
	// select the forwarders among the other candidates
	PRINTF("Best Other Candidates\n");
	memset(cand1, -1, sizeof(int8_t)*MAX_NEIGHBORS);
	memcpy(cand1, cdts->neighbors, MAX_NEIGHBORS*sizeof(int8_t));

	//k2 = best_other_candidates(fwdrs, cdts, k-k2, cand1, weight_sink, allowed_sinks);
	best_candidates(fwdrs, cdts, k, weight_sink, allowed_sinks);
	int8_t local_selected[MAX_SINKS];
	int8_t s2, min=-1, par=-1;
	float aux, min_d=-1;

	memset(local_selected, -1, sizeof(int8_t)*MAX_SINKS);

	if(count_valid(allowed_sinks, MAX_SINKS)>0 && fwdrs->count>=1){
		if(fwdrs->count==1){
			for(j=0;j<MAX_NEIGHBORS;j++){
				if(fwdrs->neighbors[j]>=0){
					for(s=0;s<MAX_SINKS;s++){
						if(allowed_sinks[s]>=0){
							fwdrs->sinks[j][s] = s;
						}
					}
					break;
				}
			}
		}
		else{
			for(j=0;j<MAX_NEIGHBORS;j++){
				if(fwdrs->neighbors[j]>=0){
					for(s=0;s<MAX_SINKS;s++){
						if(fwdrs->sinks[j][s]>=0){
							local_selected[s]=j;
						}
					}
				}
			}

			for(s=0;s<MAX_SINKS;s++){
				if(allowed_sinks[s]>=0){
					fwdrs->sinks[local_selected[sinks_tree[s]]][s]=s;
					local_selected[s]=local_selected[sinks_tree[s]];
					allowed_sinks[min]=-1;
					//printf("Sink %d.0 is <<connected>> to sink %d.0 and will go through node %d.0\n", get_sink_list()->addr[s].u8[0], get_sink_list()->addr[sinks_tree[s]].u8[0], get_neighbor_list()->addr[local_selected[s]].u8[0]);
				}
			}
		}
	}

	return;
}

/*---------------------------------------------------------------------------*/

uint8_t best_other_candidates(kforwarders *fwdrs, const kcandidates *cdts, uint8_t k, int8_t *cand, float *weight, int8_t *available_sinks){

	int8_t k2=0, k3=k, closest_sink, pk;
	int8_t v, index;
	uint8_t n, s;
	int8_t cand_pair[MAX_NEIGHBORS], ignore_list[MAX_SINKS], selected_sinks[MAX_SINKS];

	pk = (int8_t)((k/count_valid(available_sinks, MAX_SINKS))*100);

	memset(ignore_list, -1, sizeof(int8_t)*MAX_SINKS);
	memset(selected_sinks, -1, sizeof(int8_t)*MAX_SINKS);

	do{
		remove_invalid_candidates(cand, cdts);
		closest_sink=get_next_sink(weight);
		//printf("Searching forwarder for sink: %d.0  \n", get_sink_list()->addr[closest_sink].u8[0]);
		v=sink_is_neighbor(&(get_sink_list()->addr[closest_sink]));
		if(v==-1){
			memset(cand_pair, -1, sizeof(int8_t)*MAX_NEIGHBORS);
			for(n=0;n<MAX_NEIGHBORS;n++){
				if(closest_sink>=0 && cand[n]>=0){
					if(cdts->sinks[n][closest_sink]>=0){
						cand_pair[n] = n;
						//printf("\nChecking neighbor: %d.0 -- ", get_neighbor_list()->addr[n].u8[0]);
						/*if(v==-1){
							v=n;
						}
						else{
							memset(cand_pair, -1, sizeof(int8_t)*MAX_NEIGHBORS);
							cand_pair[v] = v;
							cand_pair[n] = n;
							v=metric_calc(fwdrs, cand_pair, closest_sink, k3, pk);
						}*/
					}
				}
			}
			v=metric_calc(fwdrs, cand_pair, closest_sink, k3, pk);
		}
		if(v!=-1){
			index = fwdrs->neighbors[v];
			if(index==-1){
				fwdrs->neighbors[v] = v;
				fwdrs->sinks[v][closest_sink] = closest_sink;
				fwdrs->k[v]=1;
				fwdrs->recovery_flag[v] = cdts->recovery_flag;
				fwdrs->count++;
				if(fwdrs->dest[v]==-1){
					fwdrs->dest[v] = closest_sink;
				}
				k2++;
				k3--;
				//printf("Node %d.0 selected (new)\n", get_neighbor_list()->addr[v].u8[0]);
			}else{
				fwdrs->k[index]++;
				fwdrs->recovery_flag[index] = cdts->recovery_flag;
				k2++;
				k3--;
				fwdrs->sinks[index][closest_sink] = closest_sink;
				//printf("Node %d.0 selected (old)\n", get_neighbor_list()->addr[index].u8[0]);
			}
			available_sinks[closest_sink] = -1;
		}
		weight[closest_sink] = -1;
	}while(k3);

	return k2;
}

/*---------------------------------------------------------------------------*/

uint8_t metric_calc(kforwarders *fwdrs, int8_t *cand, int8_t sink, int8_t k, int8_t pk){

	uint8_t n, v=0;
	int8_t selected, min_pkt, max_pkt;
	float x, z, h, y, w[MAX_NEIGHBORS];
	float dist[MAX_NEIGHBORS], min_dist, max_dist;
	double enrg[MAX_NEIGHBORS], min_enrg, max_enrg;
	double min_consumed, max_consumed;

	// Initialization ---------------------------------------------------------
	selected=-1;
	memset(dist, -1, MAX_NEIGHBORS*sizeof(float));
	max_dist=-1;
	min_dist=-1;
	max_enrg=-1;
	min_enrg=-1;
	min_pkt=-1;
	max_pkt=-1;
	max_consumed=-1;
	min_consumed=-1;
	//-----------------------------------------------------------------------


	// Distance + Packet Count -------------------------------------------------
	for(n=0;n<MAX_NEIGHBORS;n++){
		if(cand[n]>=0){
			// Distance --------------------------------------------------------
			dist[n]=distance(get_neighbor_list()->pos[n],get_sink_list()->pos[sink]);
			enrg[n] = ENERGY_COST_TX(own_pos, get_neighbor_list()->pos[n],reading_pkt_t);

			if(min_dist>dist[n] || min_dist==-1){
				min_dist = dist[n];
			}
			if(max_dist<dist[n] || max_dist==-1){
				max_dist = dist[n];
			}

			if(min_enrg>enrg[n] || min_enrg==-1){
				min_enrg = enrg[n];
			}
			if(max_enrg<enrg[n] || max_enrg==-1){
				max_enrg = enrg[n];
			}

			// Packet Count ----------------------------------------------------
			if(min_pkt>get_neighbor_list()->packet_count[n] || min_pkt==-1){
				min_pkt = get_neighbor_list()->packet_count[n];
			}
			if(max_pkt<get_neighbor_list()->packet_count[n] || max_pkt==-1){
				max_pkt = get_neighbor_list()->packet_count[n];
			}

			// Consumed Energy ----------------------------------------------------
			if(min_consumed>get_neighbor_list()->consumed_energy[n] || min_consumed==-1){
				min_consumed = get_neighbor_list()->consumed_energy[n];
			}
			if(max_consumed<get_neighbor_list()->consumed_energy[n] || max_consumed==-1){
				max_consumed = get_neighbor_list()->consumed_energy[n];
			}
		}
	}

	for(n=0;n<MAX_NEIGHBORS;n++){
		if(cand[n]>=0){
			if(selected==-1){
				v = n;
			}
			//printf("[ Comparing v: %d.0 with i: %d.0 | ", get_neighbor_list()->addr[v].u8[0], get_neighbor_list()->addr[n].u8[0]);

			// PACKET COUNT
			x = ((max_pkt-min_pkt)==0.0?0.0:(get_neighbor_list()->packet_count[n]-min_pkt)/(max_pkt-min_pkt));

			// DIST to sinks
			h = ((max_dist-min_dist)==0.0?0.0:(dist[n] - min_dist)/(max_dist - min_dist));

			// ENERGY COST
			z = ((max_enrg-min_enrg)==0.0?0.0:(enrg[n] - min_enrg)/(max_enrg - min_enrg));

			// ENERGY CONSUMPTION
			y = ((max_consumed-min_consumed)==0.0?0.0:(get_neighbor_list()->consumed_energy[n] - min_consumed)/(max_consumed - min_consumed));

			//AGGREGATED METRIC
			w[n] = ((PACKET_COUNT_METRIC_WEIGHT(pk)*x) + (DIST_SINK_METRIC_WEIGHT(pk)*h) + (ENERGY_METRIC_WEIGHT(pk)*z) + (CONSUMED_ENERGY_WEIGHT(pk)*y) + (IS_FWDR_METRIC_WEIGHT(pk)*(fwdrs->neighbors[n]>=0?0:1)))/MAX_METRICS(pk);

			if(w[v]>=w[n]){
				v = n;
				selected=v;
				//printf("%d.0 selected.", get_neighbor_list()->addr[v].u8[0]);
			}
			//printf("] ");
		}
	}

	return selected;
}

/*---------------------------------------------------------------------------*/

void best_candidates(kforwarders *fwdrs, const kcandidates *cdts, uint8_t k, float *weight, int8_t *available_sinks){

	int8_t n, s, i, pk, w_count, k2, sinks[MAX_SINKS], found;

	struct calculated_weight {
		float node[MAX_NEIGHBORS];
		float avg;
	} w[MAX_SINKS];

	struct p_list {
			int8_t node[MAX_NEIGHBORS];
			int8_t sink[MAX_SINKS];
			int8_t count;
			int8_t closer_sink;
	} p[k];

	float z, h, y, w_sum, tmp_weight[MAX_SINKS];
	float dist[MAX_NEIGHBORS][MAX_SINKS], min_dist[MAX_SINKS], max_dist[MAX_SINKS];
	double enrg[MAX_NEIGHBORS], min_enrg, max_enrg;
	double min_consumed, max_consumed;

	// Initialization ---------------------------------------------------------
	memset(dist, -1, MAX_SINKS*MAX_NEIGHBORS*sizeof(float));
	memset(sinks, -1, MAX_SINKS*sizeof(int8_t));
	max_enrg=-1;
	min_enrg=-1;
	max_consumed=-1;
	min_consumed=-1;
	memset(w, -1, sizeof(struct calculated_weight)*MAX_SINKS);
	memset(p, -1, sizeof(struct p_list)*k);
	//pk = (int8_t)((k/count_valid(available_sinks, MAX_SINKS))*100);
	pk = (int8_t)((k/MAX_K)*100);
	//-----------------------------------------------------------------------


	// Distance ---------------------------------------------------------------
	for(s=0;s<MAX_SINKS;s++){
		max_dist[s]=-1;
		min_dist[s]=-1;
		if(available_sinks[s]>=0){
			for(n=0;n<MAX_NEIGHBORS;n++){
				if(cdts->neighbors[n]>=0){
					dist[n][s]=distance(get_neighbor_list()->pos[n],get_sink_list()->pos[s]);

					if(min_dist[s]>dist[n][s] || min_dist[s]==-1){
						min_dist[s] = dist[n][s];
					}
					if(max_dist[s]<dist[n][s] || max_dist[s]==-1){
						max_dist[s] = dist[n][s];
					}
				}
			}
		}
	}

	// Energy Cost + Consumed Energy -----------------------------
	for(n=0;n<MAX_NEIGHBORS;n++){
			if(cdts->neighbors[n]>=0){
			// Energy Cost --------------------------------------------------------
			enrg[n] = ENERGY_COST_TX(own_pos, get_neighbor_list()->pos[n],reading_pkt_t);

			if(min_enrg>enrg[n] || min_enrg==-1){
				min_enrg = enrg[n];
			}
			if(max_enrg<enrg[n] || max_enrg==-1){
				max_enrg = enrg[n];
			}

			// Consumed Energy ----------------------------------------------------
			if(min_consumed>get_neighbor_list()->consumed_energy[n] || min_consumed==-1){
				min_consumed = get_neighbor_list()->consumed_energy[n];
			}
			if(max_consumed<get_neighbor_list()->consumed_energy[n] || max_consumed==-1){
				max_consumed = get_neighbor_list()->consumed_energy[n];
			}
		}
	}

	//Calculate the metric
	memcpy(tmp_weight, weight, sizeof(float)*MAX_SINKS);
	k2=k;
	i=0;
	do{
		s=get_next_sink(tmp_weight);
		if(available_sinks[s]>=0){
			w_sum=0;
			w_count=0;
			//printf("\nSink: %d.0 | ", get_sink_list()->addr[s].u8[0]);
			for(n=0;n<MAX_NEIGHBORS;n++){
				if(cdts->neighbors[n]>=0 && cdts->sinks[n][s]>=0){
					// DIST to sinks
					h = ((max_dist[s]-min_dist[s])==0.0?0.0:(dist[n][s] - min_dist[s])/(max_dist[s] - min_dist[s]));

					// ENERGY COST
					z = ((max_enrg-min_enrg)==0.0?0.0:(enrg[n] - min_enrg)/(max_enrg - min_enrg));

					// ENERGY CONSUMPTION
					y = ((max_consumed-min_consumed)==0.0?0.0:(get_neighbor_list()->consumed_energy[n] - min_consumed)/(max_consumed - min_consumed));

					//AGGREGATED METRIC
					if(s==neighbor_is_sink(&(get_neighbor_list()->addr[n]))){
						w[s].node[n] = 0;
					}
					else{
						w[s].node[n] = ((DIST_SINK_METRIC_WEIGHT(pk)*h) + (ENERGY_METRIC_WEIGHT(pk)*z) + (CONSUMED_ENERGY_WEIGHT(pk)*y))/MAX_METRICS(pk);
					}
					w_sum+=w[s].node[n];
					w_count++;
					//printf(" Weight (%d.0): %d.%d -- ", get_neighbor_list()->addr[n].u8[0], (int)w[s].node[n], decimals(w[s].node[n]));
					//printf(" h: %d.%d ; max_dist: %d.%d ; min_dist: %d.%d ; dist: %d.%d | ", (int)h, decimals(h), (int)max_dist[s], decimals(max_dist[s]), (int)min_dist[s], decimals(min_dist[s]), (int)dist[n][s], decimals(dist[n][s]));
				}
			}
			w[s].avg = (w_count>0?(w_sum/w_count):-2);
			//printf("Average %d.%d | ", (int)w[s].avg, decimals(w[s].avg));
			for(n=0;n<MAX_NEIGHBORS;n++){
				if(cdts->neighbors[n]>=0 && cdts->sinks[n][s]>=0 && w[s].node[n]>(w[s].avg*DEVIATION_FACTOR(pk))){
					w[s].node[n] = -1;
					//printf("Excluded (%d.0) | ", get_neighbor_list()->addr[n].u8[0]);
				}
			}
		}
		k2--;
		tmp_weight[s]=-1;
		sinks[i]=s;
		i++;
	}while(k2>0);

	//printf("\nForwarders selection:\n");
	//printf("Checking for sink (%d.0) -- ", get_sink_list()->addr[sinks[0]].u8[0]);

	//Initialize p[0] with the closest sink and its forwarder candidates
	p[0].sink[sinks[0]]=sinks[0];
	p[0].closer_sink=sinks[0];
	for(n=0;n<MAX_NEIGHBORS;n++){
		if(cdts->neighbors[n]>=0 && cdts->sinks[n][sinks[0]]>=0 && w[sinks[0]].node[n]>=0){
			p[0].node[n]=n;
			p[0].count++;
			//printf("(%d.0)", get_neighbor_list()->addr[n].u8[0]);
		}
	}

	//Go through the list of sinks and neighbors to check for intersections
	s=1;
	i=1;
	int8_t temp_nodes[MAX_NEIGHBORS], max_nodes[MAX_NEIGHBORS], max_intersec=0, index;
	do{
		//printf("\nChecking for sink (%d.0) -- ", get_sink_list()->addr[sinks[s]].u8[0]);
		max_intersec=0;
		memset(max_nodes, -1, MAX_NEIGHBORS*sizeof(int8_t));
		if(sinks[s]>=0){
			for(k2=0;k2<i;k2++){
				found=0;
				memset(temp_nodes, -1, MAX_NEIGHBORS*sizeof(int8_t));
				for(n=0;n<MAX_NEIGHBORS;n++){
					if(cdts->neighbors[n]>=0 && cdts->sinks[n][sinks[s]]>=0 && w[sinks[s]].node[n]>=0){
						//printf("[%d.0]", get_neighbor_list()->addr[n].u8[0]);
						if(p[k2].node[n]>=0){
							//printf("*");
							temp_nodes[n]=n;
							found++;
						}
					}
				}
				if(found>max_intersec){
					index=k2;
					max_intersec=found;
					memcpy(max_nodes, temp_nodes, sizeof(int8_t)*MAX_NEIGHBORS);
				}
			}
			if(max_intersec==0){
				p[i].sink[sinks[s]]=sinks[s];
				p[i].closer_sink=sinks[s];
				//printf(" | New instance! (i=%d) | ", i);
				for(n=0;n<MAX_NEIGHBORS;n++){
					if(cdts->neighbors[n]>=0 && cdts->sinks[n][sinks[s]]>=0 && w[sinks[s]].node[n]>=0){
						p[i].node[n]=n;
						p[i].count++;
						//printf("(%d.0)", get_neighbor_list()->addr[n].u8[0]);
					}
				}
				i++;
			}
			else{
				p[index].sink[sinks[s]]=sinks[s];
				memcpy(p[index].node, max_nodes, sizeof(int8_t)*MAX_NEIGHBORS);
				p[index].count=max_intersec;
				/*printf(" | Added to existent! (index=%d) | ", index);
				for(n=0;n<MAX_NEIGHBORS;n++){
					if(p[index].node[n]>=0){
						printf("(%d.0)", get_neighbor_list()->addr[n].u8[0]);
					}
				}*/
			}
			available_sinks[sinks[s]]=-1;
		}
		s++;
	}while(s<k);

	//printf("\nCreating the forwarders list: ");

	//Create the forwarders list with the list of sink/neighbor intersection
	float temp_n[MAX_NEIGHBORS], stdev_n, metric_min, stdev_min;

	for(k2=0;k2<i;k2++){
		//printf("\nCheking k2=%d -- ", k2);
		index=-1;
		metric_min=-1.0;
		stdev_min=-1.0;
		if(count_valid(p[k2].sink, MAX_SINKS)>1 && count_valid(p[k2].node, MAX_NEIGHBORS)>1){
			//if the flow entered here it means that we have more than one neighbor to more than one sink;
			//we need to check which neighbor is the most suitable to the most of the sinks
			for(n=0;n<MAX_NEIGHBORS;n++){
				//we go through all neighbors and all sinks to calculate the average metric of that neighbor in relation to all sinks
				if(p[k2].node[n]>=0){
					w_sum=0;
					w_count=0;
					//printf("1-Node (%d.0) -> ", get_neighbor_list()->addr[n].u8[0]);
					for(s=0;s<MAX_SINKS;s++){
						if(p[k2].sink[s]>=0){
							//printf("Sink (%d.0): %d.%d | ", get_sink_list()->addr[s].u8[0], (int)w[s].node[n], decimals(w[s].node[n]));
							w_sum+=w[s].node[n];
							w_count++;
						}
					}
				    //average
					temp_n[n]=w_sum/w_count;
					//printf(" | Average: %d.%d", (int)temp_n[n], decimals(temp_n[n]));
					if(metric_min>temp_n[n] || metric_min==-1.0){
						index = n;
						metric_min = temp_n[n];
					}

					//If two neighbors have the same average distance, we select the one with the smaller variation
					if(metric_min == temp_n[n]){
						w_sum=0;
						//We go through entry to calculate the standard deviation
						for(s=0;s<MAX_SINKS;s++){
							if(p[k2].sink[s]>=0){
								w_sum+=fastPrecisePow(w[s].node[n]-temp_n[n],2);
							}
						}
						stdev_n = sqrt(w_sum/w_count);
						//printf(" | stdev_n: %d.%d", (int)stdev_n, decimals(stdev_n));
						//We select the neighbor with the smallest variation of the metric for each sink
						if(stdev_min>stdev_n || stdev_min==-1.0){
							index = n;
							stdev_min = stdev_n;
						}
					}
				}
			}
		}
		else{
			if(count_valid(p[k2].node, MAX_NEIGHBORS)>1){
				//If the flow entered here it means that there is only one sink which is common to more than one neighbor.
				//We need to know which sink is that
				for(s=0;s<MAX_SINKS;s++){
					if(p[k2].sink[s]>=0){
						break;
					}
				}
				//We go through the list of neighbor to select the one with the min value for the combined metric
				for(n=0;n<MAX_NEIGHBORS;n++){
					if(p[k2].node[n]>=0){
						//printf("2-Node (%d.0) -> ", get_neighbor_list()->addr[n].u8[0]);
						//printf("Average: %d.%d | ", (int)w[s].node[n], decimals(w[s].node[n]));
						if(metric_min>w[s].node[n] || metric_min==-1.0){
							index = n;
							metric_min = w[s].node[n];
						}
					}
				}
			}
			else{
				//if the flow entered here, it means that there is only one neighbor to one or more sinks.
				//we select this neighbor directly, but we need to search for it
				for(n=0;n<MAX_NEIGHBORS;n++){
					if(p[k2].node[n]>=0){
						index = n;
						//printf("3-Node (%d.0) is the only option!", get_neighbor_list()->addr[n].u8[0]);
						break;
					}
				}
			}
		}
		n=index;
		//printf(" Selected: %d.0 ", get_neighbor_list()->addr[n].u8[0]);
		if(n>=0){
			fwdrs->neighbors[n] = n;
			memcpy(fwdrs->sinks[n], p[k2].sink, sizeof(int8_t)*MAX_SINKS);
			fwdrs->k[n]=count_valid(p[k2].sink, MAX_SINKS);
			fwdrs->recovery_flag[n] = cdts->recovery_flag;
			fwdrs->count++;
			fwdrs->dest[n]=p[k2].closer_sink;
		}
	}

}

/*---------------------------------------------------------------------------*/
