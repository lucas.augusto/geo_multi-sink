#ifndef MY_METRICS_H
#define MY_METRICS_H

#include "geo-kanycast.h"

typedef struct {
	/* Total Received Packets (Sinks)*/
	unsigned long long received_pkt;

	/* Average Latency of the received packets */
	clock_time_t latency;
} sink_metrics;

typedef struct {
	/* Node's TX energy consumption - Data */
	double energy_cost_tx;

	/* Node's RX energy consumption - Data*/
	double energy_cost_rx;

	/* Node's TX energy consumption - Control */
	double ctrl_energy_cost_tx;

	/* Node's RX energy consumption - Control*/
	double ctrl_energy_cost_rx;

	/* Total Forwarded Packets */
	unsigned long long forwarded_pkt;

	/* Total Generated Packets */
	unsigned long long generated_pkt;
} node_metrics;

node_metrics * sensor_metrics();

void my_metrics_init();

void register_generated_packet();

void register_forwarded_packet();

void register_energy_cost(double energy_cost, uint8_t direction, uint8_t type);

void register_received_packet();

void register_latency(clock_time_t latency);

void print_my_metrics(uint8_t type);


#endif
