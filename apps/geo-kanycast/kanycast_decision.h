#ifndef KANYCAST_DECISION_H
#define KANYCAST_DECISION_H

#include "geo-kanycast.h"
#include "support.h"

struct gabriel_graph {
	int8_t vertice[MAX_NEIGHBORS];
	uint8_t edge[MAX_NEIGHBORS];
};

void candidate_forwarders(kcandidates *cdts, double min_weight, double node_weight, int8_t sender, const int8_t *allowed_sinks, uint8_t k, reading_pkt_t *reading_pkt);

void recovery(kcandidates *cdts, uint8_t k, const int8_t *allowed_sinks, int8_t sender, int8_t dest);

uint8_t change_face(const pos_t *src, const pos_t *curr, const pos_t *dest, const pos_t *next);

void gabriel_graph_builder(struct gabriel_graph *gg);

void select_forwarders(kforwarders *fwdrs, const kcandidates *cdts, uint8_t k, int8_t *allowed_sinks);

uint8_t best_other_candidates(kforwarders *fwdrs, const kcandidates *cdts, uint8_t k, int8_t *cand, float *weight, int8_t *available_sinks);

uint8_t metric_calc(kforwarders *fwdrs, int8_t *cand, int8_t sink, int8_t k, int8_t pk);

void best_candidates(kforwarders *fwdrs, const kcandidates *cdts, uint8_t k, float *weight, int8_t *available_sinks);

#endif
