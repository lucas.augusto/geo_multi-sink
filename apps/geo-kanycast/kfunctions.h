#ifndef KFUNCTIONS_H
#define KFUNCTIONS_H

#include "geo-kanycast.h"

int8_t neighbor_is_sink(const rimeaddr_t *neighbor_addr);

int8_t sink_is_neighbor(const rimeaddr_t *sink_addr);

void closest_sinks(int8_t *neighbor_sink_list, pos_t neighbor, pos_t node);

uint8_t count_valid(const int8_t *node_list, uint8_t list_size);

int8_t get_index_by_addr(const rimeaddr_t *item_addr, const rimeaddr_t *list_addr, uint8_t list_size);

struct encounter * get_encounter_pointer_by_addr(const rimeaddr_t *item_addr);

void update_available_sinks(int8_t *available_sinks, const kforwarders *fwdrs);

void remove_unavailable_sinks(int8_t *neighbor_sink_list, const int8_t *available_sinks);

void remove_invalid_candidates(int8_t *cand, const kcandidates *cdts);

void balance_packet_count();

int8_t get_the_closest(const int8_t *available_sinks, const int8_t *ignore_list, const int8_t *selected_sinks);

int8_t get_next_sink(const float *weight);

void create_sink_tree(const int8_t *all_sinks, float *weight, int8_t k, int8_t *sinks_tree);

int8_t avoid_duplication(const kforwarders *fwdrs, int8_t n1, int8_t s1);

double updated_weight(const int8_t *allowed_sinks, int8_t node, uint8_t k);

double updated_weight2(const int8_t *allowed_sinks, int8_t node, uint8_t k, int8_t sender);

int8_t next_by_awake_time(kforwarders *fwdrs, int8_t *sent, int8_t size);

#endif
