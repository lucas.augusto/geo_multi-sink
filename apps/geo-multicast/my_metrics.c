/* contiki includes */
#include "contiki.h"
#include "net/rime.h"
#include "sys/rtimer.h"

/* standard library includes */
#include <stdio.h>  /* For printf() */
#include <string.h> /* For memcpy */

/* Project Includes */
#include "my_metrics.h"
#include "geo-multicast.h"
#include "kfunctions.h"

sink_metrics sink;
node_metrics sensor_node;

/*---------------------------------------------------------------------------*/

node_metrics * sensor_metrics(){
	return &sensor_node;
}

/*---------------------------------------------------------------------------*/

void my_metrics_init(){

	sensor_node.energy_cost_tx = 0.0;
	sensor_node.energy_cost_rx = 0.0;
	sensor_node.forwarded_pkt = 0;
	sensor_node.generated_pkt = 0;

	if(is_sink()){
		sink.latency = 0.0;
		sink.received_pkt = 0;
	}

	return;
}

/*---------------------------------------------------------------------------*/

void register_generated_packet(){

	sensor_node.generated_pkt++;

	return;
}

/*---------------------------------------------------------------------------*/

void register_forwarded_packet(){

	sensor_node.forwarded_pkt++;

	return;
}

/*---------------------------------------------------------------------------*/

void register_energy_cost(double energy_cost, uint8_t direction, uint8_t type){

	if(direction==TX){
		if(type==CONTROL){
			sensor_node.ctrl_energy_cost_tx+=energy_cost;
		}
		else{
			sensor_node.energy_cost_tx+=energy_cost;
		}
	}
	else{
		if(type==CONTROL){
			sensor_node.ctrl_energy_cost_rx+=energy_cost;
		}
		else{
			sensor_node.energy_cost_rx+=energy_cost;
		}
	}

	return;
}

/*---------------------------------------------------------------------------*/

void register_received_packet(){

	sink.received_pkt++;

	return;
}

/*---------------------------------------------------------------------------*/

void register_latency(clock_time_t latency){

	sink.latency=sink.latency + (unsigned long long)latency;

	return;
}

/*---------------------------------------------------------------------------*/

void print_my_metrics(uint8_t type){

	double total = sensor_node.energy_cost_tx + sensor_node.energy_cost_rx + sensor_node.ctrl_energy_cost_tx + sensor_node.ctrl_energy_cost_rx;

	if(type == 1){
		TIMESTAMP;
		printf("[GeoM] Final State - Generated: %lu | ", sensor_node.generated_pkt);
		printf("Forwarded: %lu | ", sensor_node.forwarded_pkt);
		printf("Consumed Energy TX_DATA: ");
		NOEXP(sensor_node.energy_cost_tx);
		printf(" | Consumed Energy RX_DATA: ");
		NOEXP(sensor_node.energy_cost_rx);
		printf(" | Consumed Energy TX_CTRL: ");
		NOEXP(sensor_node.ctrl_energy_cost_tx);
		printf(" | Consumed Energy RX_CTRL: ");
		NOEXP(sensor_node.ctrl_energy_cost_rx);
		printf(" | Total Consumed Energy: ");
		NOEXP(total);

		if(is_sink()){
			printf("| Received: %lu | ", sink.received_pkt);
			printf("Sum Latency: %llu | ", (sink.received_pkt==0?0:(unsigned long long)TICKSTOMS(sink.latency)));
			printf("Average Latency: %llu | ", (sink.received_pkt==0?0:(unsigned long long)TICKSTOMS(sink.latency/sink.received_pkt)));
		}

		/*TIMESTAMP;
		printf("[GeoM][Data] GN:%llu|", sensor_node.generated_pkt);
		printf("FW:%llu|", sensor_node.forwarded_pkt);
		printf("TX_D:%e|", sensor_node.energy_cost_tx);
		printf("RX_D:%e|", sensor_node.energy_cost_rx);
		printf("TX_C:%e|", sensor_node.ctrl_energy_cost_tx);
		printf("RX_C:%e|", sensor_node.ctrl_energy_cost_rx);

		if(is_sink()){
			printf("RCV:%llu|", sink.received_pkt);
			printf("LT:%lu||#", sink.latency);
		}
		else{
			printf("RCV:0|");
			printf("LT:0||#");
		}*/

		printf("\n");

		//printf("DEBUG: node %d.%d neighbor count: [%d] \n", rimeaddr_node_addr.u8[0], rimeaddr_node_addr.u8[1], count_valid(get_neighbor_list()->addr, MAX_NEIGHBORS));
	}
	else{
			TIMESTAMP;
			printf("[GeoM] Current State - Generated: %lu | ", sensor_node.generated_pkt);
			printf("Forwarded: %lu | ", sensor_node.forwarded_pkt);
			printf("Consumed Energy TX_DATA: ");
			NOEXP(sensor_node.energy_cost_tx);
			printf(" | Consumed Energy RX_DATA: ");
			NOEXP(sensor_node.energy_cost_rx);
			printf(" | Consumed Energy TX_CTRL: ");
			NOEXP(sensor_node.ctrl_energy_cost_tx);
			printf(" | Consumed Energy RX_CTRL: ");
			NOEXP(sensor_node.ctrl_energy_cost_rx);
			printf(" | Total Consumed Energy: ");
			NOEXP(total);

			if(is_sink()){
				printf(" | Received: %lu | ", sink.received_pkt);
				printf("Sum of Latency: %llu | ", (sink.received_pkt==0?0:(unsigned long long)TICKSTOMS(sink.latency)));
				printf("Average Latency: %llu | ", (sink.received_pkt==0?0:(unsigned long long)TICKSTOMS(sink.latency/sink.received_pkt)));
			}

			printf("\n");
		}

	return;
}
