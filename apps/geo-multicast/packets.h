#ifndef PACKETS_H
#define PACKETS_H

#include "contiki.h"
#include "net/rime.h"
#include "sys/rtimer.h"

#include "support.h"

typedef struct {
	uint8_t ver;		/**< Protocol version. */
	uint8_t type;		/**< Packet type. */
	uint8_t hops;
	pos_t   pos;
	rimeaddr_t originator;
} multicast_hdr_t;

typedef struct {
	multicast_hdr_t hdr;
	//int8_t sink_conn[MAX_SINKS];
	int8_t sink_void_list[MAX_SINKS];
	double consumed_energy;
} broadcast_pkt_t;

typedef struct {
	multicast_hdr_t hdr;
	rimeaddr_t allowed_sinks[MAX_SINKS];
	rimeaddr_t forwarder;
	rimeaddr_t prev;
	rimeaddr_t recovery_trigger_addr;
	uint8_t k;
	uint8_t prep;
	uint8_t recovery_flag;
	int8_t dest;
	pos_t recovery_ref;
	float min_weight;
	//int8_t place_holder1[MAX_NEIGHBORS+MAX_SINKS]; //just to make the size of the KanGuRou and GeoK packets the same
} reading_hdr_t;

typedef struct {
	reading_hdr_t reading_hdr;
	clock_time_t timestamp;
	clock_time_t timestamp_original;
	double energy_cost;
	float value;
} reading_pkt_t;



void prepare_reading_pkt(reading_pkt_t *reading_pkt);

//void print_pkt_info(reading_pkt_t *reading_pkt);


#endif
