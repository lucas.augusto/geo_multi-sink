#ifndef KANYCAST_DECISION_H
#define KANYCAST_DECISION_H

#include "geo-multicast.h"
#include "support.h"

struct gabriel_graph {
	int8_t vertice[MAX_NEIGHBORS];
	uint8_t edge[MAX_NEIGHBORS];
};

/* Structure of the Candidate Forwarders */
typedef struct {
	int8_t sinks[MAX_SINKS];
} sinks_t;

/* Structure of the Candidate Forwarders */
typedef struct {
	sinks_t neighbors[MAX_NEIGHBORS];
	pos_t recovery_ref;
	rimeaddr_t recovery_trigger_addr;
	uint8_t recovery_flag;
	uint8_t count;
} kcandidates;

typedef struct {
		float node[MAX_NEIGHBORS];
		float avg;
		float stdv;
} calculated_weight;

void candidate_forwarders(reading_pkt_t *reading_pkt);

void recovery(const kcandidates *cdts, const int8_t *allowed_sinks, int8_t sender, int8_t dest, reading_pkt_t *reading_pkt);

uint8_t change_face(const pos_t *src, const pos_t *curr, const pos_t *dest, const pos_t *next);

void gabriel_graph_builder(struct gabriel_graph *gg);

void select_k(const kcandidates *cdts, uint8_t k, int8_t *available_sinks, reading_pkt_t *reading_pkt);

void calculate_metric(const kcandidates *cdts, int8_t s, float *w);

#endif
