#ifndef MULTICAST_H
#define MULTICAST_H

#include "lib/list.h"
#include "lib/memb.h"
#include "net/rime.h"
#include "sys/rtimer.h"

#include <stdint.h>

#include "support.h"
#include "packets.h"

#define MULTICAST_VERSION 		1
#define MULTICAST_BROADCAST_POS  1
#define MULTICAST_MULTIHOP 		2

/* This structure holds information about sinks. */
typedef struct {
  /* The ->addr field holds the Rime address of the sink. */
  rimeaddr_t addr[MAX_SINKS];

  /* The list of sinks */
  int8_t sinks[MAX_SINKS];

  /* Count of sinks */
  uint8_t count;

  /* The ->pos field holds the location of the sink */
  pos_t pos[MAX_SINKS];


} ksink;

/* This structure holds information about neighbors. */
typedef struct  {
  /* The ->addr field holds the Rime address of the neighbor. */
  rimeaddr_t addr[MAX_NEIGHBORS];

  /* The ->pos field holds the location of the neighbor */
  pos_t pos[MAX_NEIGHBORS];

  /* The ->sink_list holds the list of sinks */
  int8_t sink_list[MAX_NEIGHBORS][MAX_SINKS];

  /* The ->consumed_energy contains the the value of the consumed energy by the neighbor nodes */
  double consumed_energy[MAX_NEIGHBORS];

} kneighbor;

/* Avoid Loop */
struct pkt_loop {
	int8_t id_1;
	int8_t id_2;
	unsigned long id_3;
	rimeaddr_t sinks[MAX_SINKS];
	uint8_t count;
	int8_t black_list[MAX_NEIGHBORS];
};

extern pos_t own_pos;

extern process_event_t broadcast_event;

PROCESS_NAME(broadcast_process);
PROCESS_NAME(multihop_process);

uint8_t get_received_external_id();

uint8_t get_received_max_k();

int8_t * get_sink_void_list();

kneighbor * get_neighbor_list();

ksink * get_sink_list();

long int get_node_offset();

uint8_t is_sink();

void geo_multicast_init();

//void print_neighbors();

//void print_sinks();

void sink_void_notification(const int8_t *found_sinks, const int8_t *allowed_sinks);

void update_sink_void(int8_t *sink_void_list, int8_t node);

uint8_t send_packet(const reading_pkt_t *reading_pkt, int8_t node, const int8_t *sinks, int8_t k, uint8_t recovery_flag, int8_t dest);

void new_packet_generation(void *data);

void packet_received(reading_pkt_t *pkt);

//int8_t get_black_list(uint8_t node, reading_pkt_t *reading_pkt);

#endif
