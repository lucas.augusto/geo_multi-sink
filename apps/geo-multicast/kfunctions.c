#include "net/rime.h"
#include "sys/rtimer.h"

#include <stdio.h> /* For printf() */
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include "kfunctions.h"
#include "support.h"
#include "multicast_decision.h"
#include "packets.h"

//-----------------------------------------------------------------------------
/* Verifies if the neighbor is a sink */
int8_t neighbor_is_sink(const rimeaddr_t *neighbor_addr) {

	int8_t s;

	/* Check if we know this sink. */
	for(s=0; s<MAX_SINKS; s++) {
		if(!rimeaddr_cmp(&(get_sink_list()->addr[s]), &rimeaddr_null)){
			if(rimeaddr_cmp(&(get_sink_list()->addr[s]), neighbor_addr)) {
				return s;
			}
		}
	}
	return -1;
}

//-----------------------------------------------------------------------------
/* Returns the list of sinks that are closer to the neighbor */
void closest_sinks(int8_t *neighbor_sink_list, pos_t neighbor, pos_t node){

	uint8_t s=0;
	float neighbor_sink=0.0, node_sink=0.0;

	//printf("Neighbor is closer to: ");
	for(s=0; s<MAX_SINKS; s++) {
		if(!rimeaddr_cmp(&(get_sink_list()->addr[s]), &rimeaddr_null)){
			//printf("Pos Neighbor: ");
			//print_pos(neighbor);
			//printf("Pos Sink: ");
			//print_pos(get_sink_list()->pos[s]);
			//printf("Pos Node: ");
			//print_pos(node);
			neighbor_sink=distance(neighbor, get_sink_list()->pos[s]);
			node_sink=distance(node, get_sink_list()->pos[s]);
			//printf("Sink: %d.%d | neighbor_sink: %e | node_sink: %e | distance(node, neighbor): %e | (neighbor_s<node_s && (node_s>(node,neighbor) || neighbor_s==0.0))=%d \n",get_sink_list()->addr[s].u8[0], get_sink_list()->addr[s].u8[1],neighbor_sink, node_sink, distance(node, neighbor), (neighbor_sink<node_sink && (node_sink>distance(node, neighbor) || neighbor_sink==0.0)));
			if(neighbor_sink<node_sink && (node_sink>distance(node, neighbor) || neighbor_sink==0.0)) {
				neighbor_sink_list[s] = s;
				//printf("%d.%d ", get_sink_list()->addr[s].u8[0], get_sink_list()->addr[s].u8[1]);
			}
	    }
	}
	//printf("sinks. \n");

	return;
}

//-----------------------------------------------------------------------------
/* Returns the count of sinks */
int8_t count_valid(const int8_t *node_list, uint8_t list_size){

	int8_t i=0, s_count=0;

	for(i=0;i<list_size;i++){
	    if(node_list[i]>=0){
	    	s_count++;
	    }
	}

	return s_count;
}

//-----------------------------------------------------------------------------
/* Returns the index of an array based on the rime address */
int8_t get_index_by_addr(const rimeaddr_t *item_addr, const rimeaddr_t *list_addr, uint8_t list_size){

	if(item_addr==NULL || list_addr==NULL){
		return -1;
	}

	uint8_t i;
	for(i=0;i<list_size;i++){
		if(rimeaddr_cmp(item_addr, &list_addr[i])){
			return (int)i;
		}
	}
	return -1;
}

//-----------------------------------------------------------------------------
/* Update the list of available sinks */
void update_available_sinks(int8_t *available_sinks, int8_t *used_sinks){
	uint8_t i;

	for(i=0;i<MAX_SINKS;i++){
		if(used_sinks[i]>=0 && available_sinks[i]>=0){
			available_sinks[i]=-1;
		}
	}

	return;
}

//-----------------------------------------------------------------------------
/* Return the closest sink to any other already selected sink*/
int8_t get_the_closest_selected(const int8_t *available_sinks, const int8_t *selected_sinks){

	int8_t s1, s2;
	float min_d=0, aux;
	int8_t min=-1;

	if(count_valid(selected_sinks, MAX_SINKS)==0){
		return get_the_closest(available_sinks);
	}

	for(s1=0;s1<MAX_SINKS;s1++){
		if(available_sinks[s1]>=0){
			for(s2=0;s2<MAX_SINKS;s2++){
				if(selected_sinks[s2]>=0){
					aux=distance(get_sink_list()->pos[s1], get_sink_list()->pos[s2]);
					if(min_d>aux || min==-1){
						min_d = aux;
						min = s1;
					}
				}
			}
		}
	}

	return min;

}

//-----------------------------------------------------------------------------
/* Return the closest sink to the current node */
int8_t get_the_closest(const int8_t *available_sinks){

	int8_t s;
	float min_d=0, aux;
	int8_t min=-1;

	for(s=0;s<MAX_SINKS;s++){
		if(available_sinks[s]>=0){
			aux=distance(own_pos, get_sink_list()->pos[s]);
			if(min_d>aux || min==-1){
				min_d = aux;
				min = s;
			}
		}
	}

	return min;

}

//-----------------------------------------------------------------------------
/* Return the current node update weight */
float updated_weight(const int8_t *allowed_sinks, int8_t node, uint8_t k){

	uint8_t s1, s2;
	int8_t available[MAX_SINKS], selected[MAX_SINKS], index=-1;
	float my_updated_weight, d=0.0, d_min=-1;
	pos_t node_pos;

	memcpy(available, allowed_sinks, MAX_SINKS*sizeof(int8_t));
	memset(selected, -1, MAX_SINKS*sizeof(int8_t));

	if(node==-1){
		node_pos.x=own_pos.x;
		node_pos.y=own_pos.y;
	}
	else{
		node_pos.x=get_neighbor_list()->pos[node].x;
		node_pos.y=get_neighbor_list()->pos[node].y;
	}

	my_updated_weight=0.0;

	for(s1=0; s1<MAX_SINKS; s1++){
		//if(get_sink_list()->sinks[s1]>=0){
		if(available[s1]>=0){
			d = distance(node_pos, get_sink_list()->pos[s1]);
			if(d<d_min || d_min==-1){
				d_min = d;
				index = s1;
			}
		}
	}

	if(index>=0){
		my_updated_weight+=d_min;

		available[index] = -1;
		selected[index] = index;
		k--;
	}

	while(index>=0 && k>0){
		d_min=-1;
		index=-1;
		for(s1=0; s1<MAX_SINKS; s1++){
			if(available[s1]>=0){
				d = distance(node_pos, get_sink_list()->pos[s1]);
				if(d<d_min || d_min==-1){
					d_min = d;
					index = s1;
				}
			}
		}
		if(index>=0){
			for(s2=0; s2<MAX_SINKS; s2++){
				if(selected[s2]>=0){
					for(s1=0; s1<MAX_SINKS; s1++){
						if(available[s1]>=0){
							d = distance(get_sink_list()->pos[s2], get_sink_list()->pos[s1]);
							if(d<d_min || d_min==-1){
								d_min = d;
								index = s1;
							}
						}
					}
				}
			}
		}
		if(index>=0){
			my_updated_weight+=d_min;

			available[index] = -1;
			selected[index] = index;
			k--;
		}
	};

	if(k>0 || my_updated_weight==-1){
		my_updated_weight = 9999999;
	}
	return my_updated_weight;
}

//-----------------------------------------------------------------------------
/* Return the mean */
float mean_w(const kcandidates *cdts, calculated_weight *w, int8_t s, int8_t n, int8_t *sinks){

	float w_sum=0.0;
	int8_t w_count=0;

	if(n==-1){
		for(n=0;n<MAX_NEIGHBORS;n++){
			//if(cdts->neighbors[n]>=0 && cdts->sinks[n][s]>=0){
			if(cdts->neighbors[n].sinks[s]>=0){
				w_sum+=w[s].node[n];
				w_count++;
			}
		}
	}
	else if(s==-1){
		for(s=0;s<MAX_SINKS;s++){
			if(sinks[s]>=0){
				w_sum+=w[s].node[n];
				w_count++;
			}
		}
	}

	return (w_count>0?(w_sum/(float)w_count):-2.0);
}

//-----------------------------------------------------------------------------
/* Return the mean */
float stdev_w(const kcandidates *cdts, calculated_weight *w, int8_t s, int8_t n, int8_t *sinks, float mean_w){

	float w_sum=0.0;
	int8_t w_count=0;

	if(n==-1){
		for(n=0;n<MAX_NEIGHBORS;n++){
			//if(cdts->neighbors[n]>=0 && cdts->sinks[n][s]>=0){
			if(cdts->neighbors[n].sinks[s]>=0){
				w_sum+=(float)fastPrecisePow(w[s].node[n]-w[s].avg,2);
				w_count++;
			}
		}
	}
	else if(s==-1){
		for(s=0;s<MAX_SINKS;s++){
			if(sinks[s]>=0){
				w_sum+=(float)fastPrecisePow(w[s].node[n]-mean_w,2);
				w_count++;
			}
		}
	}

	return (w_count>0?sqrtf(w_sum/(float)w_count):-2.0);
}
