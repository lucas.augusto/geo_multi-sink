/**
 * \file
 *         multicast_decisions.c
 * \author
 *         Lucas Leao <lucas.leao@femto-st.fr>
 *
 */

/* contiki includes */
#include "net/rime.h"

/* standard library includes */
#include <stdio.h>  /* For PRINTF()         */
#include <string.h> /* For memcpy           */
#include <stdlib.h>
#include <math.h>

#include "multicast_decision.h"
#include "support.h"
#include "kfunctions.h"
#include "geo-multicast.h"

#define PI 3.14159265

/* Set DEBUG to 1 to include debug output */
#define DEBUG 0
#if DEBUG
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

/*---------------------------------------------------------------------------*/
void candidate_forwarders(reading_pkt_t *reading_pkt){

	int8_t j,h,sink_counter,k2=0;
	int8_t found_sinks[MAX_SINKS];
	int8_t dest=-1;

#ifdef CALCULATION_TIME
	clock_time_t first_mark, original_timestamp;
	first_mark = (clock_time_t)(SYSNOW+get_node_offset());
	original_timestamp = reading_pkt->timestamp;
#endif

	//Get the list of available sinks
	int8_t allowed_sinks[MAX_SINKS];
	memset(allowed_sinks, -1, MAX_SINKS*sizeof(int8_t));
	for(j=0; j<MAX_SINKS; j++){
		if(!rimeaddr_cmp(&reading_pkt->reading_hdr.allowed_sinks[j], &rimeaddr_null)){
			allowed_sinks[j] = j;
		}
	}

	uint8_t k = reading_pkt->reading_hdr.k;

	int8_t sender = get_index_by_addr(&reading_pkt->reading_hdr.prev, get_neighbor_list()->addr, MAX_NEIGHBORS);

	float min_weight = reading_pkt->reading_hdr.min_weight;

	float node_weight = updated_weight(allowed_sinks, -1, k);

	//List of Candidates

	kcandidates cdts;
	memset(&cdts, 0, sizeof(kcandidates));
	memset(cdts.neighbors, -1, sizeof(sinks_t)*MAX_NEIGHBORS);
	//memset(cdts.neighbors, -1, sizeof(int8_t)*MAX_NEIGHBORS);
	//memset(cdts.sinks, -1, sizeof(int8_t)*MAX_NEIGHBORS*MAX_SINKS);


	PRINTF("Selecting candidate neighbors! \n");
	cdts.recovery_ref.x = reading_pkt->reading_hdr.recovery_ref.x;
	cdts.recovery_ref.y = reading_pkt->reading_hdr.recovery_ref.y;
	cdts.recovery_flag = reading_pkt->reading_hdr.recovery_flag;
	rimeaddr_copy(&cdts.recovery_trigger_addr, &reading_pkt->reading_hdr.recovery_trigger_addr);

	memset(found_sinks, -1, sizeof(int8_t)*MAX_SINKS);

	// check whether the node should leave the recovery mode
	if(min_weight>node_weight || min_weight==-1){

		reading_pkt->reading_hdr.min_weight = node_weight;

		// go through the list of neighbors
		for(j=0; j<MAX_NEIGHBORS; j++) {
			if(j!=sender && !rimeaddr_cmp(&(get_neighbor_list()->addr[j]), &rimeaddr_null) && get_black_list(j, reading_pkt)!=1) {
			//if(j!=sender && !rimeaddr_cmp(&(get_neighbor_list()->addr[j]), &rimeaddr_null)) {
				//printf("Checking neighbor: %d.%d with sinks: ", get_neighbor_list()->addr[j].u8[0], get_neighbor_list()->addr[j].u8[1]);
				sink_counter = 0;
				for(h=0;h<MAX_SINKS;h++){
					if(get_neighbor_list()->sink_list[j][h]>=0){
						// select only the allowed sinks
						if(allowed_sinks[h]>=0){
							//printf("  %d.%d", get_sink_list()->addr[h].u8[0], get_sink_list()->addr[h].u8[1]);
							// check if it is the first time this sink is seen -- we must find at least k sinks
							if(found_sinks[h]==-1){
								found_sinks[h]=h;
								k2++;
								//printf("*");
							}
							//cdts.sinks[j][h]=h;
							cdts.neighbors[j].sinks[h]=h;
							sink_counter++;
						}
					}
				}
				// the neighbor is closer to at least one allowed sink
				/*if(sink_counter>0){
					cdts.neighbors[j] = j;
					cdts.count++;
				}*/
				//printf("\n");
		    }
		}

		if(k2==k && cdts.recovery_flag){
			cdts.recovery_flag=0;
		}

		if(k2>0){
			//printf("k2 = %d \n", k2);
			update_available_sinks(allowed_sinks, found_sinks);
#ifdef CALCULATION_TIME
			reading_pkt->timestamp = (clock_time_t)(original_timestamp + (SYSNOW + get_node_offset() - first_mark));
			//printf("SYSNOW: %lu | pkt[pkt_count].timestamp: %lu | Latency: %lu \n", SYSNOW+get_node_offset(), reading_pkt->timestamp, (SYSNOW+get_node_offset()-reading_pkt->timestamp));
#endif
			select_k(&cdts, k2, found_sinks, reading_pkt);
#ifdef CALCULATION_TIME
			first_mark = (clock_time_t)(SYSNOW+get_node_offset());
			original_timestamp = reading_pkt->timestamp;
#endif
		}

		k2=count_valid(allowed_sinks, MAX_SINKS);
		//printf("selec_k -- k2 = %d \n", k2);

		if(k2==0){
			return;
		}

		// notify the neighbors that the node is in a void for the sinks in allowed_sinks
		// call the recovery function if it was not possible to find at least k sinks
		if(k2>0){
			sink_void_notification(found_sinks, allowed_sinks);

			dest=-1;
			if(reading_pkt->reading_hdr.dest>=0){
				dest = allowed_sinks[reading_pkt->reading_hdr.dest];
			}

			if(dest==-1){
				dest = get_the_closest(allowed_sinks);
				reading_pkt->reading_hdr.dest = dest;
			}
		}
	}

	if(dest==-1){
		// continue with the recovery mode
		dest = reading_pkt->reading_hdr.dest;
		if(rimeaddr_cmp(&rimeaddr_node_addr, &(get_sink_list()->addr[dest]))){
			dest = get_the_closest(allowed_sinks);
		}
	}

	if(cdts.recovery_flag == 0){
		reading_pkt->reading_hdr.recovery_ref.x = own_pos.x;
		reading_pkt->reading_hdr.recovery_ref.y = own_pos.y;
		cdts.recovery_ref.x = own_pos.x;
		cdts.recovery_ref.y = own_pos.y;
		rimeaddr_copy(&reading_pkt->reading_hdr.recovery_trigger_addr, &rimeaddr_node_addr);
		rimeaddr_copy(&cdts.recovery_trigger_addr, &rimeaddr_node_addr);
		cdts.recovery_flag = 1;
		sender=-1;
	}

#ifdef CALCULATION_TIME
			reading_pkt->timestamp = (clock_time_t)(original_timestamp + (SYSNOW + get_node_offset() - first_mark));
			//printf("SYSNOW: %lu | pkt[pkt_count].timestamp: %lu | Latency: %lu \n", SYSNOW+get_node_offset(), reading_pkt->timestamp, (SYSNOW+get_node_offset()-reading_pkt->timestamp));
#endif
	recovery(&cdts, allowed_sinks, sender, dest, reading_pkt);

	return;
}

/*---------------------------------------------------------------------------*/

void recovery(const kcandidates *cdts, const int8_t *allowed_sinks, int8_t sender, int8_t dest, reading_pkt_t *reading_pkt){

	uint8_t flag;
	int8_t n, v, v_min, v_max;
	float ang=0, ang_min, ang_max, a, b, c, d, m;
	struct gabriel_graph gg;

#ifdef CALCULATION_TIME
	clock_time_t first_mark, original_timestamp;
	first_mark = (clock_time_t)(SYSNOW+get_node_offset());
	original_timestamp = reading_pkt->timestamp;
#endif

	// calculate the gabriel graph of the sub-graph formed by the node and its neighbors
	memset(&gg, 0, sizeof(struct gabriel_graph));
	memset(gg.vertice, -1, sizeof(int8_t)*MAX_NEIGHBORS);
	gabriel_graph_builder(&gg);

	v=-2;
	flag=1;
	v_max=-2;
	v_min=-2;
	ang=0;
	ang_min=361;
	ang_max=-1;
	if(sender==-1){
		b = distance(get_sink_list()->pos[dest],own_pos);
	}
	else{
		b = distance(get_neighbor_list()->pos[sender],own_pos);
	}
	do{
		for(n=0; n<MAX_NEIGHBORS; n++){
			// neighbor cannot be the sender nor the already selected node
			if(n!=sender && !rimeaddr_cmp(&(get_neighbor_list()->addr[n]), &rimeaddr_null)){
				// check if the edge exist in the gabriel graph
				if(gg.edge[n]==1){
					if(v==-2){
						if(sender==-1){
							a = (own_pos.x - get_sink_list()->pos[dest].x)*(own_pos.x - get_neighbor_list()->pos[n].x) + (own_pos.y - get_sink_list()->pos[dest].y)*(own_pos.y - get_neighbor_list()->pos[n].y);
							c = (get_neighbor_list()->pos[n].x - own_pos.x)*(get_sink_list()->pos[dest].y - own_pos.y) - (get_neighbor_list()->pos[n].y - own_pos.y)*(get_sink_list()->pos[dest].x - own_pos.x);
							//printf("Testing - n: %d.0, sink: %d.0, self: %d.0 | ", get_neighbor_list()->addr[n].u8[0], get_sink_list()->addr[dest].u8[0], rimeaddr_node_addr.u8[0]);
						}
						else{
							a = (own_pos.x - get_neighbor_list()->pos[sender].x)*(own_pos.x - get_neighbor_list()->pos[n].x) + (own_pos.y - get_neighbor_list()->pos[sender].y)*(own_pos.y - get_neighbor_list()->pos[n].y);
							c = (get_neighbor_list()->pos[n].x - own_pos.x)*(get_neighbor_list()->pos[sender].y - own_pos.y) - (get_neighbor_list()->pos[n].y - own_pos.y)*(get_neighbor_list()->pos[sender].x - own_pos.x);
							//printf("Testing - n: %d.0, sender: %d.0, self: %d.0 | ", get_neighbor_list()->addr[n].u8[0], get_neighbor_list()->addr[sender].u8[0], rimeaddr_node_addr.u8[0]);
						}
					}
					else{
						a = (own_pos.x - get_neighbor_list()->pos[v].x)*(own_pos.x - get_neighbor_list()->pos[n].x) + (own_pos.y - get_neighbor_list()->pos[v].y)*(own_pos.y - get_neighbor_list()->pos[n].y);
						c = (get_neighbor_list()->pos[n].x - own_pos.x)*(get_neighbor_list()->pos[v].y - own_pos.y) - (get_neighbor_list()->pos[n].y - own_pos.y)*(get_neighbor_list()->pos[v].x - own_pos.x);
						//printf("Testing - n: %d.0, v: %d.0, self: %d.0 | ", get_neighbor_list()->addr[n].u8[0], get_neighbor_list()->addr[v].u8[0], rimeaddr_node_addr.u8[0]);
					}
					d = distance(get_neighbor_list()->pos[n],own_pos);
					m = a/(b*d);
					ang = acos(m)*(180.0/PI);
					//printf("ang_min = %d.%d | ang_max = %d.%d | ang = %d.%d | c = %d.%d | ", (int)ang_min, decimals(ang_min), (int)ang_max, decimals(ang_max), (int)ang, decimals(ang), (int)c, decimals(c));
					if(ang<ang_min && (int)c>=0){
						ang_min = ang;
						v_min = n;
						//printf("MIN: v_min = %d.0 | ", get_neighbor_list()->addr[v_min].u8[0]);
					}
					if(ang>ang_max && (int)c<0){
						ang_max = ang;
						v_max = n;
						//printf("MAX: v_max = %d.0 | ", get_neighbor_list()->addr[v_max].u8[0]);
					}
					//printf("\n");
				}
			}
		}

		// check whether we need to take the opposite side or go back
		if(v_min==-2){
			if(v_max>=0){
				//printf("opposite side \n");
				v = v_max;
			}
			else{
				//printf("go back \n");
				v = sender;
			}
			break;
		}

		flag = change_face(&cdts->recovery_ref, &own_pos, &(get_sink_list()->pos[dest]), &(get_neighbor_list()->pos[v_min]));
		//printf("flag: %d | v_min = %d.0 | v = %d.0\n", flag, get_neighbor_list()->addr[v_min].u8[0], get_neighbor_list()->addr[v].u8[0]);
		if(v==v_min){
			flag=0;
		}
		v = v_min;

	}while(flag);

	// register the neighbor as the forwarder for the sink s
	if(v>=0){
#ifdef CALCULATION_TIME
		reading_pkt->timestamp = (clock_time_t)(original_timestamp + (SYSNOW + get_node_offset() - first_mark));
		//printf("SYSNOW: %lu | pkt[pkt_count].timestamp: %lu | Latency: %lu \n", SYSNOW+get_node_offset(), reading_pkt->timestamp, (SYSNOW+get_node_offset()-reading_pkt->timestamp));
#endif
		//printf("Neighbor: %d.0 selected!\n", get_neighbor_list()->addr[v].u8[0]);
		send_packet(reading_pkt, v, allowed_sinks, count_valid(allowed_sinks, MAX_SINKS), cdts->recovery_flag, dest);
	}

	return;
}

/*---------------------------------------------------------------------------*/

uint8_t change_face(const pos_t *src, const pos_t *curr, const pos_t *dest, const pos_t *next){

	uint8_t flag=1;
	float a, b, c, d, dist;
	pos_t cf;

	a = (curr->x - src->x)*(dest->y - src->y) - (src->y - curr->y)*(src->x - dest->x);

	if(a==0){
		return flag;
	}

	b = (next->x - src->x)*(dest->y - src->y) - (src->y - next->y)*(src->x - dest->x);

	if((a<0 && b<0) || (a>0 && b>0)){
		flag = 0;
	}

	if(flag){
		flag = 0;

		a = (dest->y - src->y)/(dest->x - src->x);
		b = (next->y - curr->y)/(next->x - curr->x);

		c = src->y - a*src->x;
		d = curr->y - b*curr->x;

		cf.x = (d-c)/(a-b);
		cf.y = (a*cf.x)+c;

		dist = distance(cf, *src) + distance(cf, *dest);

		if((int)(distance(*src,*dest)) == (int)(powi(10,6)*dist)){
			flag = 1;
		}
	}

	return flag;
}

/*---------------------------------------------------------------------------*/

void gabriel_graph_builder(struct gabriel_graph *gg){

	float d1, d2, d3;
	uint8_t n2, n3;

	// the process to calculate the gabriel graph was adapted from here: http://onlinelibrary.wiley.com/doi/10.1111/j.1538-4632.1980.tb00031.x/pdf

	for(n2=0; n2<MAX_NEIGHBORS; n2++){
		if(!rimeaddr_cmp(&(get_neighbor_list()->addr[n2]), &rimeaddr_null)){
			gg->vertice[n2]=n2;
			gg->edge[n2] = 1;
			for(n3=0; n3<MAX_NEIGHBORS; n3++){
				if(n3!=n2 && !rimeaddr_cmp(&(get_neighbor_list()->addr[n3]), &rimeaddr_null)){
					d1 = distance(own_pos, get_neighbor_list()->pos[n2]);
					d2 = distance(own_pos, get_neighbor_list()->pos[n3]);
					d3 = distance(get_neighbor_list()->pos[n2], get_neighbor_list()->pos[n3]);
					if(d1*d1 > (d2*d2 + d3*d3)){
						gg->edge[n2] = 0;
					}
				}
			}
		}
	}
	return;
}

/*---------------------------------------------------------------------------*/

void select_k(const kcandidates *cdts, uint8_t k, int8_t *available_sinks, reading_pkt_t *reading_pkt){

#ifdef CALCULATION_TIME
	clock_time_t first_mark, original_timestamp;
	first_mark = (clock_time_t)(SYSNOW+get_node_offset());
	original_timestamp = reading_pkt->timestamp;
#endif

	int8_t n, s, found;

	calculated_weight w[MAX_SINKS];

	struct p_list {
			int8_t node[MAX_NEIGHBORS];
			int8_t sink[MAX_SINKS];
			int8_t closer_sink;
	} p[k];

	// Initialization ---------------------------------------------------------
	memset(w, -1.0, sizeof(calculated_weight)*MAX_SINKS);
	memset(p, -1, sizeof(struct p_list)*k);
	//-----------------------------------------------------------------------

	//Calculate the W for all available sinks and sensors
	for(s=0;s<MAX_SINKS;s++){
		if(available_sinks[s]>=0){

			//Calculate the metric
			calculate_metric(cdts, s, w[s].node);

			//We calculate the the mean
			w[s].avg = mean_w(cdts, w, s, -1, available_sinks);

			//We calculate the standard deviation
			w[s].stdv = stdev_w(cdts, w, s, -1, available_sinks, 0.0);

			//We select the nodes with a deviation from the average
			for(n=0;n<MAX_NEIGHBORS;n++){
				//if(cdts->neighbors[n]>=0 && cdts->sinks[n][s]>=0 && w[s].node[n]>(w[s].avg+(w[s].stdv*DEVIATION_FACTOR))){
				if(cdts->neighbors[n].sinks[s]>=0 && w[s].node[n]>(w[s].avg+(w[s].stdv*DEVIATION_FACTOR))){
					w[s].node[n] = -1.0;
				}
			}
		}
	}

	//Go through the list of sinks and neighbors to check for intersections
	int8_t temp_nodes[MAX_NEIGHBORS], max_nodes[MAX_NEIGHBORS], max_intersec, index, i, j, selected_sinks[MAX_SINKS];
	memset(selected_sinks, -1, sizeof(int8_t)*MAX_SINKS);
	i=0;
	while(count_valid(available_sinks, MAX_SINKS)){
		s=get_the_closest_selected(available_sinks, selected_sinks);

		if(s==-1){
			PRINTF("ERROR! Closest sink not found. \n");
			return;
		}

		max_intersec=0;
		index=-1;
		memset(max_nodes, -1, MAX_NEIGHBORS*sizeof(int8_t));

		//Go through the list P to find intersection
		for(j=0;j<i;j++){
			found=0;
			memset(temp_nodes, -1, MAX_NEIGHBORS*sizeof(int8_t));
			for(n=0;n<MAX_NEIGHBORS;n++){
				//if(cdts->neighbors[n]>=0 && cdts->sinks[n][s]>=0 && w[s].node[n]>=0.0){
				if(cdts->neighbors[n].sinks[s]>=0 && w[s].node[n]>=0.0){
					if(p[j].node[n]>=0){
						temp_nodes[n]=n;
						found++;
					}
				}
			}
			if(found>max_intersec){
				index=j;
				max_intersec=found;
				memcpy(max_nodes, temp_nodes, sizeof(int8_t)*MAX_NEIGHBORS);
			}
		}
		if(max_intersec==0 || index==-1){
			p[i].sink[s]=s;
			//printf("New entry in p[%d].sink[%d]=%d \n", i, s, p[i].sink[s]);
			p[i].closer_sink=s;
			for(n=0;n<MAX_NEIGHBORS;n++){
				//if(cdts->neighbors[n]>=0 && cdts->sinks[n][s]>=0 && w[s].node[n]>=0){
				if(cdts->neighbors[n].sinks[s]>=0 && w[s].node[n]>=0){
					p[i].node[n]=n;
				}
			}
			i++;
		}
		else{
			p[index].sink[s]=s;
			//printf("Intersection found, max_intersec = %d! p[%d].sink[%d]=%d \n", max_intersec, index, s, p[index].sink[s]);
			memcpy(p[index].node, max_nodes, sizeof(int8_t)*MAX_NEIGHBORS);
		}
		available_sinks[s]=-1;
		selected_sinks[s]=s;
	}

	//Create the forwarders list with the list of sink/neighbor intersection
	float temp_n[MAX_NEIGHBORS], stdev_n, metric_min, stdev_min;

	for(j=0;j<i;j++){
		index=-1;
		metric_min=-1.0;
		stdev_min=-1.0;
		if(count_valid(p[j].sink, MAX_SINKS)>1 && count_valid(p[j].node, MAX_NEIGHBORS)>1){
			//if the flow entered here it means that we have more than one neighbor to more than one sink;
			//we need to check which neighbor is the most suitable to the most of the sinks
			for(n=0;n<MAX_NEIGHBORS;n++){
				//we go through all neighbors and all sinks to calculate the average metric of that neighbor in relation to all sinks
				if(p[j].node[n]>=0){

					//We calculate the mean
					temp_n[n]=mean_w(cdts, w, -1, n, p[j].sink);

					if(metric_min>temp_n[n] || metric_min==-1.0){
						index = n;
						metric_min = temp_n[n];
					}

					//If two neighbors have the same average distance, we select the one with the smaller variation
					if(metric_min == temp_n[n]){

						//We calculate the standard deviation
						stdev_n = stdev_w(cdts, w, -1, n, p[j].sink, temp_n[n]);

						//We select the neighbor with the smallest variation of the metric for each sink
						if(stdev_min>stdev_n || stdev_min==-1.0){
							index = n;
							stdev_min = stdev_n;
						}
					}
				}
			}
		}
		else{
			if(count_valid(p[j].node, MAX_NEIGHBORS)>1){
				//If the flow entered here it means that there is only one sink which is common to more than one neighbor.
				//We need to know which sink is that
				for(s=0;s<MAX_SINKS;s++){
					if(p[j].sink[s]>=0){
						break;
					}
				}
				//We go through the list of neighbor to select the one with the min value for the combined metric
				for(n=0;n<MAX_NEIGHBORS;n++){
					if(p[j].node[n]>=0){
						if(metric_min>w[s].node[n] || metric_min==-1.0){
							index = n;
							metric_min = w[s].node[n];
						}
					}
				}
			}
			else{
				//if the flow entered here, it means that there is only one neighbor to one or more sinks.
				//we select this neighbor directly, but we need to search for it
				for(n=0;n<MAX_NEIGHBORS;n++){
					if(p[j].node[n]>=0){
						index = n;
						break;
					}
				}
			}
		}
		n=index;

		if(n>=0){
			//printf("Select k: neighbor %d.%d selected to %d sinks.\n", get_neighbor_list()->addr[n].u8[0], get_neighbor_list()->addr[n].u8[1], count_valid(p[j].sink, MAX_SINKS));

#ifdef CALCULATION_TIME
			reading_pkt->timestamp = (clock_time_t)(original_timestamp + (SYSNOW + get_node_offset() - first_mark));
			//printf("SYSNOW: %lu | pkt[pkt_count].timestamp: %lu | Latency: %lu \n", SYSNOW+get_node_offset(), reading_pkt->timestamp, (SYSNOW+get_node_offset()-reading_pkt->timestamp));
#endif

			send_packet(reading_pkt, n, p[j].sink, count_valid(p[j].sink, MAX_SINKS), cdts->recovery_flag, p[j].closer_sink);
		}
	}

	return;

}

/*---------------------------------------------------------------------------*/

void calculate_metric(const kcandidates *cdts, int8_t s, float *w){

	int8_t n;

	float z, h, y;
	float dist[MAX_NEIGHBORS], min_dist, max_dist;
	double enrg[MAX_NEIGHBORS], min_enrg, max_enrg;
	double min_consumed, max_consumed;

	// Distance ---------------------------------------------------------------
	max_dist=-1;
	min_dist=-1;
	for(n=0;n<MAX_NEIGHBORS;n++){
		//if(cdts->neighbors[n]>=0){
		if(cdts->neighbors[n].sinks[s]>=0){
			dist[n]=distance(get_neighbor_list()->pos[n],get_sink_list()->pos[s]);
			if(min_dist>dist[n] || min_dist==-1){
				min_dist = dist[n];
			}
			if(max_dist<dist[n] || max_dist==-1){
				max_dist = dist[n];
			}
		}
	}

	// Energy Cost + Consumed Energy -----------------------------
	max_enrg=-1.0;
	min_enrg=-1.0;
	max_consumed=-1.0;
	min_consumed=-1.0;
	for(n=0;n<MAX_NEIGHBORS;n++){
			//if(cdts->neighbors[n]>=0){
		if(cdts->neighbors[n].sinks[s]>=0){
			// Energy Cost --------------------------------------------------------
			enrg[n] = ENERGY_COST_TX(own_pos, get_neighbor_list()->pos[n],reading_pkt_t);

			if(min_enrg>enrg[n] || min_enrg==-1.0){
				min_enrg = enrg[n];
			}
			if(max_enrg<enrg[n] || max_enrg==-1.0){
				max_enrg = enrg[n];
			}

			// Consumed Energy ----------------------------------------------------
			if(min_consumed>get_neighbor_list()->consumed_energy[n] || min_consumed==-1.0){
				min_consumed = get_neighbor_list()->consumed_energy[n];
			}
			if(max_consumed<get_neighbor_list()->consumed_energy[n] || max_consumed==-1.0){
				max_consumed = get_neighbor_list()->consumed_energy[n];
			}
		}
	}

	//Calculate the metric
	for(n=0;n<MAX_NEIGHBORS;n++){
		//if(cdts->neighbors[n]>=0 && cdts->sinks[n][s]>=0){
		if(cdts->neighbors[n].sinks[s]>=0){
			// DIST to sinks
			h = ((max_dist-min_dist)==0.0?0.0:(dist[n] - min_dist)/(max_dist - min_dist));

			// ENERGY COST
			z = ((max_enrg-min_enrg)==0.0?0.0:(enrg[n] - min_enrg)/(max_enrg - min_enrg));

			// ENERGY CONSUMPTION
			y = ((max_consumed-min_consumed)==0.0?0.0:(get_neighbor_list()->consumed_energy[n] - min_consumed)/(max_consumed - min_consumed));

			//AGGREGATED METRIC
			if(s==neighbor_is_sink(&(get_neighbor_list()->addr[n]))){
				w[n] = 0.0;
			}
			else{
				w[n] = (DIST_SINK_METRIC_WEIGHT*h) + (ENERGY_METRIC_WEIGHT*z) + (CONSUMED_ENERGY_WEIGHT*y);
			}
		}
	}

	return;
}

/*---------------------------------------------------------------------------*/
