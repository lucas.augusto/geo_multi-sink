/*
 * Copyright (c) 2006, Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

/**
 * \file
 *         geo-multicast.c
 * \author
 *         Lucas Leao <lucas.leao@femto-st.fr>
 *
 *  Based on the geoware_app:
 *  https://github.com/noname77/geoware
 *
 */

/* contiki includes */
#include "contiki.h"
#include "net/rime.h"
#include "dev/serial-line.h"
#ifndef SENSLAB
	#include "dev/button-sensor.h"
#endif
#include "lib/random.h"
#include "sys/rtimer.h"

/* standard library includes */
#include <stdio.h>  /* For printf() */
#include <string.h> /* For memcpy */
#include <stdlib.h>

/* project includes */
#include "support.h"
#include "packets.h"
#include "kfunctions.h"
#include "multicast_decision.h"
#include "my_metrics.h"
#include "geo-multicast.h"

#define MAX_PKT			MAX_SINKS

/* Set DEBUG to 1 to include debug output */
#define DEBUG 0
#if DEBUG
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

/* Set LOGS to 1 to include log output */
#define LOGS 1
#if LOGS
#define PRINTF_LOG(...) printf(__VA_ARGS__)
#else
#define PRINTF_LOG(...)
#endif

/*---------------------------------------------------------------------------*/

/* node's position */
pos_t own_pos;

/* the list of sinks in void */
int8_t my_sink_void_list[MAX_SINKS];

/* time offset for network sync */
long int my_offset;

/* if node is sink */
uint8_t i_am_sink = 0;

/* variable with the received max k */
uint8_t received_max_k;

/* External node ID */
uint8_t myid = 0;

#ifdef SENSLAB
/* Wait for the broadcast message of all neighbors */
uint8_t count_neighbors = 0;
#endif

/* Loop avoidance */
struct pkt_loop count_loop;


/*---------------------------------------------------------------------------*/

/* processes */
PROCESS(broadcast_process, "Broadcast process");
PROCESS(multihop_process, "Multihop process");
PROCESS(boot_process, "Boot process");

/*---------------------------------------------------------------------------*/

/* events */
process_event_t broadcast_pos_event;
process_event_t newpacket_event;
process_event_t multipacket_event;

/*---------------------------------------------------------------------------*/

/* List of all neighbors */
kneighbor neighbors_list;

/* List of sinks */
ksink my_sink_list;

/*---------------------------------------------------------------------------*/
/*
 * Get Black List.
 */
int8_t get_black_list(uint8_t node, reading_pkt_t *reading_pkt){

	if(count_loop.id_1==reading_pkt->reading_hdr.hdr.originator.u8[0] && count_loop.id_2==reading_pkt->reading_hdr.hdr.originator.u8[1] && count_loop.id_3==reading_pkt->timestamp){
		return count_loop.black_list[node];
	}
	return -1;
}

/*---------------------------------------------------------------------------*/
/*
 * Get received external id.
 */
uint8_t get_received_external_id(){
	return myid;
}

/*---------------------------------------------------------------------------*/
/*
 * Get received max k.
 */
uint8_t get_received_max_k(){
	return received_max_k;
}

/*---------------------------------------------------------------------------*/
/*
 * Get node's sink void list.
 */
int8_t * get_sink_void_list(){
	return my_sink_void_list;
}

/*---------------------------------------------------------------------------*/
/*
 * Get neighbors list.
 */
kneighbor * get_neighbor_list(){
	return &neighbors_list;
}

/*---------------------------------------------------------------------------*/
/*
 * Get sinks list.
 */
ksink * get_sink_list(){
	return &my_sink_list;
}

/*---------------------------------------------------------------------------*/
/*
 * Get node's offset for network sync.
 */
long int get_node_offset(){
	return my_offset;
}

/*---------------------------------------------------------------------------*/
/*
 * Get node's type.
 */
uint8_t is_sink(){
	return i_am_sink;
}

/*---------------------------------------------------------------------------*/
/*
 * This function prints the neighbor list.
 */
/*void
print_neighbors() {
	uint8_t i, j;
	printf("Neighbors:\n");
	for(j=0; j<MAX_NEIGHBORS; j++) {
		if(!rimeaddr_cmp(&neighbors_list.addr[j],&rimeaddr_null)){
			printf("Rime Address: %d.%d | # closest sinks: %d | sink list: ", neighbors_list.addr[j].u8[0], neighbors_list.addr[j].u8[1], count_valid(neighbors_list.sink_list[j], MAX_SINKS));
			for(i=0;i<MAX_SINKS;i++){
				if(neighbors_list.sink_list[j][i]>=0){
					printf("%d.%d, ", my_sink_list.addr[i].u8[0], my_sink_list.addr[i].u8[1]);
				}
			}
			printf("Position: ");
			print_pos(neighbors_list.pos[j]);
		}
	}
}*/
/*---------------------------------------------------------------------------*/
/*
 * This function prints the sink list (all sinks and void sinks).
 */
/*void
print_sinks() {
	uint8_t i;
	for(i=0;i<MAX_SINKS;i++){
		if(!rimeaddr_cmp(&my_sink_list.addr[i], &rimeaddr_null)){
			printf("%d.%d, ", my_sink_list.addr[i].u8[0], my_sink_list.addr[i].u8[1]);
		}
	}
}*/
/*---------------------------------------------------------------------------*/
/*
 * This function is called by the ctimer present in each neighbor
 * table entry. The function removes the neighbor from the table
 * because it has become too old.
 */
/*static void
remove_neighbor(uint8_t index)
{

	rimeaddr_copy(&neighbors_list.addr[index],&rimeaddr_null);

}*/

/*---------------------------------------------------------------------------*/

static int8_t
add_sink(pos_t pos, rimeaddr_t *addr)
{
	int8_t sink;

	sink = get_index_by_addr(addr, my_sink_list.addr, MAX_SINKS);

	/* If sink is -1, this sink was not found in our list, and we
     allocate the new sink */
	if(sink == -1) {
		sink = get_index_by_addr(&rimeaddr_null, my_sink_list.addr, MAX_SINKS);

		/* If we could not allocate a new sink entry, we give up. */
		if(sink == -1) {
			return -1;
		}

		/* Initialize the fields. */
		rimeaddr_copy(&my_sink_list.addr[sink], addr);

		my_sink_list.sinks[sink] = sink;

		/* Register the sink position */
		my_sink_list.pos[sink].x = pos.x;
		my_sink_list.pos[sink].y = pos.y;

		if(sink == 0){
			my_sink_list.count = 1;
		}
		else{
			my_sink_list.count++;
		}

		PRINTF("DEBUG: sink %d.%d at (%d.%d;%d.%d) included. \n", addr->u8[0], addr->u8[1], (int)my_sink_list.pos[sink].x, decimals(my_sink_list.pos[sink].x), (int)my_sink_list.pos[sink].y, decimals(my_sink_list.pos[sink].y));

		if(rimeaddr_cmp(&my_sink_list.addr[sink], &rimeaddr_node_addr)) {
			i_am_sink = 1;
			PRINTF("DEBUG: I am sink!\n");
		}

	}

	return sink;
};

/*---------------------------------------------------------------------------*/

static int8_t
//add_neighbor(pos_t pos, rimeaddr_t *addr, int8_t *sink_conn)
add_neighbor(pos_t pos, rimeaddr_t *addr)
{
	int8_t node;

	node = get_index_by_addr(addr, neighbors_list.addr, MAX_NEIGHBORS);

	/* If node is -1, this neighbor was not found in our list, and we
	     allocate the new neighbor. */
#ifdef SENSLAB
	if(node == -1 && count_neighbors == 0){
#else
	if(node == -1) {
#endif
		node = get_index_by_addr(&rimeaddr_null, neighbors_list.addr, MAX_NEIGHBORS);

		/* If we could not allocate a new neighbor entry, we give up. We
       could have reused an old neighbor entry, but we do not do this
       for now. */ // TODO ----> look for a worst neighbor to replace
		if(node == -1) {
			return -1;
		}

		/* Initialize the fields. */
		rimeaddr_copy(&neighbors_list.addr[node], addr);

		/* initialize the sink_list and the sink_void_list */
		memset(neighbors_list.sink_list[node], -1, sizeof(int8_t)*MAX_SINKS);

		/* set its position */
		neighbors_list.pos[node].x = pos.x;
		neighbors_list.pos[node].y = pos.y;

		/* set the sink_list */
		closest_sinks(neighbors_list.sink_list[node], neighbors_list.pos[node], own_pos);

		/* set the neighbor's energy consumption */
		neighbors_list.consumed_energy[node] = 0.0;

		PRINTF_LOG("Node %d.%d added neighbor %d.%d in position \n", rimeaddr_node_addr.u8[0], rimeaddr_node_addr.u8[1], addr->u8[0], addr->u8[1]);
		//print_pos(neighbors_list.pos[node]);

	}

#ifdef SENSLAB
	if(node>=0){
		if(neighbors_list.pos[node].x != pos.x || neighbors_list.pos[node].y != pos.y){
			/* set its position */
			neighbors_list.pos[node].x = pos.x;
			neighbors_list.pos[node].y = pos.y;
			printf("DEBUG: node %d.%d updated neighbor %d.%d \n", rimeaddr_node_addr.u8[0], rimeaddr_node_addr.u8[1], addr->u8[0], addr->u8[1]);
			closest_sinks(neighbors_list.sink_list[node], neighbors_list.pos[node], own_pos);
			//count_neighbors++;
		}
	}
#endif

	/* set the list of connected sinks */
	//memcpy(neighbors_list.sink_conn[node], sink_conn, MAX_SINKS*sizeof(int8_t));

	return node;
};

/*****************************************************************************/
/* Broadcast Process                                                         */
/*****************************************************************************/

/*---------------------------------------------------------------------------*/
/* This function is called whenever a broadcast message is received. */
static void
broadcast_recv(struct broadcast_conn *c, const rimeaddr_t *from)
{
	broadcast_pkt_t broadcast_pkt;

	register_energy_cost(ENERGY_COST_RX(broadcast_pkt_t), RX, CONTROL);

	/* copying to avoid unalignment issues */
	memcpy(&broadcast_pkt, packetbuf_dataptr(), sizeof(broadcast_pkt_t));

	//printf("WARNING: Received broadcast from: %d.%d\n", from->u8[0], from->u8[1]);
	//PRINTF("ver: %d, type: %d\n", broadcast_pkt.hdr.ver, broadcast_pkt.hdr.type);

	if(broadcast_pkt.hdr.ver != MULTICAST_VERSION) {
		return;
	}

	if(broadcast_pkt.hdr.type == MULTICAST_BROADCAST_POS) {

		int8_t node=-1;
		//int8_t sink_conn[MAX_SINKS];

		//memcpy(sink_conn, broadcast_pkt.sink_conn, sizeof(uint8_t)*MAX_SINKS);

		//node = add_neighbor(broadcast_pkt.hdr.pos, (rimeaddr_t*)from, sink_conn);

#ifdef FORCED_NEIGHBORS
		if(distance(broadcast_pkt.hdr.pos, own_pos)<=8.0){
			node = add_neighbor(broadcast_pkt.hdr.pos, (rimeaddr_t*)from);
		}
#else
		node = add_neighbor(broadcast_pkt.hdr.pos, (rimeaddr_t*)from);
#endif

		//PRINTF("Neighbor %d.%d added under index: %d. \n", neighbors_list.addr[node].u8[0], neighbors_list.addr[node].u8[1], node);

		if(node>=0){
			//PRINTF("updated neighbor: %d.%d, ", neighbors_list.addr[node].u8[0], neighbors_list.addr[node].u8[1]);

			//List of voids or unreachable sinks
			update_sink_void(broadcast_pkt.sink_void_list, node);

			neighbors_list.consumed_energy[node] = broadcast_pkt.consumed_energy;
		}
	}
}
/*---------------------------------------------------------------------------*/
/* Declare the broadcast  structures */
static struct broadcast_conn broadcast;
/* This is where we define what function to be called when a broadcast
   is received. We pass a pointer to this structure in the
   broadcast_open() call below. */
static const struct broadcast_callbacks broadcast_call = {broadcast_recv};
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(broadcast_process, ev, data)
{
	PROCESS_EXITHANDLER(broadcast_close(&broadcast);)

	PROCESS_BEGIN();

	static struct etimer et, et_bootstrap;

	broadcast_pos_event = process_alloc_event();

	broadcast_open(&broadcast, BROADCAST_CHANNEL, &broadcast_call);

	etimer_set(&et, CLOCK_SECOND + random_rand()%(MAX_NEIGHBORS*CLOCK_SECOND));
	etimer_set(&et_bootstrap, BOOTSTRAP_TIME*CLOCK_SECOND + random_rand()%(MAX_NEIGHBORS*CLOCK_SECOND));

	while(1) {
		PROCESS_WAIT_EVENT();

		if(etimer_expired(&et)) {

			broadcast_pkt_t broadcast_pkt;

			/* Send a broadcast every BROADCAST_PERIOD */
			if(etimer_expired(&et_bootstrap)){
				etimer_set(&et, CLOCK_SECOND*BROADCAST_PERIOD + random_rand()%((BROADCAST_PERIOD+1)*CLOCK_SECOND));
			}
			else{
				etimer_set(&et, CLOCK_SECOND*MAX_NEIGHBORS + random_rand()%((MAX_NEIGHBORS+1)*CLOCK_SECOND));
			}

			// prepare the broadcast packet
			broadcast_pkt.hdr.ver = MULTICAST_VERSION;
			broadcast_pkt.hdr.type = MULTICAST_BROADCAST_POS;
			broadcast_pkt.hdr.pos.x = own_pos.x;
			broadcast_pkt.hdr.pos.y = own_pos.y;
			broadcast_pkt.consumed_energy = sensor_metrics()->energy_cost_tx + sensor_metrics()->energy_cost_rx + sensor_metrics()->ctrl_energy_cost_tx + sensor_metrics()->ctrl_energy_cost_rx;
			//memset(broadcast_pkt.sink_conn, -1, sizeof(int8_t)*MAX_SINKS);

			memcpy(broadcast_pkt.sink_void_list, my_sink_void_list, sizeof(int8_t)*MAX_SINKS);

			/*uint8_t i;
			for(i=0; i<MAX_SINKS; i++){
				if(get_index_by_addr(&my_sink_list.addr[i], neighbors_list.addr, MAX_NEIGHBORS)>=0){
					broadcast_pkt.sink_conn[i]=i;
				}
			}*/

			//printf("WARNING: Broadcast from %d.%d sent. Packet size: %d\n", rimeaddr_node_addr.u8[0], rimeaddr_node_addr.u8[1], PKTSIZE(broadcast_pkt_t));

			packetbuf_copyfrom(&broadcast_pkt, sizeof(broadcast_pkt_t));

			register_energy_cost(ENERGY_COST_TX_CTRL(MAX_COMMUNICATION_RANGE, broadcast_pkt_t), TX, CONTROL);

			broadcast_send(&broadcast);
		}
	}

	printf("WARNING: Broadcast process is finished!\n");

	PROCESS_END();
}

/*---------------------------------------------------------------------------*/

void sink_void_notification(const int8_t *found_sinks, const int8_t *allowed_sinks){

	if(VOID_CONTROL_MESSAGE){
		int8_t i;
		i = count_valid(allowed_sinks, MAX_SINKS)-count_valid(found_sinks, MAX_SINKS);
		if(i>0){
			for(i=0; i<MAX_SINKS; i++){
				if(allowed_sinks[i]>=0 && found_sinks[i]==-1){
					if(my_sink_void_list[i]==-1){
						my_sink_void_list[i]=i;
					}
				}
			}
		}
	}

	return;
}

/*---------------------------------------------------------------------------*/

void update_sink_void(int8_t *sink_void_list, int8_t node){

	if(VOID_CONTROL_MESSAGE){
		int8_t OK, i, j;

		if(node>=0){
			for(i=0;i<MAX_SINKS;i++){
				if(sink_void_list[i]>=0){
					neighbors_list.sink_list[node][i] = -1;
					OK = 0;
					for(j=0;j<MAX_NEIGHBORS;j++){
						if(neighbors_list.sink_list[j][i]>=0){
							OK = 1;
						}
					}
					if(OK==0){
						my_sink_void_list[i]=i;
					}
				}
			}
		}
	}
}

/*---------------------------------------------------------------------------*/

/*****************************************************************************/
/* Multihop Process                                                          */
/*****************************************************************************/

/*---------------------------------------------------------------------------*/
/*
 * This function is called at the final recipient of the message.
 */
static void
recv(struct multihop_conn *c, const rimeaddr_t *sender,
		const rimeaddr_t *prevhop,
		uint8_t hops)
{

	reading_pkt_t reading_pkt;

	memcpy(&reading_pkt, packetbuf_dataptr(), sizeof(reading_pkt_t));

	if(reading_pkt.reading_hdr.hdr.ver != MULTICAST_VERSION) {
		return;
	}

	if(reading_pkt.reading_hdr.hdr.type == MULTICAST_MULTIHOP) {
		register_energy_cost(ENERGY_COST_RX(reading_pkt_t), RX, DATA);
		packet_received(&reading_pkt);
		PRINTF("Packet received from: %d.%d with originator: %d.%d\n", prevhop->u8[0], prevhop->u8[1], reading_pkt.reading_hdr.hdr.originator.u8[0],reading_pkt.reading_hdr.hdr.originator.u8[1]);
	}
}
/*---------------------------------------------------------------------------*/
/*
 * This function is called to forward a packet. The function picks the
 * neighbor closest to the destination from the neighbor list. If no
 * neighbor is found, the function returns NULL to signal to the multihop layer
 * that the packet should be dropped.
 */
static rimeaddr_t *
forward(struct multihop_conn *c,
		const rimeaddr_t *originator, const rimeaddr_t *dest,
		const rimeaddr_t *prevhop, uint8_t hops)
{

	static reading_pkt_t *packet=NULL;
	static uint8_t countp=0;

	if(packet==NULL){
		packet = (reading_pkt_t *)malloc(MAX_K*sizeof(reading_pkt_t));
	}

	countp++;
	if(countp<MAX_K){
		countp=0;
	}

	// The packetbuf_dataptr() returns a pointer to the first data byte in the received packet.
	memcpy(&packet[countp], (reading_pkt_t *)packetbuf_dataptr(), sizeof(reading_pkt_t));

	if(packet[countp].reading_hdr.prep==0 && !rimeaddr_cmp(&packet[countp].reading_hdr.hdr.originator, &rimeaddr_node_addr)){
		register_energy_cost(ENERGY_COST_RX(reading_pkt_t), RX, DATA);
		packet[countp].energy_cost=packet[countp].energy_cost+ENERGY_COST_RX(reading_pkt_t);
	}

	PRINTF("**Multihop process started!**\n");

	int8_t i, node;
	//Check if it's a packet that was duplicated
	if(packet[countp].reading_hdr.prep==1){

		node = get_index_by_addr(&packet[countp].reading_hdr.forwarder, neighbors_list.addr, MAX_NEIGHBORS);

		clock_time_t latency;
		latency = (clock_time_t)(((long int)SYSNOW + my_offset) - (long int)packet[countp].timestamp);

		TIMESTAMP;
		PRINTF_LOG("[GeoM] Forwarding packet ID: %d-%d-%lu to %d.%d, ", packet[countp].reading_hdr.hdr.originator.u8[0], packet[countp].reading_hdr.hdr.originator.u8[1], packet[countp].timestamp_original, packet[countp].reading_hdr.forwarder.u8[0], packet[countp].reading_hdr.forwarder.u8[1]);
		PRINTF_LOG("k: %d, sinks: ", packet[countp].reading_hdr.k);
		for(i=0;i<MAX_SINKS;i++){
			if(!rimeaddr_cmp(&rimeaddr_null, &packet[countp].reading_hdr.allowed_sinks[i])){
				PRINTF_LOG("%d.%d, ", packet[countp].reading_hdr.allowed_sinks[i].u8[0], packet[countp].reading_hdr.allowed_sinks[i].u8[1]);
			}
		}
		PRINTF_LOG("hops: %d | ", packet[countp].reading_hdr.hdr.hops);
		//PRINTF_LOG("Latency: %lu ms, now: %lu, offset: %ld, timestamp: %lu | ", TICKSTOMS(latency), now, my_offset, packet[countp].timestamp);
		PRINTF_LOG("Latency: %lu ms | ", TICKSTOMS(latency));
		PRINTF_LOG("Originator: %d.%d | Packet size: %d bytes", packet[countp].reading_hdr.hdr.originator.u8[0], packet[countp].reading_hdr.hdr.originator.u8[1], PKTSIZE(reading_pkt_t));
		if(packet[countp].reading_hdr.recovery_flag==1){
			PRINTF_LOG("| Recovery");
		}
		PRINTF_LOG(" | dest: %d.%d", my_sink_list.addr[packet[countp].reading_hdr.dest].u8[0], my_sink_list.addr[packet[countp].reading_hdr.dest].u8[1]);
		PRINTF_LOG("\n");

		packet[countp].reading_hdr.prep = 0;

		register_forwarded_packet();
		register_energy_cost(ENERGY_COST_TX(own_pos,neighbors_list.pos[node],reading_pkt_t), TX, DATA);

		packetbuf_copyfrom(&packet[countp], sizeof(reading_pkt_t));

		if(get_index_by_addr(&packet[countp].reading_hdr.forwarder, packet[countp].reading_hdr.allowed_sinks, MAX_SINKS)>=0 && packet[countp].reading_hdr.k==1){
			packetbuf_set_addr(PACKETBUF_ADDR_ERECEIVER, &packet[countp].reading_hdr.forwarder);
		}

		  packetbuf_set_attr(PACKETBUF_ATTR_RELIABLE, 1);
		  packetbuf_set_attr(PACKETBUF_ATTR_PACKET_TYPE, PACKETBUF_ATTR_PACKET_TYPE_DATA);
		  packetbuf_set_attr(PACKETBUF_ATTR_MAX_MAC_TRANSMISSIONS, 10);

		return &packet[countp].reading_hdr.forwarder;
	}

	if(packet[countp].reading_hdr.hdr.ver != MULTICAST_VERSION) {
		PRINTF("ERROR: Wrong packet type! \n");
		return NULL;
	}

	//Check if the node is within the destination sinks
	i=get_index_by_addr(&rimeaddr_node_addr, packet[countp].reading_hdr.allowed_sinks, MAX_SINKS);
	if(i>=0){
		rimeaddr_copy(&packet[countp].reading_hdr.allowed_sinks[i], &rimeaddr_null);
		packet[countp].reading_hdr.k = packet[countp].reading_hdr.k - 1;

		packet_received(&packet[countp]);
	}

	if(packet[countp].reading_hdr.k==0){
		PRINTF("ERROR: k is already 0! \n");
		return NULL;
	}


	//DETECT LOOP
	//printf("DEBUG: Packet ID Loop: %d-%d-%lu, Packet ID: %d-%d-%lu. Same sinks: %d.\n", count_loop.id_1, count_loop.id_2, count_loop.id_3, packet[countp].reading_hdr.hdr.originator.u8[0], packet[countp].reading_hdr.hdr.originator.u8[1], packet[countp].timestamp_original, memcmp(packet[countp].reading_hdr.allowed_sinks, count_loop.sinks, MAX_SINKS*sizeof(rimeaddr_t)));
	if(count_loop.id_1==packet[countp].reading_hdr.hdr.originator.u8[0] && count_loop.id_2==packet[countp].reading_hdr.hdr.originator.u8[1] && count_loop.id_3==packet[countp].timestamp_original && memcmp(packet[countp].reading_hdr.allowed_sinks, count_loop.sinks, MAX_SINKS*sizeof(rimeaddr_t))==0){
		count_loop.count++;
		if(count_loop.count==3){
			if(packet[countp].reading_hdr.dest>=0){
				int8_t black_list_sinks[MAX_SINKS];
				memset(black_list_sinks, -1, MAX_SINKS*sizeof(int8_t));
				black_list_sinks[packet[countp].reading_hdr.dest]=packet[countp].reading_hdr.dest;
				for(i=0; i<MAX_NEIGHBORS; i++){
					if(count_loop.black_list[i]==0){
						update_sink_void(black_list_sinks, i);
					}
				}
			}
			packet[countp].reading_hdr.min_weight = -1;
			packet[countp].reading_hdr.recovery_flag = 0;
		}
		if(count_loop.count>=4){
			TIMESTAMP;
			PRINTF_LOG("[GeoM] Loop detected with packet ID: %d-%d-%lu. Packet was dropped.\n", packet[countp].reading_hdr.hdr.originator.u8[0], packet[countp].reading_hdr.hdr.originator.u8[1], packet[countp].timestamp_original);
			return NULL;
		}
	}
	else{
		count_loop.id_1=packet[countp].reading_hdr.hdr.originator.u8[0];
		count_loop.id_2=packet[countp].reading_hdr.hdr.originator.u8[1];
		count_loop.id_3=packet[countp].timestamp_original;
		memcpy(count_loop.sinks, packet[countp].reading_hdr.allowed_sinks, MAX_SINKS*sizeof(rimeaddr_t));
		count_loop.count=1;
	}


	//clock_time_t calc_time;
	//calc_time = (clock_time_t)(SYSNOW);

	candidate_forwarders(&packet[countp]);

	PRINTF("Candidates selected! \n");

	//calc_time = (clock_time_t)(SYSNOW - calc_time);
	//printf("total calc time: %d ms\n", calc_time);

	return NULL;
}

/*---------------------------------------------------------------------------*/

uint8_t send_packet(const reading_pkt_t *reading_pkt, int8_t node, const int8_t *sinks, int8_t k, uint8_t recovery_flag, int8_t dest){

#ifdef CALCULATION_TIME
	clock_time_t first_mark, original_timestamp;
	first_mark = (clock_time_t)(SYSNOW+get_node_offset());
	original_timestamp = reading_pkt->timestamp;
#endif

	if(node>=0){

		PRINTF("Preparing packet to be sent!\n");

		static reading_pkt_t new_packet[MAX_PKT];
		static uint8_t count_from=0;

		count_from++;
		if(count_from==MAX_PKT){
			count_from = 0;
		}

		//printf("Preparing packet number %d to be sent to %d.%d with k=%d. \n", count_from, neighbors_list.addr[node].u8[0], neighbors_list.addr[node].u8[1], fwdrs->k);

		//Configuring the packet
		new_packet[count_from].reading_hdr.hdr.ver = reading_pkt->reading_hdr.hdr.ver;
		new_packet[count_from].reading_hdr.hdr.type = reading_pkt->reading_hdr.hdr.type;
		new_packet[count_from].reading_hdr.hdr.hops = reading_pkt->reading_hdr.hdr.hops + 1;
		new_packet[count_from].reading_hdr.hdr.pos.x = reading_pkt->reading_hdr.hdr.pos.x;
		new_packet[count_from].reading_hdr.hdr.pos.y = reading_pkt->reading_hdr.hdr.pos.y;
		rimeaddr_copy(&new_packet[count_from].reading_hdr.hdr.originator, &reading_pkt->reading_hdr.hdr.originator);

		if(recovery_flag==1){
			new_packet[count_from].reading_hdr.dest = reading_pkt->reading_hdr.dest;
			new_packet[count_from].reading_hdr.min_weight = reading_pkt->reading_hdr.min_weight;
		}
		else{
			new_packet[count_from].reading_hdr.dest = dest;
			new_packet[count_from].reading_hdr.min_weight = updated_weight(sinks, -1, k);
		}

		new_packet[count_from].reading_hdr.k = k;
		new_packet[count_from].reading_hdr.recovery_ref.x = reading_pkt->reading_hdr.recovery_ref.x;
		new_packet[count_from].reading_hdr.recovery_ref.y = reading_pkt->reading_hdr.recovery_ref.y;
		new_packet[count_from].reading_hdr.recovery_flag = recovery_flag;
		rimeaddr_copy(&new_packet[count_from].reading_hdr.recovery_trigger_addr, &reading_pkt->reading_hdr.recovery_trigger_addr);
		memset(new_packet[count_from].reading_hdr.allowed_sinks, 0, sizeof(rimeaddr_t)*MAX_SINKS);
		uint8_t j;
		for(j=0; j<MAX_SINKS; j++){
			if(sinks[j]>=0){
				rimeaddr_copy(&new_packet[count_from].reading_hdr.allowed_sinks[j], &my_sink_list.addr[j]);
			}
		}
		rimeaddr_copy(&new_packet[count_from].reading_hdr.forwarder, &neighbors_list.addr[node]);
		rimeaddr_copy(&new_packet[count_from].reading_hdr.prev, &rimeaddr_node_addr);
		new_packet[count_from].reading_hdr.prep = 1;

		new_packet[count_from].value = reading_pkt->value;
		new_packet[count_from].timestamp = reading_pkt->timestamp;
		new_packet[count_from].timestamp_original = reading_pkt->timestamp_original;

		//Calculate the energy cost
		new_packet[count_from].energy_cost=reading_pkt->energy_cost+ENERGY_COST_TX(own_pos,neighbors_list.pos[node],reading_pkt_t);


		if(count_loop.id_1==reading_pkt->reading_hdr.hdr.originator.u8[0] && count_loop.id_2==reading_pkt->reading_hdr.hdr.originator.u8[1] && count_loop.id_3==reading_pkt->timestamp_original && memcmp(new_packet[count_from].reading_hdr.allowed_sinks, count_loop.sinks, MAX_SINKS*sizeof(rimeaddr_t))==0){
			count_loop.black_list[node] = 0;
		}


#ifdef CALCULATION_TIME
		new_packet[count_from].timestamp = (clock_time_t)(original_timestamp + (SYSNOW + get_node_offset() - first_mark));
#endif

		//send out the duplication
		if(process_post(&multihop_process, multipacket_event, (void*)&new_packet[count_from])==PROCESS_ERR_OK){
			//process_poll(&multihop_process);
			PRINTF("New multihop event posted to %d.%d, k = %d! \n", new_packet[count_from].reading_hdr.forwarder.u8[0], new_packet[count_from].reading_hdr.forwarder.u8[1], new_packet[count_from].reading_hdr.k);
		}
		else{
			PRINTF("ERROR: Multihop event for packet duplication FAILED (Neighbor: %d.%d, k = %d)! \n", neighbors_list.addr[node].u8[0], neighbors_list.addr[node].u8[1], k);
		}

		return 1;
	}

	return 0;
}

/*---------------------------------------------------------------------------*/

void new_packet_generation(void *data) {

	/* Send the broadcast message with the sink void list */
	process_post(&multihop_process, newpacket_event, data);
	process_poll(&multihop_process);

	//UPDATE NODE METRICS
	register_generated_packet();
}

/*---------------------------------------------------------------------------*/

void packet_received(reading_pkt_t *reading_pkt_in){

	clock_time_t latency;

	latency = (clock_time_t)(((long int)SYSNOW + my_offset) - (long int)reading_pkt_in->timestamp);

	//if((((long int)SYSNOW + my_offset) - (long int)reading_pkt_in->timestamp)<0){
	//	printf("ERROR --- negative latency!\n");
	//}

	register_received_packet();
	register_latency(latency);

	TIMESTAMP;
	PRINTF_LOG("[GeoM] Latency: %lu ms | ", TICKSTOMS(latency));
	//PRINTF_LOG("[GeoM] Latency: %lu ms - now: %lu - my_offset: %lld - now_e: %lu - timestamp: %lu | ", TICKSTOMS(latency), SYSNOW, my_offset, (clock_time_t)(((long int)SYSNOW + my_offset)), reading_pkt_in->timestamp);
	PRINTF_LOG("Originator: %d.%d | ", reading_pkt_in->reading_hdr.hdr.originator.u8[0], reading_pkt_in->reading_hdr.hdr.originator.u8[1]);
	PRINTF_LOG("Previous Sender: %d.%d | ", reading_pkt_in->reading_hdr.prev.u8[0], reading_pkt_in->reading_hdr.prev.u8[1]);
	PRINTF_LOG("Hops: %d | ", reading_pkt_in->reading_hdr.hdr.hops);
	PRINTF_LOG("Consumed Energy (TX+RX): ");
	NOEXP(reading_pkt_in->energy_cost);
	PRINTF_LOG(" J | Packet ID: %d-%d-%lu | ", reading_pkt_in->reading_hdr.hdr.originator.u8[0], reading_pkt_in->reading_hdr.hdr.originator.u8[1], reading_pkt_in->timestamp_original);
	PRINTF_LOG("Packet Size: %d bytes\n", PKTSIZE(reading_pkt_t));
}

/*---------------------------------------------------------------------------*/
/* Declare multihop structures */
static const struct multihop_callbacks multihop_call = {recv, forward};
static struct multihop_conn multihop;
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(multihop_process, ev, data)
{
	PROCESS_EXITHANDLER(multihop_close(&multihop);)

		  PROCESS_BEGIN();

	static rimeaddr_t to;

	multipacket_event = process_alloc_event();
	newpacket_event = process_alloc_event();

	/* Open a multihop connection on Rime channel MULTIHOP_CHANNEL. */
	multihop_open(&multihop, MULTIHOP_CHANNEL, &multihop_call);

	/* Set the Rime address of the final receiver of the packet to
     254.254 as we rely on the x,y coordinates to deliver the packet.
     once the recipient is found the address of the destination is updated. */
	to.u8[0] = 254;
	to.u8[1] = 254;

	/* Loop forever, send a packet when the button is pressed. */
	while(1) {
		/* Wait until we get a sensor event with the button sensor as data. */
		PRINTF("Waiting for multihop packet event.\n");
		PROCESS_WAIT_EVENT();
		if (ev == newpacket_event) {
			/* this event is just for debug */
			PRINTF("Creating multihop packet\n");

			reading_pkt_t reading_pkt;

			memset(&reading_pkt, 0, sizeof(reading_pkt_t));

			memset(reading_pkt.reading_hdr.allowed_sinks, 0, sizeof(rimeaddr_t)*MAX_SINKS);
			memcpy(reading_pkt.reading_hdr.allowed_sinks, my_sink_list.addr, sizeof(rimeaddr_t)*MAX_SINKS);

			/* prepare the multihop packet */
			reading_pkt.reading_hdr.hdr.ver = MULTICAST_VERSION;
			reading_pkt.reading_hdr.hdr.type = MULTICAST_MULTIHOP;
			reading_pkt.reading_hdr.hdr.pos.x = own_pos.x;
			reading_pkt.reading_hdr.hdr.pos.y = own_pos.y;
			reading_pkt.reading_hdr.hdr.hops = 0;
			rimeaddr_copy(&reading_pkt.reading_hdr.hdr.originator, &rimeaddr_node_addr);
			reading_pkt.reading_hdr.k = MAX_K;
			reading_pkt.reading_hdr.min_weight = -1;//updated_weight(my_sink_list.sinks, -1, MAX_K);
			reading_pkt.reading_hdr.prep = 0;
			reading_pkt.reading_hdr.recovery_ref.x = 0.0;
			reading_pkt.reading_hdr.recovery_ref.y = 0.0;
			reading_pkt.reading_hdr.recovery_flag = 0;
			reading_pkt.reading_hdr.dest = -1;

			prepare_reading_pkt(&reading_pkt);

			/* Copy the reading to the packet buffer. */
			packetbuf_copyfrom(&reading_pkt, sizeof(reading_pkt_t));

			/* Send the packet. */
			multihop_send(&multihop, &to);

			PRINTF("Created multihop packet posted.\n");

		}
		else{
			if (ev == multipacket_event) {

				PRINTF("Sending multihop packet\n");

				/* Copy the reading to the packet buffer. */
				packetbuf_copyfrom((reading_pkt_t *)data, sizeof(reading_pkt_t));

				/* Send the packet. */
				multihop_send(&multihop, &to);

				PRINTF("Multihop packet sent.\n");
			}
		}
	}

	PROCESS_END();
}

/*---------------------------------------------------------------------------*/

/*****************************************************************************/
/* Boot Process                                                              */
/*****************************************************************************/

void receive_info(char *data_copy){

	int c;
	char str[SERIAL_LINE_CONF_BUFSIZE], *aux;
	pos_t rcvd_pos = {0.0 , 0.0};
	rimeaddr_t rime_addr;
	clock_time_t ref=0;

	//Initialize string
	memset(str, '\0', sizeof(char)*SERIAL_LINE_CONF_BUFSIZE);
	memcpy(str, data_copy, SERIAL_LINE_CONF_BUFSIZE*sizeof(char));

	c=0;
	while(str[c]!='#'){
		//aux=&str[c];
		//printf("c: %d, STRING: %s\n", c, aux);
		if(str[c]=='t'){
			c+=2;
			aux=&str[c];
			while(str[c]!='|') c++;
			str[c]='\0';
			ref = (clock_time_t)atol((char *)aux);
			//memcpy(str, data_copy, SERIAL_LINE_CONF_BUFSIZE*sizeof(char));
			printf("Reference time received: %lu.\n", ref);
		}
#ifdef SENSLAB
		if(str[c]=='i'){
			c+=2;
			aux=&str[c];
			while(str[c]!='|') c++;
			str[c]='\0';
			myid = (uint8_t)atoi((char *)aux);
			//memcpy(str, data_copy,SERIAL_LINE_CONF_BUFSIZE*sizeof(char));
			printf("Received Node ID: %d.\n", myid);
		}
		if(str[c]=='v'){
			while(str[c]!=':') c++;
			c++;
			aux=&str[c];
			while(str[c]!='.') c++;
			str[c]='\0';
			rime_addr.u8[0]=(unsigned char)atoi((char *)aux);
			//memcpy(str, data_copy,SERIAL_LINE_CONF_BUFSIZE*sizeof(char));
			c+=1;
			aux=&str[c];
			while(str[c]!='|') c++;
			str[c]='\0';
			rime_addr.u8[1]=(unsigned char)atoi((char *)aux);
			//memcpy(str, data_copy,SERIAL_LINE_CONF_BUFSIZE*sizeof(char));
			rcvd_pos.x=own_pos.x;
			rcvd_pos.y=own_pos.y;
			if(add_neighbor(rcvd_pos, &rime_addr)==-1){
				printf("ERROR: Neighbor %d.%d was not included.\n", rime_addr.u8[0], rime_addr.u8[1]);
			}
		}
#endif
		if(str[c]=='k'){
			c+=2;
			aux=&str[c];
			while(str[c]!='|') c++;
			str[c]='\0';
			//received_max_k = (uint8_t)stoi((char *)aux);
			received_max_k = (uint8_t)atoi((char *)aux);
			//memcpy(str, data_copy,SERIAL_LINE_CONF_BUFSIZE*sizeof(char));
			printf("Maximum K received: %d.\n", received_max_k);
		}
		if(str[c]=='n'){
			c+=2;
			aux=&str[c];
			while(str[c]!=';') c++;
			str[c]='\0';
			c++;
#ifdef SENSLAB
			own_pos.x = stof((char *)aux);
#else
			own_pos.x = atof((char *)aux);
#endif
			//memcpy(str, data_copy,SERIAL_LINE_CONF_BUFSIZE*sizeof(char));
			aux=&str[c];
			while(str[c]!='|') c++;
			str[c]='\0';
#ifdef SENSLAB
			own_pos.y = stof((char *)aux);
#else
			own_pos.y = atof((char *)aux);
#endif
			//memcpy(str, data_copy,SERIAL_LINE_CONF_BUFSIZE*sizeof(char));
			printf("Coords X,Y received.\n");
		}
		if(str[c]=='s'){
			c+=2;
			aux=&str[c];
			while(str[c]!='.') c++;
			str[c]='\0';
			rime_addr.u8[0]=(unsigned char)atoi((char *)aux);
			//memcpy(str, data_copy,SERIAL_LINE_CONF_BUFSIZE*sizeof(char));
			c+=1;
			aux=&str[c];
			while(str[c]!=';') c++;
			str[c]='\0';
			rime_addr.u8[1]=(unsigned char)atoi((char *)aux);
			//memcpy(str, data_copy,SERIAL_LINE_CONF_BUFSIZE*sizeof(char));
			c++;
			aux=&str[c];
			while(str[c]!=';') c++;
			str[c]='\0';
			c++;
#ifdef SENSLAB
			rcvd_pos.x = stof((char *)aux);
#else
			rcvd_pos.x = atof((char *)aux);
#endif
			//memcpy(str, data_copy,SERIAL_LINE_CONF_BUFSIZE*sizeof(char));
			aux=&str[c];
			while(str[c]!='|') c++;
			str[c]='\0';
#ifdef SENSLAB
			rcvd_pos.y = stof((char *)aux);
#else
			rcvd_pos.y = atof((char *)aux);
#endif
			//memcpy(str, data_copy,SERIAL_LINE_CONF_BUFSIZE*sizeof(char));
			if(add_sink(rcvd_pos, &rime_addr)==-1){
				printf("Sink %d.%d was not included.\n", rime_addr.u8[0], rime_addr.u8[1]);
			}
			else{
				printf("Sink %d.%d was included.\n", rime_addr.u8[0], rime_addr.u8[1]);
				if(rimeaddr_cmp(&rimeaddr_node_addr, &rime_addr)){
					i_am_sink = 1;
				}
			}
		}
		c++;
	}

	if(ref>0){
		// Calculate the offset for network synchronization
		my_offset = (long int)ref - my_offset;
		printf("My offset: %ld, External Ref.: %lu, Now: %lu, Ext_now: %lu \n", my_offset, ref, SYSNOW, TICKSTOMS((long int)SYSNOW+my_offset));
	}
}

/*---------------------------------------------------------------------------*/

void geo_multicast_init() {

	//starts the serial line process
	serial_line_init();

	process_start(&boot_process, NULL);
}

/*---------------------------------------------------------------------------*/

PROCESS_THREAD(boot_process, ev, data)
{

	PROCESS_BEGIN();

	char data1[SERIAL_LINE_CONF_BUFSIZE];
#ifndef SENSLAB
	char data2[SERIAL_LINE_CONF_BUFSIZE], data3[SERIAL_LINE_CONF_BUFSIZE], data4[SERIAL_LINE_CONF_BUFSIZE];
#endif

	received_max_k = 0;

	// Initialize the list used for the neighbor table.
	memset(&neighbors_list, 0, sizeof(kneighbor));
	//memset(neighbors_list.sink_conn, -1, sizeof(int8_t)*MAX_NEIGHBORS*MAX_SINKS);
	memset(neighbors_list.sink_list, -1, sizeof(int8_t)*MAX_NEIGHBORS*MAX_SINKS);

	// Initialize the list used for the sink table.
	memset(&my_sink_list, 0, sizeof(ksink));
	memset(my_sink_list.sinks, -1, sizeof(int8_t)*MAX_SINKS);

	//initialize my_sink_void_list
	memset(my_sink_void_list, -1, sizeof(int8_t)*MAX_SINKS);

	memset(data1, '\0', sizeof(char)*SERIAL_LINE_CONF_BUFSIZE);
#ifndef SENSLAB
	memset(data2, '\0', sizeof(char)*SERIAL_LINE_CONF_BUFSIZE);
	memset(data3, '\0', sizeof(char)*SERIAL_LINE_CONF_BUFSIZE);
	memset(data4, '\0', sizeof(char)*SERIAL_LINE_CONF_BUFSIZE);
#endif

	// SINK DETECTION
	// get the Sink list and coordinates from COOJA
	// get the node coordinates from cooja, adapted from:
	// https://sourceforge.net/p/contiki/mailman/message/34696644/
	printf("Waiting for node coordinates from Cooja...\n");
	PROCESS_WAIT_EVENT_UNTIL(ev == serial_line_event_message);
	memcpy(data1,(char *)data,SERIAL_LINE_CONF_BUFSIZE*sizeof(char));
	my_offset=SYSNOW;
#ifndef SENSLAB
	printf("data1: %s\n", data1);
#endif
	printf("Received-1...\n");

#ifndef SENSLAB
	PROCESS_WAIT_EVENT_UNTIL(ev == serial_line_event_message);
	memcpy(data2,(char *)data,SERIAL_LINE_CONF_BUFSIZE*sizeof(char));
	printf("data2: %s\n", data2);
	printf("Received-2...\n");

  	PROCESS_WAIT_EVENT_UNTIL(ev == serial_line_event_message);
  	memcpy(data3,(char *)data,SERIAL_LINE_CONF_BUFSIZE*sizeof(char));
	printf("data3: %s\n", data3);
	printf("Received-3...\n");

	PROCESS_WAIT_EVENT_UNTIL(ev == serial_line_event_message);
	memcpy(data4,(char *)data,SERIAL_LINE_CONF_BUFSIZE*sizeof(char));
	printf("data4: %s\n", data4);
	printf("Received-4...\n");
#endif

	receive_info(data1);
#ifndef SENSLAB
	receive_info(data2);
	receive_info(data3);
	receive_info(data4);
#endif

#ifdef SENSLAB
	count_neighbors = 1;
#endif


	count_loop.id_1 = 0;
	count_loop.id_2 = 0;
	count_loop.id_3 = 0;
	count_loop.count = 0;
	memset(count_loop.black_list, -1, sizeof(int8_t)*MAX_NEIGHBORS);
	memset(count_loop.sinks, 0, MAX_SINKS*sizeof(rimeaddr_t));


	//Initialize the metrics variables
	my_metrics_init();

	/* start broadcast process */
	process_start(&broadcast_process, NULL);

	/* start multihop process */
	process_start(&multihop_process, NULL);

	printf("Node %d.%d: Booting completed. \n", rimeaddr_node_addr.u8[0], rimeaddr_node_addr.u8[1]);

	PROCESS_END();
}
/*---------------------------------------------------------------------------*/
