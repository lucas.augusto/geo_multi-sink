#ifndef KFUNCTIONS_H
#define KFUNCTIONS_H

#include "multicast_decision.h"

int8_t neighbor_is_sink(const rimeaddr_t *neighbor_addr);

void closest_sinks(int8_t *neighbor_sink_list, pos_t neighbor, pos_t node);

int8_t count_valid(const int8_t *node_list, uint8_t list_size);

int8_t get_index_by_addr(const rimeaddr_t *item_addr, const rimeaddr_t *list_addr, uint8_t list_size);

void update_available_sinks(int8_t *available_sinks, int8_t *used_sinks);

int8_t get_the_closest(const int8_t *available_sinks);

int8_t get_the_closest_selected(const int8_t *available_sinks, const int8_t *selected_sinks);

float updated_weight(const int8_t *allowed_sinks, int8_t node, uint8_t k);

float mean_w(const kcandidates *cdts, calculated_weight *w, int8_t s, int8_t n, int8_t *sinks);

float stdev_w(const kcandidates *cdts, calculated_weight *w, int8_t s, int8_t n, int8_t *sinks, float mean_w);

#endif
