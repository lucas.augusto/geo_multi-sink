/*****************************************************************************/
/* Execution Configuration - Geo-Multicast							         */
/*****************************************************************************/

/* Defines the maximum number of neighbors we can remember. */
#define MAX_NEIGHBORS				16
/* Defines the maximum number of sinks in the network. */
#define MAX_SINKS           		10
/* Defines the maximum number of K sinks to be reached. */
#define MAX_K						get_received_max_k()
