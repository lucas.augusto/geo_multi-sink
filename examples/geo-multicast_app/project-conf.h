/*****************************************************************************/
/* Geo-multicast App - Configuration											 */
/*****************************************************************************/
/*
* Contiki stack layers possible configurations:
* 1. Network layer: rime_driver, sicslowpan_driver
* 2. MAC layer....​: nullmac_driver, csma_driver
* 3. RDC..........​: nullrdc_driver, contikimac_driver, cxmac_driver, xmac_driver, lpp_driver
* 4. Framer.......:​­ framer_nullmac, framer_802154
* 5. Radio​........: nullradio_driver, framer_802154
*/

#ifndef PROJECT_CONF_H_
#define PROJECT_CONF_H_

#include "sim_config.h"

/* Radio Duty Cycle channel check rate (Hz) */
#undef NETSTACK_CONF_RDC_CHANNEL_CHECK_RATE
#define NETSTACK_CONF_RDC_CHANNEL_CHECK_RATE	8

/* Definition of the Radio Duty Cycle driver */
#undef NETSTACK_CONF_RDC
#define NETSTACK_CONF_RDC	cxmac_driver

/* Definition of the MAC driver */
#undef NETSTACK_CONF_MAC
#define NETSTACK_CONF_MAC	csma_driver

/* Use 2 byte rime address */
#undef RIMEADDR_CONF_SIZE
#define RIMEADDR_CONF_SIZE			2

/* Broadcast channel number */
#define BROADCAST_CHANNEL			129

/* Multihop channel number */
#define MULTIHOP_CHANNEL			128

/* The size of the serial line to send the position of the node and sinks */
#define SERIAL_LINE_CONF_BUFSIZE    256

/* Packet data size */
#define PACKETBUF_CONF_SIZE			256

/* Maximum Communication Range in meters */
#define MAX_COMMUNICATION_RANGE     50

/* Defines how often (in seconds) a broadcast packet will be sent */
#define BROADCAST_PERIOD 			30

/* Define the time interval of the data packet */
#define PACKET_GENERATION_PERIOD	60

/* Define the rate of the data packet generation (% of the interval) */
#define PACKET_GENERATION_RATE		20
#define P_GEN_RATE					((PACKET_GENERATION_RATE/10)-1)

/* Define the max number of queued neighbors for transmission */
#undef CSMA_CONF_MAX_NEIGHBOR_QUEUES
#define CSMA_CONF_MAX_NEIGHBOR_QUEUES 	MAX_NEIGHBORS

#define BOOTSTRAP_TIME				    BROADCAST_PERIOD*3

/* Decision Metrics */
#define DIST_SINK_METRIC_WEIGHT     	  0.7
#define ENERGY_METRIC_WEIGHT			  0.1
#define CONSUMED_ENERGY_WEIGHT			  0.2
#define DEVIATION_FACTOR				  0.0

#define VOID_CONTROL_MESSAGE              1

//#define SENSLAB						  1
//#define FORCED_NEIGHBORS				  1
#define CALCULATION_TIME				  1

#ifdef SENSLAB
	#define SYSNOW					(clock_time_t)(clock_time())
//	#define TICKSTOMS(t)			(clock_time_t)((double)t/0.128)
	#define TICKSTOMS(t)			(t)
	#define NOEXP(value)			printf("%llue-12", (unsigned long long)(value*1000000000000.0))
	#define TIMESTAMP				printf("%lu ms ID:%d - ", TICKSTOMS(SYSNOW + get_node_offset()), get_received_external_id())
	#define PKTSIZE(c)				100
#else
	#define SYSNOW					(clock_time_t)(RTIMER_NOW()/(RTIMER_SECOND/1000))
	#define TICKSTOMS(t)			(t)
	#define TIMESTAMP
	#define NOEXP(value)			printf("%e", value)
	//#define PKTSIZE(c)				sizeof(c)
	#define PKTSIZE(c)				100
#endif

/* Energy Consumption Parameters */
#define ENERGY_COST_TX(a, b, c)			((50e-09*PKTSIZE(c)*8.0)+(100e-12*PKTSIZE(c)*8.0*(float)fastPrecisePow(distance(a,b),2)))
#define ENERGY_COST_TX_CTRL(ab, c)		((50e-09*PKTSIZE(c)*8.0)+(100e-12*PKTSIZE(c)*8.0*(float)fastPrecisePow(ab,2)))
#define ENERGY_COST_RX(c)				(50e-09*PKTSIZE(c)*8.0)
#define ENERGY_COST(dist)				((50e-09*8.0)+(100e-12*8.0*(float)fastPrecisePow(dist,2)))
#define TX								1
#define RX								2
#define CONTROL							1
#define DATA							2

/* Wait for all packets to be received by the sinks */
#define WAIT_FOR_OTHERS        		PACKET_GENERATION_PERIOD*2

/* Execution Time for Packet Generation */
#define TOTAL_EXECUTION_TIME        ((120*PACKET_GENERATION_PERIOD)+BOOTSTRAP_TIME)


#endif /* PROJECT_CONF_H_ */
