#include "contiki.h"
#include "net/rime.h"
#include "lib/random.h"

#include <stdio.h>  /* For printf() */

/* multicast includes */
#include "geo-multicast.h"
#include "my_metrics.h"

/* just for testing */
#ifndef SENSLAB
	#include "dev/button-sensor.h"
	#include "dev/leds.h"
#endif

PROCESS(app_process, "App process");
AUTOSTART_PROCESSES(&app_process);

/*---------------------------------------------------------------------------*/
PROCESS_THREAD(app_process, ev, data)
{
  PROCESS_BEGIN();

  printf("Start.\n");

  static struct etimer et, total, status;

  // starts the multicast process
    geo_multicast_init();

  // wait for a moment to let the network settle
  etimer_set(&et, CLOCK_SECOND * BOOTSTRAP_TIME);

  PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));

  // Activate the button sensor. We use the button to drive traffic when the button is pressed, a packet is sent.
#ifndef SENSLAB
  SENSORS_ACTIVATE(button_sensor);
#endif

  etimer_set(&status, (PACKET_GENERATION_PERIOD*CLOCK_SECOND*5));
  etimer_set(&et, random_rand()%(PACKET_GENERATION_PERIOD*CLOCK_SECOND));
  etimer_set(&total, TOTAL_EXECUTION_TIME*CLOCK_SECOND);


  while(1) {
  	PROCESS_WAIT_EVENT();

  	if(etimer_expired(&et) && !etimer_expired(&total)) {
  		if(((random_rand()%(10*CLOCK_SECOND))/CLOCK_SECOND)<=P_GEN_RATE){
  			if(!is_sink()){
  				//if((rimeaddr_node_addr.u8[0]==184 && rimeaddr_node_addr.u8[1]==20) || (rimeaddr_node_addr.u8[0]==181 && rimeaddr_node_addr.u8[1]==225) || (rimeaddr_node_addr.u8[0]==186 && rimeaddr_node_addr.u8[1]==56) || (rimeaddr_node_addr.u8[0]==179 && rimeaddr_node_addr.u8[1]==143) || (rimeaddr_node_addr.u8[0]==200 && rimeaddr_node_addr.u8[1]==234) || (rimeaddr_node_addr.u8[0]==206 && rimeaddr_node_addr.u8[1]==253) || (rimeaddr_node_addr.u8[0]==182 && rimeaddr_node_addr.u8[1]==97) || (rimeaddr_node_addr.u8[0]==205 && rimeaddr_node_addr.u8[1]==46)){
  				//if((rimeaddr_node_addr.u8[0]==184 && rimeaddr_node_addr.u8[1]==20) || (rimeaddr_node_addr.u8[0]==181 && rimeaddr_node_addr.u8[1]==225) || (rimeaddr_node_addr.u8[0]==186 && rimeaddr_node_addr.u8[1]==56) || (rimeaddr_node_addr.u8[0]==179 && rimeaddr_node_addr.u8[1]==143)){
  				//if(rimeaddr_node_addr.u8[0]==184 && rimeaddr_node_addr.u8[1]==20){
  					new_packet_generation(NULL);
  				//}
  			}
  		}
  		//if(etimer_expired(&status)) {
  			print_my_metrics(2);
  		  	//etimer_set(&status, (PACKET_GENERATION_PERIOD*CLOCK_SECOND*5));
  		//}
  	    etimer_set(&et, PACKET_GENERATION_PERIOD*CLOCK_SECOND + random_rand()%(PACKET_GENERATION_PERIOD*CLOCK_SECOND));
  	}
#ifndef SENSLAB
  	else if(ev == sensors_event && data == &button_sensor){
  			//printf("Button click. New packet generated.\n");
  			new_packet_generation(NULL);
  			print_my_metrics(2);
	}
#endif
  	else if(etimer_expired(&total)){

  		if(is_sink()){
  			etimer_set(&et, WAIT_FOR_OTHERS*CLOCK_SECOND);

  			PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));
  		}

  		printf("Packet Generation Finished! Printing all node data.\n");
  		print_my_metrics(1);

  		break;
	}
  }

  printf("The End.\n");

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/

