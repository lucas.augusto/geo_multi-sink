#include "contiki.h"
#include "net/rime.h"

#include <stdio.h>  /* For printf() */

/* kanycast includes */
#include "geo-kanycast.h"
#include "my_metrics.h"

/* just for testing */
#include "dev/button-sensor.h"
#include "dev/leds.h"

PROCESS(app_process, "App process");
AUTOSTART_PROCESSES(&app_process);

/*---------------------------------------------------------------------------*/
PROCESS_THREAD(app_process, ev, data)
{
  PROCESS_BEGIN();

  static struct etimer et, total;

  // starts the kanycast process
  geo_kanycast_init();

  // wait for a moment to let the network settle
  etimer_set(&et, CLOCK_SECOND * BOOTSTRAP_TIME);

  PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));

  /* Activate the button sensor. We use the button to drive traffic -
     when the button is pressed, a packet is sent. */
  SENSORS_ACTIVATE(button_sensor);

  etimer_set(&et, random_rand()%(PACKET_GENERATION_PERIOD*CLOCK_SECOND));
  //etimer_set(&et, (CLOCK_SECOND*rimeaddr_node_addr.u8[0]));

  etimer_set(&total, TOTAL_EXECUTION_TIME*CLOCK_SECOND);

  while(1) {
  	PROCESS_WAIT_EVENT();

  	if(etimer_expired(&et) && !etimer_expired(&total)) {
  		//printf("Timer expired. New packet generated.\n");
  		//if(!is_sink() && rimeaddr_node_addr.u8[0]==55){

  		if(((random_rand()%(10*CLOCK_SECOND))/CLOCK_SECOND)<=P_GEN_RATE){
  			if(!is_sink()){
  				new_packet_generation(NULL);
  			}
  		}
  		print_my_metrics(2);
  	    etimer_set(&et, PACKET_GENERATION_PERIOD*CLOCK_SECOND + random_rand()%(PACKET_GENERATION_PERIOD*CLOCK_SECOND));
  	}
  	else if(ev == sensors_event && data == &button_sensor){
  			printf("Button click. New packet generated.\n");
  			//printf("LIST OF SINKS: ");
  			//print_sinks();
  			//printf("\n");
  			new_packet_generation(NULL);
  			//print_my_metrics(2);
	}
  	else if(etimer_expired(&total)){

  		if(is_sink()){
  			etimer_set(&et, WAIT_FOR_OTHERS*CLOCK_SECOND);

  			PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));
  		}

  		printf("Packet Generation Finished! Printing all node data.\n");
  		print_my_metrics(1);

  		break;
	}
  }
  PROCESS_END();
}
/*---------------------------------------------------------------------------*/

