/*****************************************************************************/
/* Geo-Kanycast App - Configuration											 */
/*****************************************************************************/
/*
* Contiki stack layers possible configurations:
* 1. Network layer: rime_driver, sicslowpan_driver
* 2. MAC layer....​: nullmac_driver, csma_driver
* 3. RDC..........​: geo_kanycast_cxmac_driver -- (not available: nullrdc_driver, contikimac_driver, cxmac_driver, xmac_driver, lpp_driver)
* 4. Framer.......:​­ framer_nullmac, framer_802154
* 5. Radio​........: nullradio_driver, framer_802154
*/

#include "sim_config.h"

#ifndef PROJECT_CONF_H_
#define PROJECT_CONF_H_

/* Radio Duty Cycle channel check rate (Hz) */
#undef NETSTACK_CONF_RDC_CHANNEL_CHECK_RATE
#define NETSTACK_CONF_RDC_CHANNEL_CHECK_RATE	8

/* Definition of the Radio Duty Cycle driver */
#undef NETSTACK_CONF_RDC
#define NETSTACK_CONF_RDC	geo_kanycast_cxmac_driver

/* Definition of the MAC driver */
#undef NETSTACK_CONF_MAC
#define NETSTACK_CONF_MAC	csma_driver

/* Use 2 byte rime address */
#undef RIMEADDR_CONF_SIZE
#define RIMEADDR_CONF_SIZE			2

/* Broadcast channel number */
#define BROADCAST_CHANNEL			129

/* Multihop channel number */
#define MULTIHOP_CHANNEL			128

/* Packet data size */
#define PACKETBUF_CONF_SIZE			256

/* The size of the serial line to send the position of the node and sinks */
#define SERIAL_LINE_CONF_BUFSIZE    256

/* Maximum Communication Range in meters */
#define MAX_COMMUNICATION_RANGE     50

/* Defines how often (in seconds) a broadcast packet will be sent */
#define BROADCAST_PERIOD 			30

/* Define the time interval of the data packet */
#define PACKET_GENERATION_PERIOD	60

/* Define the rate of the data packet generation (% of the interval) */
#define PACKET_GENERATION_RATE		20
#define P_GEN_RATE					((PACKET_GENERATION_RATE/10)-1)

/* Define the max number of queued neighbors for transmission */
#undef CSMA_CONF_MAX_NEIGHBOR_QUEUES
#define CSMA_CONF_MAX_NEIGHBOR_QUEUES 	MAX_NEIGHBORS

/* Enables the ENCOUNTER OPTIMIZATION - AwakeDiff Dependency */
#define MAX_ENCOUNTERS_KANYCAST  MAX_NEIGHBORS

#define BOOTSTRAP_TIME				     BROADCAST_PERIOD*3
//#define BOOTSTRAP_TIME				         BROADCAST_PERIOD*10

#define DYNAMIC_METRIC				1

/* Decision Metrics */
#ifndef DYNAMIC_METRIC
	#define IS_FWDR_METRIC_WEIGHT(pk)			  0.0
	#define PACKET_COUNT_METRIC_WEIGHT(pk)		  0.0
	#define DIST_SINK_METRIC_WEIGHT(pk)	     	  0.6
	#define ENERGY_METRIC_WEIGHT(pk)			  0.2
	#define CONSUMED_ENERGY_WEIGHT(pk)			  0.2
	#define MAX_METRICS(pk)						  1.0
#else
	#define IS_FWDR_METRIC_WEIGHT(pk)			  (pk<=20?0.6:(pk<=40?0.6:(pk<=60?0.7:(pk<=80?0.7:0.9))))
	#define PACKET_COUNT_METRIC_WEIGHT(pk)		  (pk<=20?0.0:(pk<=40?0.0:(pk<=60?0.0:(pk<=80?0.0:0.0))))
	#define DIST_SINK_METRIC_WEIGHT(pk)		      (pk<=20?0.3:(pk<=40?0.3:(pk<=60?0.2:(pk<=80?0.2:0.1))))
	#define ENERGY_METRIC_WEIGHT(pk)			  (pk<=20?0.0:(pk<=40?0.0:(pk<=60?0.0:(pk<=80?0.0:0.0))))
	#define CONSUMED_ENERGY_WEIGHT(pk)			  (pk<=20?0.1:(pk<=40?0.1:(pk<=60?0.1:(pk<=80?0.1:0.0))))
	#define MAX_METRICS(pk)						  (pk<=20?3.0:(pk<=40?3.0:(pk<=60?3.0:(pk<=80?3.0:2.0))))
#endif

//#define DEVIATION_FACTOR(pk)				  1.0
#define DEVIATION_FACTOR(pk)				  (pk<=20?1.0:(pk<=40?1.0:(pk<=60?1.0:(pk<=80?1.0:1.0))))

#define SYSNOW					(clock_time_t)(RTIMER_NOW()/(RTIMER_SECOND/1000))
#define VOID_CONTROL_MESSAGE              1
#define PKTSIZE(c)				100
#define CALCULATION_TIME				  1

/* Energy Consumption Parameters */
#define ENERGY_COST_TX(a, b, c)			((50e-09*PKTSIZE(c)*8.0)+(100e-12*PKTSIZE(c)*8.0*(float)fastPrecisePow(distance(a,b),2)))
#define ENERGY_COST_TX_CTRL(ab, c)		((50e-09*PKTSIZE(c)*8.0)+(100e-12*PKTSIZE(c)*8.0*(float)fastPrecisePow(ab,2)))
#define ENERGY_COST_RX(c)				(50e-09*PKTSIZE(c)*8.0)
#define ENERGY_COST(dist)				((50e-09*8.0)+(100e-12*8.0*(float)fastPrecisePow(dist,2)))
#define TX								1
#define RX								2
#define CONTROL							1
#define DATA							2

/* Wait for all packets to be received by the sinks */
#define WAIT_FOR_OTHERS        		PACKET_GENERATION_PERIOD*2

/* Execution Time for Packet Generation */
#define TOTAL_EXECUTION_TIME        ((120*PACKET_GENERATION_PERIOD)+BOOTSTRAP_TIME)

#endif /* PROJECT_CONF_H_ */

